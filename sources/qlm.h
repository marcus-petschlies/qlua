#ifndef QLM_H__DAB3D8AA_24DB_4C80_8268_62DEB1688C8E
#define QLM_H__DAB3D8AA_24DB_4C80_8268_62DEB1688C8E

#include "modules.h"                                                 /* DEPS */
#include "qlua.h"                                                    /* DEPS */
#include "qcomplex.h"                                                /* DEPS */
#include "qvector.h"                                                 /* DEPS */
#include "lattice.h"                                                 /* DEPS */
#include "qlayout.h"                                                 /* DEPS */
#include "latreal.h"                                                 /* DEPS */
#include "latcomplex.h"                                              /* DEPS */
#include "latcolmat.h"                                               /* DEPS */
#include "latcolvec.h"                                               /* DEPS */
#include "latdirferm.h"                                              /* DEPS */
#include "latdirprop.h"                                              /* DEPS */
#include "crc32.h"                                                   /* DEPS */
#include "qend.h"                                                    /* DEPS */
#include "qlanczos.h"                                                /* DEPS */
#include "qmp.h"

#include <string.h>
#include <math.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

/* type for sizes and indices: need max > 2^31 with RAM wkspace >2GiB */
#define LONG_T  long long

/* XXX NOTES
    * some enum values are defined explicitly to be able to change 
      aux functions (len/size/etc) into define's and have them inlined 
    * to apply eo-prec matvec, double conversion is required: block<->qdp,qdp<->internal
 */
/*! error codes */
typedef enum {
    QLM_ERR_SUCCESS         = 0,
    QLM_ERR_ENOMEM,                 /* out of memory (malloc) */
    QLM_ERR_INVALID_VEC,            /* vec.index out of range */
    QLM_ERR_INVALID_FIELD,          /* on export in C functions */
    QLM_ERR_INVALID_STATE,          /* state is invalid for the requested action */
    QLM_ERR_INVALID_PARAM,          /* parameter is invalid for the requested action */
    QLM_ERR_TYPE_MISMATCH,          /* mismatch of metadata in func with >=2 qlmData's */
    QLM_ERR_GEOM_MISMATCH,          /* mismatch of metadata in func with >=2 qlmData's */
    QLM_ERR_GEOM_SITE,              /* geometry error : operation on a missing site */
    QLM_ERR_ENUM_ERROR              /* unsupported enum value (internal error) */
} qlmError;
/*! get string desribing the error */
const char *qlm_strerror(qlmError e);
/*! global error code */
extern qlmError qlm_error;  /* TODO init to QLM_ERR_SUCCESS */

/*! domain enum: real or complex */
typedef enum {
    QLM_REAL    = 1,
    QLM_CPLX    = 2
} qlmDomain;
/* precision enum: single or double */
typedef enum {
    QLM_PREC_NONE   = 0,
    QLM_PREC_SINGLE = 4,
    QLM_PREC_DOUBLE = 8
} qlmPrec;
#define qlm_domain(d) (QLM_LATREAL==(d) ? QLM_REAL : QLM_CPLX)
#define qlm_num_reals(d) (QLM_REAL==(d) ? 1 : (QLM_CPLX ==(d) ? 2 : 0))
#define qlm_real_size(p) (QLM_PREC_SINGLE==(p) ? 4 : (QLM_PREC_DOUBLE==(p)? 8 : 0))

/* do not use existing enum QLUA_Type because different nc refer to the same type */
/*! lattice field enum for interfacing with Qlua */
typedef enum {
    QLM_LAT_NONE        =  0,
    QLM_LATREAL         =  1,
    QLM_LATCOMPLEX      =  3,
    QLM_LATCOLVEC       =  5,
    QLM_LATCOLMAT       =  7,
    QLM_LATDIRFERM      = 11,
    QLM_LATDIRPROP      = 13
} qlmLatField;
/*! sublattice enum for interfacing with Qlua and qlop operator objects*/
typedef enum qlmLayout {
    QLM_SUBLAT_NONE     = 0,
    QLM_SUBLAT_EVEN     = 1,
    QLM_SUBLAT_ODD      = 2,
    QLM_SUBLAT_FULL     = 3
} qlmSublat;
#define SUBLAT_PARITY(x) (QLM_SUBLAT_EVEN == (x) ? 0 : 1)
#define SUBLAT_EOPC(p) (0 == (p&1) ? QLM_SUBLAT_EVEN : QLM_SUBLAT_ODD)
#define IS_SUBLAT_EOPC(x) (QLM_SUBLAT_EVEN == (x) || QLM_SUBLAT_ODD == (x))
typedef enum {
    QLM_LAYOUT_NONE     = 0,
    QLM_LAYOUT_XLEX     = 1,
    QLM_LAYOUT_BLKLEX   = 2,
    QLM_LAYOUT_QDPFULL  = 3
} qlmLayout;
#define IS_LAYOUT_BLK(X) (QLM_LAYOUT_BLKLEX == (x))


/* storage of sites: site data is contiguous; complex numbers are contiguous
    * QLM_LAYOUT_QDP: QDP site order, only for QLM_SUBLAT_FULL
    * QLM_LAYOUT_BLOCK: blocked layout (mg, evec compression ; blocks must be contiguous for BLAS)
      + local dimensions must be divisible by block
      + if not QLM_SUBLAT_FULL, then `blocksize[0]' must be even; otherwise, blocks have different lengths
      + storage of blocks is lexicographic 
      + sites within block are lexicographic
   site data

   overall index for dfa [ivec, iblock, isite, i5, is, ic]
 */


/*! column-major (Fortran) matrix with float|double real|complex data
    if !is_blocked: 
        size=nvec * vec_size
        [jvec(j), {isite,iarr,is^?,ic^?}(q)]
    else (is_blocked): 
        size=nvec_basis * vec_size
        [ivec(m), iblock_lex(b), {isiteb,iarr,is^?,ic^?}(q)]

    ivec        (basis) vector index
    iblock_lex  lex-linear block index              [QLM_LAYOUT_BLOCK]
    isiteb      lex-linear site index within block  [QLM_LAYOUT_BLOCK]
    isite       lex-linear index within loc.vol
    iarr        array index(ie 5th dim for DWF)
    is, ic      spin/color indices            

    len(iblock_lex(b))  = vec_blk_len
    len(jvec(j))        = nvec
    len(q)              = blk_num_len
    NOTE if !is_blocked, init as if loc.vol==block for generality: 
        vec_blk_len=1, blk_num_len=vec_num_len for generality 
 */
typedef struct {
    mLattice    *S;         /*!< associated lattice ; masks are ignored */
    qlmLatField lftype;     /*!< corresponding lattice type */
    qlmSublat   sublat;     /*!< subset of sites */
    qlmDomain   domain;     /*!< domain: deduced from lftype */
    qlmPrec     prec;       /*!< precision */
    qlmLayout   layout;     /*!< layout of site/internal */
    
    /* latfield and layout-specific parameters */
    int         ns, nc;     /*!< color, spin, number of fields in lftype
                                 for import/export */
    int         int_nb;     /*! number of "internal" blocks */
    /* array */
    int         is_array;   /*!< whether vector is an array of lattice fields */
    int         arr_len;    /*!< length of array or 1 */
    int         arr_block, arr_nb;  /*! blocking and # of blocks in the "array" dimension" */

    /* geometry */
    int         ndim;       /*! must match S->rank */
    /* "private" geometry vars */
    int         vol_site_dim[QLUA_MAX_LATTICE_RANK]; /*!< local vol dim and len (sites) */
    LONG_T      vol_site_len;       /*!< == number of sites  / local volume (*0.5 if eoprec) */
    int         vol_blk_dim[QLUA_MAX_LATTICE_RANK]; /*!< local vol dim and len (blocks) */
    LONG_T      vol_blk_len;        /*!< vol_block_len = prod(vol_block_dim) */
    int         blk_site_dim[QLUA_MAX_LATTICE_RANK]; /*!< block dim and len(sites) [QLM_LAYOUT_BLOCK] */
    LONG_T      blk_site_len;       /*!< == number of sites  / block (eoprec taken into account) */
    int         x0[QLUA_MAX_LATTICE_RANK];  /*! initial coordinate of local vec */
    int         x0_parity;                  /*! parity of initial coordinate [!QLM_SUBLAT_FULL] */

    /* for the purpose of notations, 
        site = 1 internal block * 1 array block * 1 coord site
        elem = 1 internal block * 1 coord site
     */
    LONG_T      site_elem_len;
    LONG_T      blk_elem_len;       /*!< == number of "elements" / block */
    LONG_T      vec_elem_len;       /*!< == QLM_SUBLAT_FULL ? prod(vec_site_dim) : prod(vec_site_dim)/2 */
    LONG_T      vec_blk_len;        /*!< blocks / vector (may be different from blocks/vol 
                                         if array- OR internal-blocked) */

    LONG_T      elem_num_len,       /*!< num/elem, == field_num_len / int_nb */
                site_num_len,       /*!< num/site within block, == field_num_len / int_nb * arr_block */
                blk_num_len,        /*!< num/block, == elem_num_len * blk_elem_len */
                vec_num_len;        /*!< num/vec, == site_num_len * vec_site_len, vec. stride in vec_data */

    LONG_T      num_size,           /*!< bytes/num, 4,8,8,16 for single|double, real|complex */
                elem_size,          /*!< bytes/elem, == num_size * elem_num_len */
                site_size,          /*!< bytes/site within block */
                blk_size,           /*!< bytes/block, == num_size * blk_num_len */
                vec_size;           /*!< == num_size * vec_num_len */

/* hierarchical hypercubic geometry : Sites \in Blocks \in Mesh \in Lattice;
 * for X,Y = {s,b,v,l}
 *      XY_dim[]    dimension
 *      XY_len      length = prod(dim), does not care about parity
 *      XY_coord[]  coordinates, 0 <= XY_coord[i] < XY_dim[i]
 *      XY_ind      lexicographic index
 *      XY_ind_eo   
 *
 * constraints: 
 *  sb_dim divides sm_dim
 *  sv_dim divides sl_dim
 *  sb_len is even, => neven==nodd sites in any block
 *  can convert index<->coord only on the node, because QDP_layout is stored as a table 
 */
/* TODO replace geometry with the following (put into a separate struct? */
/* XXX replace 'len' with 'stride' ? */
//    int         sv_dim[QLUA_MAX_LATTICE_RANK],
//                sb_dim[QLUA_MAX_LATTICE_RANK],
//                bv_dim[QLUA_MAX_LATTICE_RANK];
//    int         sv_len,
//                sb_len, 
//                bv_len;
//    int         ns_len, nb_len, nv_len;
//    int         v_size, b_size, s_size, n_size;

} qlmVecLayout ;


typedef struct {
    qlmVecLayout vl;            /*!< vector element access */
    int         nvec;           /*!< number of stored vectors */
    int         last_nvec;  /*! index of the last inserted vector +1 OR zero
                                modified by insert/keep/drop functions, 
                                but not used by them by default 
                                XXX semantics is not yet clear, use with caution */
    /* block-basis */
    int         is_blocked;     /*!< whether vec_data is a block-basis */
    int         nvec_basis;     /*!< is_blocked? number of block-basis vectors
                                     ==nvec otherwise */
    LONG_T      cblkvec_num_len;    /*!< == nvec_basis * vec_blk_len, vec. stride in blk_coeff */
    LONG_T      cblkvec_size;       /*!< == num_size * cblkvec_num_len */
    LONG_T      cblk_num_len;       /*!< == vec_blk_len, block coeffs for 1 set of blocks */
    LONG_T      cblk_size;          /*!< == num_size * vec_blk_len */
    LONG_T      grbuf_size;         /*!< == global reductions buffer size */

    void        *vec_data;                          

    /*! if is_blocked, coeffs of vectors in the block-basis
        NULL otherwise 
        [jvec(j), ivec(m), iblock_lex(b)]
     */
    void        *blk_coeff;     /*!< is_blocked? vec.coeffs in the block-basis
                                     ==NULL otherwise */
    /* workspace */
    void *workM;    /*!< workspace(prec), vec_size */
    void *workN;    /*!< workspace(prec), nvec * num_size */
    void *work_blk; /*!< is_blocked? workspace(prec), cblkvec_size 
                         ==NULL otherwise */
    void *grbuf;    /*!< global reductions buffer, double */
} qlmData;

const char *qlm_lftype_str(qlmLatField lftype);

int qlm_veclayout_init(
        qlmVecLayout *vl,
        mLattice *S, 
        qlmSublat sublat, 
        qlmLatField lftype, 
        int is_array, 
        int arr_len, 
        int ns, 
        int nc,
        qlmPrec prec, 
        int is_blocked, 
        const int blk_site_dim[], 
        int arr_block,
        int int_nb);
const char *qlm_veclayout_cmp(const qlmVecLayout *vl,
    mLattice *S,                /* if dnc: set NULL */
    qlmSublat sublat,           /* if dnc: set QLM_SUBLAT_NONE */
    qlmLatField lftype,         /* if dnc: set QLM_LAT_NONE */
    int is_array,               /* if dnc: set -1; arr_len is ignored */
    int arr_len,                /* if dnc: set -1 */
    int ns, int nc,             /* if dnc: set -1 */
    qlmPrec prec,               /* if dnc: set QLM_PREC_NONE */
    const int *blk_site_dim,    /* if dnc: set NULL */
    int arr_block, int int_nb   /* if dnc: set -1 */
    );

qlmData *qlm_data_alloc(
        mLattice *S, qlmSublat sublat, int nvec, qlmLatField lftype, 
        int is_array, int arr_len,
        int ns, int nc, qlmPrec prec, 
        int is_blocked, const int blk_site_dim[], int arr_block, 
        int int_nb, int nvec_basis);

void qlm_data_free(qlmData *qlm);

/* linear algebra */
int qlm_latmv_block_(qlmData *d, 
        const void *restrict alpha, const void *restrict lat_x_,
        const void *restrict beta, void *restrict y_);
int qlm_latmv_(qlmData *d, 
        const void *restrict alpha, const void *restrict lat_x_,
        const void *restrict beta, void *restrict y_);
int qlm_latmxv_block_(qlmData *d, 
        const void *restrict alpha, const void *restrict lat_x_,
        const void *restrict beta, void *restrict y_);
int qlm_latmxv_(qlmData *d, 
        const void *restrict alpha, const void *restrict lat_x_,
        const void *restrict beta, void *restrict y_);
int qlm_latop_diag_(qlmData *d, 
        const void *restrict alpha, int n_eval, const void *restrict eval, 
        const void *lat_src_, const void *restrict beta, void *lat_dst_);

/* wrap qlmData in case some Qlua-related fields are required later */
typedef struct { 
    qlmData     *d;
} mQlm;
mQlm *qlua_checkQlm(lua_State *L, int idx, mLattice *S);
mQlm *qlua_newQlm(lua_State *L, int Sidx, qlmData *d);

int init_qlm(lua_State *L);
void fini_qlm(void);

extern float          sZero, sOne, sMone;
extern double         dZero, dOne, dMone;
extern float complex  cZero, cOne, cMone;
extern double complex zZero, zOne, zMone;
#define qlm_prec_sw(vl,as,ad,ax) (QLM_PREC_SINGLE==(vl)->prec ? (as) : (QLM_PREC_DOUBLE==(vl)->prec ? (ad) : (ax)))
#define qlm_domain_sw(vl,ar,ac,ax) (QLM_REAL==(vl)->domain ? (ar) : (QLM_CPLX==(vl)->domain ? (ac) : (ax)))
#define qlm_pzero(vl) qlm_domain_sw(vl,\
        qlm_prec_sw(vl,(const void *)&sZero,(const void *)&dZero,NULL),\
        qlm_prec_sw(vl,(const void *)&cZero,(const void *)&zZero,NULL),NULL)
#define qlm_pone(vl)  qlm_domain_sw(vl,\
        qlm_prec_sw(vl,(const void *)&sOne, (const void *)&dOne, NULL),\
        qlm_prec_sw(vl,(const void *)&cOne, (const void *)&zOne, NULL),NULL)
#define qlm_pmone(vl) qlm_domain_sw(vl,\
        qlm_prec_sw(vl,(const void *)&sMone,(const void *)&dMone,NULL),\
        qlm_prec_sw(vl,(const void *)&cMone,(const void *)&zMone,NULL),NULL)


#endif/*QLM_H__DAB3D8AA_24DB_4C80_8268_62DEB1688C8E*/

