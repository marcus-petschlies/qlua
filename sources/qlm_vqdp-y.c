/* meaning of arguments is described in qlm_layout-x.c */
static void
Qppc(fc_qla2int_V)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    QT(_ColorVector) *qx0 = (QT(_ColorVector) *)src_ + src_idx;
    pcint *ix0 = (pcint *)dst_ + dst_idx * vl->elem_num_len;
    for (int c = 0 ; c < Qnc ; c++) {
        QT(_Complex) *qx = &QTc(_elem_V)(*qx0, c);
        pcint *ix = ix0 + QLM_INDEX_V(c, Qnc);
        cplx_qla2int(*ix, *qx);
    }
}
static void
Qppc(fc_int2qla_V)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    pcint *ix0 = (pcint *)src_ + src_idx * vl->elem_num_len;
    QT(_ColorVector) *qx0 = (QT(_ColorVector) *)dst_ + dst_idx;
    for (int c = 0 ; c < Qnc ; c++) {
        pcint *ix = ix0 + QLM_INDEX_V(c, Qnc);
        QT(_Complex) *qx = &QTc(_elem_V)(*qx0, c);
        cplx_int2qla(*qx, *ix);
    }
}
static void
Qppc(fc_qla2int_M)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    QT(_ColorMatrix) *qx0 = (QT(_ColorMatrix) *)src_ + src_idx;
    pcint *ix0 = (pcint *)dst_ + dst_idx * vl->elem_num_len;
    for (int c = 0 ; c < Qnc ; c++) for (int c2 = 0 ; c2 < Qnc ; c2++) {
        QT(_Complex) *qx = &QTc(_elem_M)(*qx0, c,c2);
        pcint *ix = ix0 + QLM_INDEX_M(c,c2, Qnc);
        cplx_qla2int(*ix, *qx);
    }
}
static void
Qppc(fc_int2qla_M)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    pcint *ix0 = (pcint *)src_ + src_idx * vl->elem_num_len;
    QT(_ColorMatrix) *qx0 = (QT(_ColorMatrix) *)dst_ + dst_idx;
    for (int c = 0 ; c < Qnc ; c++) for (int c2 = 0 ; c2 < Qnc ; c2++) {
        pcint *ix = ix0 + QLM_INDEX_M(c,c2, Qnc);
        QT(_Complex) *qx = &QTc(_elem_M)(*qx0, c,c2);
        cplx_int2qla(*qx, *ix);
    }
}
static void
Qppc(fc_qla2int_D)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    QT(_DiracFermion) *qx0 = (QT(_DiracFermion) *)src_ + src_idx;
    pcint *ix0 = (pcint *)dst_ + dst_idx * vl->elem_num_len;
    for (int c = 0 ; c < Qnc ; c++) for (int s = 0 ; s < Qns ; s++) {
        QT(_Complex) *qx = &QTc(_elem_D)(*qx0, c, s);
        pcint *ix = ix0 + QLM_INDEX_D(c,s, Qnc,Qns);
        cplx_qla2int(*ix, *qx);
    }
}
static void
Qppc(fc_int2qla_D)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    pcint *ix0 = (pcint *)src_ + src_idx * vl->elem_num_len;
    QT(_DiracFermion) *qx0 = (QT(_DiracFermion) *)dst_ + dst_idx;
    for (int c = 0 ; c < Qnc ; c++) for (int s = 0 ; s < Qns ; s++) {
        pcint *ix = ix0 + QLM_INDEX_D(c,s, Qnc,Qns);
        QT(_Complex) *qx = &QTc(_elem_D)(*qx0, c, s);
        cplx_int2qla(*qx, *ix);
    }
}

static void
Qppc(fc_qla2int_P)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    QT(_DiracPropagator) *qx0 = (QT(_DiracPropagator) *)src_ + src_idx;
    pcint *ix0 = (pcint *)dst_ + dst_idx * vl->elem_num_len;
    for (int c = 0 ; c < Qnc ; c++ ) for (int s = 0 ; s < Qns ; s++ ) 
    for (int c2= 0 ; c2< Qnc ; c2++) for (int s2= 0 ; s2< Qns ; s2++) {
        QT(_Complex) *qx = &QTc(_elem_P)(*qx0, c,s,c2,s2);
        pcint *ix = ix0 + QLM_INDEX_P(c,s,c2,s2, Qnc,Qns);
        cplx_qla2int(*ix, *qx);
    }
}
static void
Qppc(fc_int2qla_P)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    pcint *ix0 = (pcint *)src_ + src_idx * vl->elem_num_len;
    QT(_DiracPropagator) *qx0 = (QT(_DiracPropagator) *)dst_ + dst_idx;
    for (int c = 0 ; c < Qnc ; c++) for (int s = 0 ; s < Qns ; s++)
    for (int c2= 0 ; c2< Qnc ; c2++) for (int s2= 0 ; s2< Qns ; s2++) {
        pcint *ix = ix0 + QLM_INDEX_P(c,s,c2,s2, Qnc,Qns);
        QT(_Complex) *qx = &QTc(_elem_P)(*qx0, c,s,c2,s2);
        cplx_int2qla(*qx, *ix);
    }
}

#undef Qnc
#undef Qns
#undef Qppc
#undef QTc
