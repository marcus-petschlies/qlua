#include "qlm.h"                                                     /* DEPS */
#include "qlm_layout.h"                                              /* DEPS */
#include <assert.h>
/****************************************************************************/
/* site layout functions: conversion between coord and linear indices 
   GLOSSARY
    x2i     coordinate to index
    i2x     index to coordinate
    eo      even/odd (can take parity information from qlmData)

    dim     array of dimensions
    coord   array of coordinates, [0; dim)
    len     length of storage indexed by `idx'
    idx     linear index, [0; len)
    XY
 */

/* aux functions: coord <-> lexicographic index */
void
index_i2x(int ndim, const int *restrict dim, int *restrict x, LONG_T ilex)
{
    for (int i = 0 ; i < ndim ; i++) {
        x[i]    = ilex % dim[i];
        ilex   /= dim[i];
    }
}
void 
index_x2i(int ndim, const int *restrict dim, LONG_T *restrict ilex, const int *restrict x)
{
    LONG_T res = 0;
    for (int i = ndim ; i-- ;)
        res = res * dim[i] + x[i];
    *ilex = res;
}
void
/* aux functions: coord <-> lexicographic index, even/odd */
index_i2x_eo(int ndim, const int *restrict dim, int *restrict x, LONG_T ilex_eo, int par)
{
    LONG_T ilex = 2 * ilex_eo;
    int p = 0;
    for (int i = 0 ; i < ndim ; i++) {
        x[i]    = ilex % dim[i];
        p      += x[i];
        ilex   /= dim[i];
    }
    if ((p & 1) ^ (par & 1)) {  /* wrong parity */
        ilex = 2 * ilex_eo + 1;
        for (int i = 0 ; i < ndim ; i++) {
            x[i]    = ilex % dim[i];
            ilex   /= dim[i];
        }
    }
}
void 
index_x2i_eo(int ndim, const int *restrict dim, LONG_T *restrict ilex, int *restrict par, const int *restrict x)
{
    LONG_T res = 0;
    int p = 0;
    for (int i = ndim ; i-- ;) {
        res = res * dim[i] + x[i];
        p   = p + x[i];
    }
    *par    = p & 1;
    *ilex   = res / 2;
}

/* qdp lattice site layout */
void 
site_layout_x2i_qdp(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl)
{
    int ls_coord[QLUA_MAX_LATTICE_RANK];
    for (int i = 0 ; i < vl->ndim; i++)
        ls_coord[i] = vl->x0[i] + vs_coord[i];
    /* TODO check coordinate range? */
    if (QDP_node_number_L(vl->S->lat, ls_coord) != QDP_this_node)
        *vs_idx = -1;
    else
        *vs_idx = QDP_index_L(vl->S->lat, ls_coord);
}
void 
site_layout_i2x_qdp(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl)
{
    int ls_coord[QLUA_MAX_LATTICE_RANK];
    QDP_get_coords_L(vl->S->lat, ls_coord, QDP_this_node, vs_idx);
    for (int i = 0 ; i < vl->ndim; i++)
        vs_coord[i] = ls_coord[i] - vl->x0[i];
}

/* lexicographic site layout - default for FULL sublat */
void 
site_layout_x2i_lex(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl)
{
    assert(QLM_SUBLAT_FULL == vl->sublat);
    index_x2i(vl->ndim, vl->vol_site_dim, vs_idx, vs_coord);
}
void 
site_layout_i2x_lex(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl)
{
    assert(QLM_SUBLAT_FULL == vl->sublat);
    index_i2x(vl->ndim, vl->vol_site_dim, vs_coord, vs_idx);
}


/* blocked site layout */
void
site_layout_x2i_blk(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl)
{
    assert(QLM_SUBLAT_FULL == vl->sublat);
    int vb_coord[QLUA_MAX_LATTICE_RANK],    /* block in node */
        bs_coord[QLUA_MAX_LATTICE_RANK];    /* site in block */
    for (int i = 0 ; i < vl->ndim ; i++) {
        bs_coord[i] = vs_coord[i] % vl->blk_site_dim[i];
        vb_coord[i] = vs_coord[i] / vl->blk_site_dim[i];
    }
    LONG_T vb_idx, bs_idx;
    index_x2i(vl->ndim, vl->vol_blk_dim, &vb_idx, vb_coord);
    index_x2i(vl->ndim, vl->blk_site_dim, &bs_idx, bs_coord);
    *vs_idx = bs_idx + vl->blk_site_len * vb_idx;
}
void
site_layout_i2x_blk(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl)
{
    assert(QLM_SUBLAT_FULL == vl->sublat);
    int vb_coord[QLUA_MAX_LATTICE_RANK],    /* block in node */
        bs_coord[QLUA_MAX_LATTICE_RANK];    /* site in block */
    LONG_T vb_idx, bs_idx;
    bs_idx  = vs_idx % vl->blk_site_len;
    vb_idx  = vs_idx / vl->blk_site_len;
    index_i2x(vl->ndim, vl->vol_blk_dim, vb_coord, vb_idx);
    index_i2x(vl->ndim, vl->blk_site_dim, bs_coord, bs_idx);
    for (int i = 0 ; i < vl->ndim ; i++)
        vs_coord[i] = bs_coord[i] + vl->blk_site_dim[i] * vb_coord[i];
}


/* lexicographic site layout, even/odd */
void 
site_layout_x2i_lex_eo(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl)
{
    assert(IS_SUBLAT_EOPC(vl->sublat));
    int vs_par;
    index_x2i_eo(vl->ndim, vl->vol_site_dim, vs_idx, &vs_par, vs_coord);
    if (SUBLAT_PARITY(vl->sublat) != (vs_par ^ vl->x0_parity))
        *vs_idx = 0;
}
void 
site_layout_i2x_lex_eo(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl)
{
    assert(IS_SUBLAT_EOPC(vl->sublat));
    index_i2x_eo(vl->ndim, vl->vol_site_dim, vs_coord, vs_idx, 
            SUBLAT_PARITY(vl->sublat) ^ vl->x0_parity);
}


/* blocked site layout, even/odd */
void
site_layout_x2i_blk_eo(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl)
{
    assert(IS_SUBLAT_EOPC(vl->sublat));
    int vb_coord[QLUA_MAX_LATTICE_RANK],    /* block in node */
        bs_coord[QLUA_MAX_LATTICE_RANK];    /* site in block */
    int b0_par = vl->x0_parity;              /* parity of LO blk corner */
    for (int i = 0 ; i < vl->ndim ; i++) {
        bs_coord[i] = vs_coord[i] % vl->blk_site_dim[i];
        vb_coord[i] = vs_coord[i] / vl->blk_site_dim[i];
        b0_par     ^= (vb_coord[i] * vl->blk_site_dim[i]) & 1;
    }
    LONG_T vb_idx, bs_idx;
    int bs_par;
    index_x2i(vl->ndim, vl->vol_blk_dim, &vb_idx, vb_coord);
    index_x2i_eo(vl->ndim, vl->blk_site_dim, &bs_idx, &bs_par, bs_coord);
    if (SUBLAT_PARITY(vl->sublat) != (b0_par ^ bs_par))
        *vs_idx = -1;   /* not in sublat */
    else
        *vs_idx = bs_idx + vl->blk_site_len * vb_idx;
}
void
site_layout_i2x_blk_eo(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl)
{
    assert(IS_SUBLAT_EOPC(vl->sublat));
    int vb_coord[QLUA_MAX_LATTICE_RANK],    /* block in node */
        bs_coord[QLUA_MAX_LATTICE_RANK];    /* site in block */
    LONG_T vb_idx, bs_idx;
    int bs_par = vl->x0_parity ^ SUBLAT_PARITY(vl->sublat);
    bs_idx  = vs_idx % vl->blk_site_len;
    vb_idx  = vs_idx / vl->blk_site_len;
    index_i2x(vl->ndim, vl->vol_blk_dim, vb_coord, vb_idx);
    for (int i = 0 ; i < vl->ndim ; i++)
        bs_par ^= (vb_coord[i] * vl->blk_site_dim[i]) & 1;
    index_i2x_eo(vl->ndim, vl->blk_site_dim, bs_coord, bs_idx, bs_par);
    for (int i = 0 ; i < vl->ndim ; i++)
        vs_coord[i] = bs_coord[i] + vl->blk_site_dim[i] * vb_coord[i];
}

/*! switch to select layout function x2i
    TODO can block only by coordinate now
 */
site_layout_x2i 
get_site_layout_x2i(const qlmVecLayout *vl, qlmLayout layout)
{
    if (QLM_LAYOUT_NONE == layout) 
        layout = vl->layout;
    switch (layout) {
    case QLM_LAYOUT_XLEX:
        if (QLM_SUBLAT_FULL == vl->sublat)   return site_layout_x2i_lex;
        else if (IS_SUBLAT_EOPC(vl->sublat)) return site_layout_x2i_lex_eo;
        break;
    case QLM_LAYOUT_BLKLEX:
        if (QLM_SUBLAT_FULL == vl->sublat)   return site_layout_x2i_blk;
        else if (IS_SUBLAT_EOPC(vl->sublat)) return site_layout_x2i_blk_eo;
        break;
    case QLM_LAYOUT_QDPFULL:
        /* TODO check that sublat==full ? */
        return site_layout_x2i_qdp;
        break;
    default:
        break;
    }
    qlm_error = QLM_ERR_ENUM_ERROR;
    return NULL;
}

/*! switch to select layout function i2x
    TODO can block only by coordinate now
 */
site_layout_i2x 
get_site_layout_i2x(const qlmVecLayout *vl, qlmLayout layout)
{
    if (QLM_LAYOUT_NONE == layout) 
        layout = vl->layout;
    switch (layout) {
    case QLM_LAYOUT_XLEX:
        if (QLM_SUBLAT_FULL == vl->sublat)   return site_layout_i2x_lex;
        else if (IS_SUBLAT_EOPC(vl->sublat)) return site_layout_i2x_lex_eo;
        break;
    case QLM_LAYOUT_BLKLEX:
        if (QLM_SUBLAT_FULL == vl->sublat)   return site_layout_i2x_blk;
        else if (IS_SUBLAT_EOPC(vl->sublat)) return site_layout_i2x_blk_eo;
        break;
    case QLM_LAYOUT_QDPFULL:
        /* TODO check that sublat==full ? */
        return site_layout_i2x_qdp;
        break;
    default:
        break;
    }
    qlm_error = QLM_ERR_ENUM_ERROR;
    return NULL;
}

int 
qlm_elem_x2i(
        const qlmVecLayout *vl, 
        LONG_T *ind, 
        const int *vs_coord, int iarr, int i_ib) 
{
    LONG_T i_x; /* FIXME SITEIDX_T */
    get_site_layout_x2i(vl, QLM_LAYOUT_NONE)(&i_x, vs_coord, vl);
    if (-1 == i_x) {
        *ind = -1;
        return 1;
    }
    *ind = QLM_INDEX_ELEM(vl, 
            i_x / vl->blk_site_len, i_x % vl->blk_site_len, 
            iarr / vl->arr_block, iarr % vl->arr_block, i_ib);
    return 0;
}


/*! generate IO map ordered by file and in-file index assuming rlex ordering 
    of sites in file and on node
    rlex ordering: i_site = c[0] + L[0]*(c[1] + L[1]*(.... + c[n-1] )..)

    required conditions:
    lat_geom[i] % net_geom[i]
    lat_geom[i] % file_geom[i]

    XXX allocates three arrays, the caller must free
 */
int 
qlm_iogeom_map_lex(int ndim, 
        const int lat_dim[],        /* size of lat in sites */
        const int lat_vdst_dim[],   /* size of lat in vdst units */
        const int lat_vdst_c[],     /* vdst coord in lat */
        const int lat_vsrc_dim[],   /* size of lat in vsrc units */
        LONG_T *map_len,
        LONG_T **p_vdst_i,          /* table of indices in dst volume */
        LONG_T **p_vsrc_i,          /* table of indices in src volumes */
        LONG_T **p_vsrc_v           /* list of src volumes */
        )
{
    int vdst_c[QLUA_MAX_LATTICE_RANK],
        vdst_c0[QLUA_MAX_LATTICE_RANK],
        vdst_dim[QLUA_MAX_LATTICE_RANK],
        vsrc_c[QLUA_MAX_LATTICE_RANK],
        lat_vsrc_c[QLUA_MAX_LATTICE_RANK],
        vsrc_dim[QLUA_MAX_LATTICE_RANK];
    LONG_T vdst_vol = 1;
    for (int k = 0 ; k < ndim ; k++) {
        assert(0 ==   lat_dim[k] % lat_vsrc_dim[k]);
        vsrc_dim[k] = lat_dim[k] / lat_vsrc_dim[k];
        assert(0 ==   lat_dim[k] % lat_vdst_dim[k]);
        vdst_dim[k] = lat_dim[k] / lat_vdst_dim[k];
        vdst_vol    *= vdst_dim[k];
        vdst_c0[k]  = vdst_dim[k] * lat_vdst_c[k] ;
    }

    if (NULL != map_len) 
        *map_len = vdst_vol;
    LONG_T *vdst_i = *p_vdst_i = malloc(vdst_vol * sizeof(LONG_T));
    LONG_T *vsrc_i = *p_vsrc_i = malloc(vdst_vol * sizeof(LONG_T));
    LONG_T *vsrc_v = *p_vsrc_v = malloc(vdst_vol * sizeof(LONG_T));
    if (    NULL == *p_vdst_i ||
            NULL == *p_vsrc_i ||
            NULL == *p_vsrc_v) {
        free_not_null(*p_vdst_i);
        free_not_null(*p_vsrc_i);
        free_not_null(*p_vsrc_v);
        return 1;
    }
    /* generate map of indices ordered by destination */
    for (LONG_T i_vdst = 0 ; i_vdst < vdst_vol ; i_vdst++) {
        vdst_i[i_vdst] = i_vdst;
        index_i2x(ndim, vdst_dim, vdst_c, i_vdst);
        for (int k = 0 ; k < ndim ; k++) {
            int c = vdst_c0[k] + vdst_c[k];
            vsrc_c[k]    = c % vsrc_dim[k];
            lat_vsrc_c[k]= c / vsrc_dim[k];
        }
        index_x2i(ndim, vsrc_dim,     vsrc_i +i_vdst, vsrc_c);
        index_x2i(ndim, lat_vsrc_dim, vsrc_v +i_vdst, lat_vsrc_c);
    }
    /* simple sort by {vsrc_v,vsrc_i} */
    for (int i = vdst_vol ; --i ; )
        for (int j = 0 ; j < i ; j++) {
            if (vsrc_v[i] < vsrc_v[j] || 
                (vsrc_v[i] == vsrc_v[j] && 
                 vsrc_i[i] < vsrc_i[j])) {
                LONG_T a;
                a = vdst_i[i] ; vdst_i[i] = vdst_i[j] ; vdst_i[j] = a;
                a = vsrc_i[i] ; vsrc_i[i] = vsrc_i[j] ; vsrc_i[j] = a;
                a = vsrc_v[i] ; vsrc_v[i] = vsrc_v[j] ; vsrc_v[j] = a;
            }
        }
    
    return 0;
}


