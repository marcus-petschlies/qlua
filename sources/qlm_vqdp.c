#include "qlm.h"                                                     /* DEPS */
#include "qlm_vqdp.h"                                                /* DEPS */
#include <assert.h>

void *
qdp1_create(mLattice *S, qlmLatField lftype, int nc)
{ 
    switch(lftype) {
    case QLM_LATREAL:       return (void *)QDP_D_create_R_L(S->lat);
    case QLM_LATCOMPLEX:    return (void *)QDP_D_create_C_L(S->lat);
    case QLM_LATCOLVEC:     
        if     (2 == nc)    return (void *)QDP_D2_create_V_L(S->lat);
        else if(3 == nc)    return (void *)QDP_D3_create_V_L(S->lat);
        else                return (void *)QDP_DN_create_V_L(nc, S->lat);
    case QLM_LATCOLMAT:     
        if     (2 == nc)    return (void *)QDP_D2_create_M_L(S->lat);
        else if(3 == nc)    return (void *)QDP_D3_create_M_L(S->lat);
        else                return (void *)QDP_DN_create_M_L(nc, S->lat);
    case QLM_LATDIRFERM:
        if     (2 == nc)    return (void *)QDP_D2_create_D_L(S->lat);
        else if(3 == nc)    return (void *)QDP_D3_create_D_L(S->lat);
        else                return (void *)QDP_DN_create_D_L(nc, S->lat);
    case QLM_LATDIRPROP:
        if     (2 == nc)    return (void *)QDP_D2_create_P_L(S->lat);
        else if(3 == nc)    return (void *)QDP_D3_create_P_L(S->lat);
        else                return (void *)QDP_DN_create_P_L(nc, S->lat);
    default:
        qlm_error = QLM_ERR_INVALID_FIELD;
        return NULL;
    }
    return NULL; 
}

void
qdp1_destroy(qlmLatField lftype, int nc, void *x)
{
    switch(lftype) {
    case QLM_LATREAL:       QDP_D_destroy_R((QDP_D_Real *)x);               break;
    case QLM_LATCOMPLEX:    QDP_D_destroy_C((QDP_D_Complex *)x);            break;
    case QLM_LATCOLVEC:     
        if     (2 == nc)    QDP_D2_destroy_V((QDP_D2_ColorVector *)x);
        else if(3 == nc)    QDP_D3_destroy_V((QDP_D3_ColorVector *)x);
        else                QDP_DN_destroy_V((QDP_DN_ColorVector *)x);      
        break;
    case QLM_LATCOLMAT:     
        if     (2 == nc)    QDP_D2_destroy_M((QDP_D2_ColorMatrix *)x);
        else if(3 == nc)    QDP_D3_destroy_M((QDP_D3_ColorMatrix *)x);
        else                QDP_DN_destroy_M((QDP_DN_ColorMatrix *)x);      
        break;
    case QLM_LATDIRFERM:
        if     (2 == nc)    QDP_D2_destroy_D((QDP_D2_DiracFermion *)x);
        else if(3 == nc)    QDP_D3_destroy_D((QDP_D3_DiracFermion *)x);
        else                QDP_DN_destroy_D((QDP_DN_DiracFermion *)x);     
        break;
    case QLM_LATDIRPROP:
        if     (2 == nc)    QDP_D2_destroy_P((QDP_D2_DiracPropagator *)x);
        else if(3 == nc)    QDP_D3_destroy_P((QDP_D3_DiracPropagator *)x);
        else                QDP_DN_destroy_P((QDP_DN_DiracPropagator *)x);  
        break;
    default:
        qlm_error = QLM_ERR_INVALID_FIELD;
    }
}
void *
qdp1_expose(qlmLatField lftype, int nc, void *x)
{
    switch(lftype) {
    case QLM_LATREAL:       return (void *)QDP_D_expose_R((QDP_D_Real *)x);
    case QLM_LATCOMPLEX:    return (void *)QDP_D_expose_C((QDP_D_Complex *)x);
    case QLM_LATCOLVEC:     
        if     (2 == nc)    return (void *)QDP_D2_expose_V((QDP_D2_ColorVector *)x);
        else if(3 == nc)    return (void *)QDP_D3_expose_V((QDP_D3_ColorVector *)x);
        else                return (void *)QDP_DN_expose_V((QDP_DN_ColorVector *)x);
    case QLM_LATCOLMAT:     
        if     (2 == nc)    return (void *)QDP_D2_expose_M((QDP_D2_ColorMatrix *)x);
        else if(3 == nc)    return (void *)QDP_D3_expose_M((QDP_D3_ColorMatrix *)x);
        else                return (void *)QDP_DN_expose_M((QDP_DN_ColorMatrix *)x);
    case QLM_LATDIRFERM:
        if     (2 == nc)    return (void *)QDP_D2_expose_D((QDP_D2_DiracFermion *)x);
        else if(3 == nc)    return (void *)QDP_D3_expose_D((QDP_D3_DiracFermion *)x);
        else                return (void *)QDP_DN_expose_D((QDP_DN_DiracFermion *)x);
    case QLM_LATDIRPROP:
        if     (2 == nc)    return (void *)QDP_D2_expose_P((QDP_D2_DiracPropagator *)x);
        else if(3 == nc)    return (void *)QDP_D3_expose_P((QDP_D3_DiracPropagator *)x);
        else                return (void *)QDP_DN_expose_P((QDP_DN_DiracPropagator *)x);
    default:
        qlm_error = QLM_ERR_INVALID_FIELD;
        return NULL;
    }
    return NULL;
}
/* return value 0:OK, 1:Fail */
int
qdp1_reset(qlmLatField lftype, int nc, void *x)
{
    switch(lftype) {
    case QLM_LATREAL:       QDP_D_reset_R((QDP_D_Real *)x);                 break;
    case QLM_LATCOMPLEX:    QDP_D_reset_C((QDP_D_Complex *)x);              break;
    case QLM_LATCOLVEC:     
        if     (2 == nc)    QDP_D2_reset_V((QDP_D2_ColorVector *)x);     
        else if(3 == nc)    QDP_D3_reset_V((QDP_D3_ColorVector *)x);
        else                QDP_DN_reset_V((QDP_DN_ColorVector *)x);        
        break;
    case QLM_LATCOLMAT:
        if     (2 == nc)    QDP_D2_reset_M((QDP_D2_ColorMatrix *)x);     
        else if(3 == nc)    QDP_D3_reset_M((QDP_D3_ColorMatrix *)x);
        else                QDP_DN_reset_M((QDP_DN_ColorMatrix *)x);        
        break;
    case QLM_LATDIRFERM:
        if     (2 == nc)    QDP_D2_reset_D((QDP_D2_DiracFermion *)x);
        else if(3 == nc)    QDP_D3_reset_D((QDP_D3_DiracFermion *)x);
        else                QDP_DN_reset_D((QDP_DN_DiracFermion *)x);       
        break;
    case QLM_LATDIRPROP:
        if     (2 == nc)    QDP_D2_reset_P((QDP_D2_DiracPropagator *)x);
        else if(3 == nc)    QDP_D3_reset_P((QDP_D3_DiracPropagator *)x);
        else                QDP_DN_reset_P((QDP_DN_DiracPropagator *)x);    
        break;
    default:
        qlm_error = QLM_ERR_INVALID_FIELD;
        return 1;
    }
    return 0;
}

/*! push lattice field to the stack top and return (void *)(QDP_Field *) pointer 
    luastack:-0+1 */
void *
qdp1_push(lua_State *L, int Sidx, qlmLatField lftype, int nc)
{ 
    switch(lftype) {
    case QLM_LATREAL:       return (void *)(qlua_newZeroLatReal(L, Sidx)->ptr);
    case QLM_LATCOMPLEX:    return (void *)(qlua_newZeroLatComplex(L, Sidx)->ptr);
    case QLM_LATCOLVEC:     
        if     (2 == nc)    return (void *)(qlua_newZeroLatColVec2(L, Sidx, nc)->ptr);
        else if(3 == nc)    return (void *)(qlua_newZeroLatColVec3(L, Sidx, nc)->ptr);
        else                return (void *)(qlua_newZeroLatColVecN(L, Sidx, nc)->ptr);
    case QLM_LATCOLMAT:     
        if     (2 == nc)    return (void *)(qlua_newZeroLatColMat2(L, Sidx, nc)->ptr);
        else if(3 == nc)    return (void *)(qlua_newZeroLatColMat3(L, Sidx, nc)->ptr);
        else                return (void *)(qlua_newZeroLatColMatN(L, Sidx, nc)->ptr);
    case QLM_LATDIRFERM:
        if     (2 == nc)    return (void *)(qlua_newZeroLatDirFerm2(L, Sidx, nc)->ptr);
        else if(3 == nc)    return (void *)(qlua_newZeroLatDirFerm3(L, Sidx, nc)->ptr);
        else                return (void *)(qlua_newZeroLatDirFermN(L, Sidx, nc)->ptr);
    case QLM_LATDIRPROP:
        if     (2 == nc)    return (void *)(qlua_newZeroLatDirProp2(L, Sidx, nc)->ptr);
        else if(3 == nc)    return (void *)(qlua_newZeroLatDirProp3(L, Sidx, nc)->ptr);
        else                return (void *)(qlua_newZeroLatDirPropN(L, Sidx, nc)->ptr);
    default:
        qlm_error = QLM_ERR_INVALID_FIELD;
        return NULL;
    }
    return NULL; 
}

#if USE_Nc2
#define Q_NC2(x) x##2
#else 
#define Q_NC2(x) qlua_check_nocolor
#endif

#if USE_Nc3
#define Q_NC3(x) x##3
#else 
#define Q_NC3(x) qlua_check_nocolor
#endif

#if USE_NcN
#define Q_NCN(x) x##N
#else 
#define Q_NCN(x) qlua_check_nocolor
#endif

#define Qs(x,c) (2==(c) ? Q_NC2(x) : (3==(c) ? Q_NC3(x) : Q_NCN(x)))
/*! check that object on stack is compatible with S,lftype,nc 
    and return a QDP pointer */
void *
qdp1_check(lua_State *L, int idx, mLattice *S, qlmLatField lftype, int nc)
{
    switch(lftype) {
    case QLM_LATREAL:       return (void *)(qlua_checkLatReal(L, idx, S)->ptr);
    case QLM_LATCOMPLEX:    return (void *)(qlua_checkLatComplex(L, idx, S)->ptr);
    case QLM_LATCOLVEC:     
        if (2 == nc)        return (void *)(Q_NC2(qlua_checkLatColVec)(L, idx, S, nc)->ptr);
        else if(3 == nc)    return (void *)(Q_NC3(qlua_checkLatColVec)(L, idx, S, nc)->ptr);
        else                return (void *)(Q_NCN(qlua_checkLatColVec)(L, idx, S, nc)->ptr);
    case QLM_LATCOLMAT:     
        if (2 == nc)        return (void *)(Q_NC2(qlua_checkLatColMat)(L, idx, S, nc)->ptr);
        else if (3 == nc)   return (void *)(Q_NC3(qlua_checkLatColMat)(L, idx, S, nc)->ptr);
        else                return (void *)(Q_NCN(qlua_checkLatColMat)(L, idx, S, nc)->ptr);
    case QLM_LATDIRFERM:
        if (2 == nc)        return (void *)(Q_NC2(qlua_checkLatDirFerm)(L, idx, S, nc)->ptr);
        else if (3 == nc)   return (void *)(Q_NC3(qlua_checkLatDirFerm)(L, idx, S, nc)->ptr);
        else                return (void *)(Q_NCN(qlua_checkLatDirFerm)(L, idx, S, nc)->ptr);
    case QLM_LATDIRPROP:
        if (2 == nc)        return (void *)(Q_NC2(qlua_checkLatDirProp)(L, idx, S, nc)->ptr);
        else if (3 == nc)   return (void *)(Q_NC3(qlua_checkLatDirProp)(L, idx, S, nc)->ptr);
        else                return (void *)(Q_NCN(qlua_checkLatDirProp)(L, idx, S, nc)->ptr);
    default:
       luaL_error(L, "bad latfield type %d", lftype);
       return NULL;
    }
    return NULL;
}

/* check that a table contains QDP objects compatible with latmat 
   qlmData `is_array', `arr_len' are ignored - `len' is used for checks if 0 <= len
   returns an array of pointers in res_ if not NULL; must be able to accept 
   the pointers: either call with (len>0, res_=<ptr>) for existing storage
   or (len<=0, res_=NULL) to allocate storage, which must be deallocated externally
   returns a pointer to the storage
   FIXME make a struct with alloc/free to specifically handle (arrays of) QDP object ptrs? 
 */
void **
qdp1_check_array(lua_State *L, int idx, int len, void **res_, mLattice *S, qlmLatField lftype, int nc)
{
    luaL_checktype(L, idx, LUA_TTABLE);
    if (0 <= len && (long long)lua_objlen(L, idx) != len) {
        luaL_error(L, "expect array[%d] of %s", len, qlm_lftype_str(lftype));
        return NULL;
    }
    void **res = (NULL == res_ ? malloc(len * sizeof(void *)) : res_);
    if (NULL == res) {
        luaL_error(L, "not enough memory");
        return NULL;
    }
    lua_pushvalue(L, idx);          /* in case idx is < 0 or special */
    for (int i = 0 ; i < len ; i++) {
        lua_pushinteger(L, 1 + i);  /* sic! Lua indexing */
        lua_gettable(L, -2);
        res[i] = qdp1_check(L, -1, S, lftype, nc);
        lua_pop(L, 1);
    }
    lua_pop(L, 1);                  /* pop table ref copy */
    return res;
}



vqdp *
vqdp_alloc(qlmLatField lftype, int is_array, int arr_len, int nc)
{
    vqdp *res = NULL;
    if (!is_array) 
        arr_len = 1;
    assert(0 < arr_len);
    res = malloc(sizeof(vqdp) + arr_len * sizeof(*res->x));
    if (NULL == res)
        return NULL;
    res->lftype     = lftype;
    res->nc         = nc;
    res->is_array   = is_array;
    res->arr_len    = arr_len;
    res->is_exposed = 0;
    for (int i = 0 ; i < arr_len ; i++) {
        res->x[i].p = NULL;
        res->x[i].q = NULL;
    }
    return res;
}

void vqdp_reset(vqdp *vx);
/*! free vqdp object; qdp fields are not destroyed and remain on stack */
void
vqdp_free(vqdp *vx)
{
    if (NULL == vx) return;
    if (vx->is_exposed) 
        vqdp_reset(vx);
    free(vx);
}

#if 0 /* commented out because `vqdp' is only a presentation of 
         lattice objects stored on stack; should not be created except by 
         Lua-push operation or destroyed except by garbate collector */
/* create fields 
   return value 0:OK 1:fail (set qlm_error) */
int
vqdp_create(mLattice *S, vqdp *x)
{
    assert(NULL != x);
    if (x->is_array) {
        for (int i = 0 ; i < x->arr_len ; i++) {
            x->p.a[i] = qdp1_create(S, x->lftype, x->nc);
            if (NULL == x->p.a[i]) {
                for (int j = 0 ; j < i ; j++)
                    qdp1_destroy(x->lftype, x->nc, x->p.a[j]);
                qlm_error = QLM_ERR_ENOMEM; 
                return 1;
            }
        }
    }
    else {
        x->p.x = qdp1_create(S, x->lftype, x->nc);
        if (NULL == x->p.x) {
            qlm_error = QLM_ERR_ENOMEM; 
            return 1;
        }
    }
    x->is_exposed = 0;
    return 1;
}
/* destroy fields 
   return value 0:OK 1:fail (set qlm_error) */
void
vqdp_destroy(vqdp *x)
{
    assert(NULL != x);
    assert(0 == x->is_exposed);
    if (x->is_array) {
        for (int i = 0 ; i < x->arr_len ; i++)
            if (NULL != x->p.a[i]) 
                qdp1_destroy(x->lftype, x->nc, x->p.a[i]);
    }
    else
        if (NULL != x->p.x)
            qdp1_destroy(x->lftype, x->nc, x->p.x);
}
#endif

void
vqdp_expose(vqdp *vx)
{
    if (vx->is_array) {
        for (int i = 0 ; i < vx->arr_len ; i++)
            vx->x[i].q = qdp1_expose(vx->lftype, vx->nc, vx->x[i].p);
    } else
        vx->x[0].q = qdp1_expose(vx->lftype, vx->nc, vx->x[0].p);
    vx->is_exposed = 1;
}

void
vqdp_reset(vqdp *vx)
{
    if (vx->is_array) {
        for (int i = 0 ; i < vx->arr_len ; i++)
            qdp1_reset(vx->lftype, vx->nc, vx->x[i].p);
    } else
        qdp1_reset(vx->lftype, vx->nc, vx->x[0].p);
    vx->is_exposed = 0;
}

/* push vqdp (field or array) on top of stack;
    luastack:-0+1 */
void
vqdp_push(lua_State *L, int Sidx, vqdp *vx)
{
    assert(NULL != vx);
    NORMALIZE_INDEX(L, Sidx);
    if (vx->is_array) {
        lua_createtable(L, vx->arr_len, 0);
        int tabidx = lua_gettop(L);
        for (int i = 0 ; i < vx->arr_len ; i++) {
            vx->x[i].p = qdp1_push(L, Sidx, vx->lftype, vx->nc);
            lua_rawseti(L, tabidx, 1 + i);  
            /* total stack: +1-1*/
        }
    } else
        vx->x[0].p = qdp1_push(L, Sidx, vx->lftype, vx->nc);
    vx->is_exposed = 0;
}



typedef struct {void *ptr;} qlua_check_nocolor_s;
qlua_check_nocolor_s qlua_check_nocolor_s_NULL = {NULL};
qlua_check_nocolor_s*
qlua_check_nocolor(lua_State *L, int idx, mLattice *S, int nc)
{
    luaL_error(L, "compiled without nc=%d support", nc);
    return &qlua_check_nocolor_s_NULL;
}

/* reset qdp lattice object */

vqdp *
qlm_check_vqdp(lua_State *L, int idx, const qlmVecLayout *vl)
{
    vqdp *res = vqdp_alloc(vl->lftype, vl->is_array, vl->arr_len, vl->nc);
    if (NULL == res) {
        luaL_error(L, "not enough memory");
        return NULL;
    }
    if (vl->is_array) {
        void **p = NULL;
        p = qdp1_check_array(L, idx, vl->arr_len, NULL, vl->S, vl->lftype, vl->nc);
        if (NULL == p) {
            vqdp_free(res);
            return NULL;
        }
        for (int i = 0 ; i < vl->arr_len ; i++)
            res->x[i].p = p[i];
        free(p);
    } else
        res->x[0].p = qdp1_check(L, idx, vl->S, vl->lftype, vl->nc);
    return res;
}
/* 0:OK 1:fail (set qlm_error) */
vqdp *
qlm_new_vqdp(lua_State *L, int Sidx, const qlmVecLayout *vl)
{
    vqdp *res = vqdp_alloc(vl->lftype, vl->is_array, vl->arr_len, vl->nc);
    if (NULL == res) {
        luaL_error(L, "(qlm_new_vqdp) not enough memory\n");
        return NULL;
    }
    vqdp_push(L, Sidx, res);
    return res;
}



#define Qpp(func) func ## fd
#define pint    float
#define QT(x)   QLA_D##x
#include "qlm_vqdp-x.c"

#define Qpp(func) func ## dd
#define pint    double
#define QT(x)   QLA_D##x
#include "qlm_vqdp-x.c"


#define QLM_P(a)    (QLM_PREC_SINGLE==vl->prec ? a##fd : (QLM_PREC_DOUBLE==vl->prec ? a##dd : NULL))
#define QLM_PC(a)   (2==vl->nc ? QLM_P(a##2) : (3==vl->nc ? QLM_P(a##3) : NULL ))

/*! import latvec from exposed vtype
   RETURN 0 : ok, 1 : fail (set qlm_error) */
int
qlm_import_latvec_vqdp(qlmVecLayout *vl, void *restrict dst, const vqdp *vx)
{
    assert(vx->is_exposed);
    site_layout_x2i vol_src_x2i = get_site_layout_x2i(NULL, QLM_LAYOUT_QDPFULL);
    site_layout_i2x vol_dst_i2x = get_site_layout_i2x(vl, vl->layout);
    field_copy fc = NULL;
    switch(vl->lftype) {
    case QLM_LATREAL:       fc = QLM_P (fc_qla2int_R); break;
    case QLM_LATCOMPLEX:    fc = QLM_P (fc_qla2int_C); break;
    case QLM_LATCOLVEC:     fc = QLM_PC(fc_qla2int_V); break;
    case QLM_LATCOLMAT:     fc = QLM_PC(fc_qla2int_M); break;
    case QLM_LATDIRFERM:    
        fc = QLM_PC(fc_qla2int_D); 
        /* TODO layout switch: whether blocking spin */
        break;
    case QLM_LATDIRPROP:    fc = QLM_PC(fc_qla2int_P); break;
    default:
        qlm_error = QLM_ERR_ENUM_ERROR;
       return 1;
    }
    if (NULL == fc) {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 1;
    }

//#pragma omp parallel for
//    for (LONG_T i_xb = 0 ; i_xb < vl->vol_blk_len ; i_xb++) {
//    for (LONG_T i_x1 = 0 ; i_x1 < vl->blk_site_len ; i_x1++) { /* volume loop */
    assert(vl->vol_blk_len * vl->blk_site_len == vl->vol_site_len);
#pragma omp parallel for
    for (LONG_T i_x = 0 ; i_x < vl->vol_site_len ; i_x++) {
        LONG_T i_xb = i_x / vl->blk_site_len,
               i_x1 = i_x % vl->blk_site_len;
        int vs_coord[QLUA_MAX_LATTICE_RANK];
        LONG_T vs_idx_src;
        LONG_T i_vol = i_x1 + i_xb * vl->blk_site_len;
        vol_dst_i2x(vs_coord, i_vol, vl);
        vol_src_x2i(&vs_idx_src, vs_coord, vl);
        assert(0 <= vs_idx_src);
        for (int i_ab = 0, i_arr = 0 ; i_ab < vl->arr_nb ; i_ab++) {
        for (int i_a1 = 0 ; i_a1 < vl->arr_block ; i_a1++, i_arr++) {/* array loop */
        for (int i_ib = 0 ; i_ib < vl->int_nb; i_ib++) { /* internal block loop */
            LONG_T vs_idx_dst = QLM_INDEX_ELEM(vl, i_xb, i_x1, i_ab, i_a1, i_ib);
            assert(vs_idx_dst < vl->vec_elem_len);
            fc(dst, vs_idx_dst, vx->x[i_arr].q, vs_idx_src, i_ib, vl);
    }}}}
    return 0;
}

/*! export latvec into exposed vtype 
   return value 0:OK, 1:Fail (set qlm_error) */
int
qlm_export_latvec_vqdp(vqdp *vx, qlmVecLayout *vl, 
        const void * restrict src)
{
    assert(vx->is_exposed);
    site_layout_x2i vol_dst_x2i = get_site_layout_x2i(NULL, QLM_LAYOUT_QDPFULL);
    site_layout_i2x vol_src_i2x = get_site_layout_i2x(vl, vl->layout);
    field_copy fc = NULL;
    switch(vl->lftype) {
    case QLM_LATREAL:       fc = QLM_P (fc_int2qla_R); break;
    case QLM_LATCOMPLEX:    fc = QLM_P (fc_int2qla_C); break;
    case QLM_LATCOLVEC:     fc = QLM_PC(fc_int2qla_V); break;
    case QLM_LATCOLMAT:     fc = QLM_PC(fc_int2qla_M); break;
    case QLM_LATDIRFERM:    fc = QLM_PC(fc_int2qla_D); break;
    case QLM_LATDIRPROP:    fc = QLM_PC(fc_int2qla_P); break;
    default:
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 1;
    }
    if (NULL == fc) {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 1;
    }

//#pragma omp parallel for
//    for (LONG_T i_xb = 0 ; i_xb < vl->vol_blk_len ; i_xb++) {
//    for (LONG_T i_x1 = 0 ; i_x1 < vl->blk_site_len ; i_x1++) { /* volume loop */
    assert(vl->vol_blk_len * vl->blk_site_len == vl->vol_site_len);
#pragma omp parallel for
    for (LONG_T i_x = 0 ; i_x < vl->vol_site_len ; i_x++) {
        LONG_T i_xb = i_x / vl->blk_site_len,
               i_x1 = i_x % vl->blk_site_len;
        int vs_coord[QLUA_MAX_LATTICE_RANK];
        LONG_T vs_idx_dst;
        LONG_T i_vol = i_x1 + i_xb * vl->blk_site_len;
        vol_src_i2x(vs_coord, i_vol, vl);
        vol_dst_x2i(&vs_idx_dst, vs_coord, vl);
        assert(0 <= vs_idx_dst);
        for (int i_ab = 0, i_arr = 0 ; i_ab < vl->arr_nb ; i_ab++) {
        for (int i_a1 = 0 ; i_a1 < vl->arr_block ; i_a1++, i_arr++) {/* array loop */
        for (int i_ib = 0 ; i_ib < vl->int_nb; i_ib++) { /* internal block loop */
            LONG_T vs_idx_src = QLM_INDEX_ELEM(vl, i_xb, i_x1, i_ab, i_a1, i_ib);
            assert(vs_idx_src < vl->vec_elem_len);
            fc(vx->x[i_arr].q, vs_idx_dst, src, vs_idx_src, i_ib, vl);
    }}}}
    return 0;
}
#undef QLM_P
#undef QLM_PC

