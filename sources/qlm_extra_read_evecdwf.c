#include "qlua.h"                       /* DEPS */
#include "qlayout.h"                    /* DEPS */
#include "crc32.h"                      /* DEPS */
#include "qlm.h"                        /* DEPS */
#include "qlm_layout.h"                 /* DEPS */
#include "qvector.h"                    /* DEPS */

#include <assert.h>


struct evecdwfcl_evc_meta {
  int s[5];
  int b[5];
  int nkeep;
  int nkeep_single;

  // derived
  int nb[5];
  int blocks;

  int neig;

  int index;

  uint32_t *crc32;

  int FP16_COEF_EXP_SHARE_FLOATS;
};



static char qlm_evecdwfcl_read_meta_st_[256];
static const char *
qlm_evecdwfcl_read_meta(
        struct evecdwfcl_evc_meta *meta, 
        int ndim,
        int n_crc32,
        const char *meta_file) {
#define RETURN_STATUS(...) do {\
    snprintf(qlm_evecdwfcl_read_meta_st_, sizeof(qlm_evecdwfcl_read_meta_st_), __VA_ARGS__); \
    return qlm_evecdwfcl_read_meta_st_; \
} while(0);
#define PARSE_INT(args, n) \
    if (!strcmp(buf, #n)) { \
	(args).n = atoi(val); \
    }
#define PARSE_INT_ARRAY(args, n, len, i) \
    if (!strcmp(buf, #n)) { \
        if (len <= i) return "array '" #n "' index out of range"; \
	(args).n[i] = atoi(val); \
    }
#define PARSE_HEX(args, n) \
    if (!strcmp(buf, #n)) { \
	sscanf(val, "%X", &(args).n); \
    }
#define PARSE_HEX_ARRAY(args, n, len, i) \
    if (!strcmp(buf, #n)) { \
        unsigned x;\
        if (len <= i) return "array '" #n "' index out of range"; \
	sscanf(val, "%X", &x) ; (args).n[i] = x; \
    }

    char buf[1024];
    char val[1024];
    char line[1024];
 
    FILE* f = fopen(meta_file, "r");
    if (NULL == f) {
        RETURN_STATUS("cannot open meta file") ;
    }

    while (!feof(f)) {
        if (!fgets(line,sizeof(line),f))  break;
    
        if (sscanf(line, "%s = %s\n", buf, val) == 2) {
            char* r = strchr(buf,'[');
            if (r) {
                *r = '\0';
                int i = atoi(r+1);
                
                PARSE_INT_ARRAY(*meta, s,  ndim + 1, i) else
	        PARSE_INT_ARRAY(*meta, b,  ndim + 1, i) else
	        PARSE_INT_ARRAY(*meta, nb, ndim + 1, i) else
		PARSE_HEX_ARRAY(*meta, crc32, n_crc32, i) else 
                {   RETURN_STATUS("unknown array '%s'", buf); }
            } else {
                PARSE_INT(*meta, neig) else
	        PARSE_INT(*meta, nkeep) else
	        PARSE_INT(*meta, nkeep_single) else
	        PARSE_INT(*meta, blocks) else
		PARSE_INT(*meta, FP16_COEF_EXP_SHARE_FLOATS) else
		PARSE_INT(*meta, index) else
                {   RETURN_STATUS("unknown parameter '%s'", buf); }
            }	
        } else {
            printf("Improper format: %s\n",line); // double nl is OK
        }
    }
  
    fclose(f);
    return NULL;
#undef RETURN_STATUS
}


static char qlm_evecdwfcl_read_eval_st_[256];
const char *
qlm_evecdwfcl_read_eval(int n_eval, 
        double *eval,
        const char *eval_file)
{
#define RETURN_STATUS(...) do { \
    snprintf(qlm_evecdwfcl_read_eval_st_, sizeof(qlm_evecdwfcl_read_eval_st_), __VA_ARGS__); \
    status = qlm_evecdwfcl_read_eval_st_; \
    goto clearerr_1; \
} while(0)
    const char *status = NULL;
    FILE *fi = NULL;
    if (NULL == (fi = fopen(eval_file, "r"))) {
        RETURN_STATUS("cannot open eval file '%s'", eval_file);
    }
    int eval_read;
    if (1 != fscanf(fi, "%d", &eval_read)) {
        RETURN_STATUS("cannot parse eigenvalue count in token 1 in '%s'", 
                eval_file);
    }
    if (eval_read < n_eval) {
        RETURN_STATUS("eigenvalue number mismatch %d(eval)<%d(meta) in '%s'", 
                eval_read, n_eval, eval_file);
    }
    for (int i = 0 ; i < n_eval ; i++) {
        if (1 != fscanf(fi, "%lf", eval + i)) {
            RETURN_STATUS("incorrect data in token %d in '%s'", 2 + i, eval_file);
        }
    }

clearerr_1:
    if (NULL != fi) { fclose(fi); fi = NULL; }
    return status;
#undef RETURN_STATUS
}

#define DWF_NC  3
#define DWF_NS  4
static void
qlm_evecdwfcl_copy_block(
        const qlmVecLayout *vl, 
        void *restrict vec_dst_, /* pointer to dst vector */
        LONG_T i_lvb,   /* local vol block index */
        int i_ab,       /* array block index */
        int i_ib,       /* internal block index */
        int df_rlen,     /* for spin-blocked case */
        const void *restrict block_src_) 
{   
    int bs_len = vl->blk_site_len,
        a1_len = vl->arr_block;
    assert(1 == vl->int_nb);    /* copy all spin */
    for (int i_ax1 = 0 ; i_ax1 < bs_len * a1_len ; i_ax1++) {
        int fb_idx = i_ax1; /* i_x1 + i_a1 * bs_len; */
        int i_x1 = i_ax1 % bs_len,  /* in-block coord */
            i_a1 = i_ax1 / bs_len;  /* in-block array */
        LONG_T vd_idx = QLM_INDEX_ELEM(vl, i_lvb, i_x1, i_ab, i_a1, i_ib);
        float *ssrc = (float *)block_src_ + fb_idx * df_rlen;
        if (QLM_PREC_SINGLE == vl->prec) {
            float complex *cdst = vec_dst_ + vd_idx * vl->elem_size;
            for (int ic = 0 ; ic < DWF_NC ; ic++)
                for (int is = 0 ; is < DWF_NS ; is++) {
                    int kdst = QLM_INDEX_D(ic,is, DWF_NC,DWF_NS),
                        ksrc = ic + DWF_NC * is;
                    cdst[kdst] = ssrc[2*ksrc] + I*ssrc[2*ksrc+1];
                }
        } else if (QLM_PREC_DOUBLE == vl->prec) {
            double complex *cdst = vec_dst_ + vd_idx * vl->elem_size;
            for (int ic = 0 ; ic < DWF_NC ; ic++)
                for (int is = 0 ; is < DWF_NS ; is++) {
                    int kdst = QLM_INDEX_D(ic,is, DWF_NC,DWF_NS),
                        ksrc = ic + DWF_NC * is;
                    cdst[kdst] = ssrc[2*ksrc] + I*ssrc[2*ksrc+1];
                }
        }
        else assert(NULL == "impossible");
    }
}


#define SHRT_UMAX 65535
#define BASE 1.4142135623730950488
static float 
unpack_fp16_exp(uint16_t e)
{
    return pow(BASE, (float)((int)e - SHRT_UMAX/2));
}
static float 
unpack_fp16_float(int val, float min, float max, int N) {
  return min + (float)(val + 0.5) * (max - min)  / (float)( N + 1 );
}

static void
unpack_fp16_float_array(float *restrict dst, const uint16_t *restrict src, 
        int64_t len, int64_t len_share_exp)
{
    assert(0 == len % len_share_exp);
    int64_t nx = len / len_share_exp;
    for (int i = 0 ; i < nx ; i++) {
        float *dst_i = dst + i * len_share_exp;
        const uint16_t *src_i = src + i * (len_share_exp + 1);
        float max = unpack_fp16_exp(*src_i++);
        for (int k = 0 ; k < len_share_exp ; k++)
            *dst_i++ = unpack_fp16_float(*src_i++, -max, max, SHRT_UMAX);
    }
}


static char qlm_evecdwfcl_read_st_[256];
static const char *
qlm_evecdwfcl_read_compressed(
        qlmData *d, 
        const int file_geom[],
        const char *meta_file, 
        const char *base_path, 
        int dir_stride, 
        int file_bigendian,
        int *n_eval)
{
#define RETURN_STATUS(...) do { \
    snprintf(qlm_evecdwfcl_read_st_, sizeof(qlm_evecdwfcl_read_st_), __VA_ARGS__); \
    status = qlm_evecdwfcl_read_st_; \
    goto clearerr_1; \
} while(0)

    const char *status = NULL;
    char fname_buf[2048];
    struct evecdwfcl_evc_meta em;   memset(&em, 0, sizeof(em));

    int do_swapendian = (( file_bigendian && !is_bigendian()) ||
                         (!file_bigendian &&  is_bigendian()) );
    const qlmVecLayout *vl = &d->vl;
    mLattice *S = d->vl.S;
    int ndim = vl->ndim;
    int darr = ndim;    /* "array" (5th) dimension */
    
    LONG_T *f_vblk_ind = NULL,   /* volume-block indices in a file */
           *file_ind = NULL,    /* file indices */
           *l_vblk_ind  = NULL,  /* volume-block indices in a local volume */
           map_len = 0;         /* length of mapping */
    void *fbuf = NULL,
         *vbuf = NULL;
    FILE *fi = NULL;
    int node_coord[QLUA_MAX_LATTICE_RANK],
        lat_blk_dim[QLUA_MAX_LATTICE_RANK];
    int file_cnt, 
        f_vblk_len;
    int l_vblk_len = vl->vol_blk_len;   

    /* check compatibility 
       TODO return an error instead of aborting */
    if (    DWF_NC != vl->nc ||
            DWF_NS != vl->ns ||
            1 != vl->int_nb ||
            QLM_LATDIRFERM != vl->lftype ||
            QLM_SUBLAT_ODD != vl->sublat ||
            QLM_LAYOUT_BLKLEX != vl->layout) {
        RETURN_STATUS("inconsistent qlm data");
    }

    file_cnt = 1;
    for (int k = 0 ; k < ndim ; k++)
        file_cnt *= file_geom[k];

    em.crc32 = malloc(file_cnt * sizeof(em.crc32[0]));
    assert(NULL != em.crc32);
    if (NULL != (status = qlm_evecdwfcl_read_meta(
            &em, ndim, file_cnt, meta_file)))
        return status;

    /* check vol dim */
    for (int k = 0 ; k < ndim ; k++) {
        if (em.s[k] * file_geom[k] != vl->S->dim[k]) {
            RETURN_STATUS("file/meta geometry mismatch in dim %d", k);
        }
        if (em.b[k] != vl->blk_site_dim[k]) {
            RETURN_STATUS("opt/meta geometry mismatch in dim %d", k);
        }
        if (em.b[k] * em.nb[k] != em.s[k]) {
            RETURN_STATUS("inconsistent meta geometry in dim %d", k);
        }
    }

    /* check array dim */
    if (em.s[darr] != vl->arr_len) {
        RETURN_STATUS("opt/meta mismatch in arr_len");
    }
    if (em.b[darr] != vl->arr_block) {
        RETURN_STATUS("opt/meta mismatch in arr_block");
    }
    if (em.nb[darr] != vl->arr_nb) {
        RETURN_STATUS("opt/meta mismatch in arr_nb");
    }
    f_vblk_len = 1;
    for (int k = 0 ; k < ndim ; k++)
        f_vblk_len *= em.nb[k];
    int arr_nb  = vl->arr_nb;
    int l_nb  = arr_nb * l_vblk_len, /* tot.blocks / lvol */
        f_nb  = arr_nb * f_vblk_len; /* tot.blocks / file */
    if (em.blocks != f_nb) {
        RETURN_STATUS("inconsistent meta block count");
    }
    /* check other params */
    if (    em.neig <= 0 ||
            em.nkeep <= 0 ||
            em.nkeep_single <= 0 ||
            em.FP16_COEF_EXP_SHARE_FLOATS <= 0) {
        RETURN_STATUS("invalid (skipped?) meta parameters");
    }
    if (d->nvec < em.neig) {
        RETURN_STATUS("insufficient vector storage: %d<%d", 
                d->nvec, em.neig);
    }

    /* determine block index mapping: sorted by file and volume-block */
    /* figure out which files contain blocks for the local volume 
       lat_blk_dim[] total blocks separated into file_geom[] file sets 
       or S->net local volumes; find intersection of loc.volume at node_coord
       with file sets:
       block #k is contained in
       file_ind[k],     rlex.index of file in file_geom[] mesh
       f_vblk_ind[k],    rlex.index of the block in the file
       l_vblk_ind[k],    rlex.index of the block in the loc.volume */
    node2coord(node_coord, S->node, S);
    for (int k = 0 ; k < ndim ; k++)
        lat_blk_dim[k] = S->dim[k] / vl->blk_site_dim[k];
    if (0 != qlm_iogeom_map_lex(ndim, 
                lat_blk_dim, S->net, node_coord, file_geom, 
                &map_len, &l_vblk_ind, &f_vblk_ind, &file_ind)) {
        RETURN_STATUS("cannot compute file/node matching");
    }
    assert((int)map_len == l_vblk_len);

#define READ_BLK_OMP 0 /* disable omp: slow due to many fork/join? */
#define MYMAX(a,b) ((a) < (b) ? (b) : (a))
#define RSIZE   4
#define CSIZE   (2*RSIZE)
#define HSIZE   2
#define FP_16_SIZE(len,len_share_exp)  ((HSIZE) * ((len) + (len)/(len_share_exp))) 

    /* loop over files */
    int nkeep = em.nkeep,
        nkeep_single = em.nkeep_single;
    int nkeep_fp16 = (nkeep_single < nkeep ? nkeep - nkeep_single : 0);
    int df_rlen = DWF_NC * DWF_NS * 2; /* nc*ns*cplx */
    int n_eig = em.neig;
    /* lengths of read buffers */
    /* reals per 1 vec.block */
    LONG_T fblk_rlen = vl->blk_site_len * vl->arr_block * df_rlen;
    /* bytes / 1 vec.block, single */
    LONG_T fblk_single_size = RSIZE * fblk_rlen;
    /* bytes / 1 vec.block, fp16 */
    LONG_T fblk_fp16_size   = FP_16_SIZE(fblk_rlen, df_rlen);
    /* bytes / 1 col of coeff matr per file */
    int cblk_share_exp = em.FP16_COEF_EXP_SHARE_FLOATS;
    LONG_T f_coeff_single_size = CSIZE * nkeep_single;
    LONG_T f_coeff_fp16_rlen = 2 * nkeep_fp16;
    assert(0 == f_coeff_fp16_rlen % cblk_share_exp);
    LONG_T f_coeff_fp16_size = FP_16_SIZE(f_coeff_fp16_rlen, cblk_share_exp);
    LONG_T f_coeff_size  = f_coeff_single_size + f_coeff_fp16_size;
    LONG_T fcblk_col_size   = f_coeff_size * f_nb;
    /* read chunk lenghts, bytes */
    LONG_T fblk_single_read = fblk_single_size * nkeep_single,
           fblk_fp16_read   = fblk_fp16_size * nkeep_fp16,
           fcblk_read       = fcblk_col_size;
    assert(0 == fblk_rlen % df_rlen);
    /* buffer for reading 1 block of all evecs */
    LONG_T fbuf_size = MYMAX(fblk_single_read, 
            MYMAX(fblk_fp16_read, fcblk_read));
    if (NULL == (fbuf = malloc(fbuf_size))) {
        RETURN_STATUS("not enough memory for fbuf (%llu)", 
                (long long unsigned)fbuf_size);
    }
    /* buffer for converting 1 block of all evecs */
    LONG_T vbuf_size    = MYMAX(
            fblk_single_size * nkeep_fp16,  /* to unpack vec.blocks */
            f_coeff_fp16_rlen * RSIZE * l_nb);             /* to unpack coeffs */
    if (NULL == (vbuf = malloc(vbuf_size))) {
        RETURN_STATUS("not enough memory for vbuf (%llu)", 
                (long long unsigned)vbuf_size);
    }

    /* loop over files that contain several relevant blocks */
    int i0 = 0;
    while (i0 < l_vblk_len) {
        LONG_T file_vol = file_ind[i0];
        /* range of local block indices from this file */
        int i1 = i0;
        while (i1 < l_vblk_len && file_ind[i1] == file_vol) 
            i1++;
        int di = i1 - i0;
        uint32_t crc32 = 0;
        int dir_ind = file_vol / dir_stride;
        snprintf(fname_buf, sizeof(fname_buf), "%s/%02d/%010d.compressed", 
                base_path, dir_ind, (int)file_vol);
        if (NULL == (fi = fopen(fname_buf, "r"))) {
            RETURN_STATUS("cannot open '%s': %s", 
                    fname_buf, strerror(errno));
        }
        /* read entire file to check crc32, save relevant indices */
        /* nkeep_single */
        /* all array blocks */
        TIMING_DECLSTART(evbS_t);
        for (int i_ab = 0 ; i_ab < vl->arr_nb ; i_ab++) {
            int i = i0;
            LONG_T read_size = fblk_single_size * nkeep_single; 
            assert(read_size <= fbuf_size);
            /* all file volume-blocks */
            for (LONG_T i_fvb = 0 ; i_fvb < f_vblk_len ; i_fvb++) {
                off_t fpos = ftello(fi);
                if (1 != fread(fbuf, read_size, 1, fi)) {
                    RETURN_STATUS("cannot read %llu*%d bytes at %lld in '%s': %s",
                            (long long unsigned)fblk_single_size, 
                            nkeep_single, (long long int)fpos, fname_buf,
                            strerror(errno));
                }
                crc32 = crc32_fast(fbuf, read_size, crc32);
                if (i < i1 && i_fvb == f_vblk_ind[i]) { /* copy to storage */
#if READ_BLK_OMP
#pragma omp parallel for
#endif
                    for (int m = 0 ; m < nkeep_single ; m++) {
                        void *fbuf_m = fbuf + m * fblk_single_size;
                        if (do_swapendian)
                                swap_endian(fbuf_m, RSIZE, 
                                        fblk_single_size/ RSIZE); 
                        qlm_evecdwfcl_copy_block(vl, 
                                d->vec_data + m * vl->vec_size, 
                                l_vblk_ind[i], i_ab, 0, df_rlen, fbuf_m);
                    }
                    i++;    /* look for next local block */
                }
            }
            assert(i == i1);
        }
        /**/printf("[%04d] evbS_t[%04lld] = %.3fs\n", QDP_this_node, file_vol, TIMING_CHECK(evbS_t));

        /* nkeep_fp16 */
        /* all array blocks (5th dim) */
        TIMING_DECLSTART(evbH_t);
        for (int i_ab = 0 ; i_ab < vl->arr_nb ; i_ab++) {
            int i = i0;
            LONG_T read_size = fblk_fp16_size * nkeep_fp16;
            assert(read_size <= fbuf_size);
            /* all file volume-blocks (1..4dim) */
            for (LONG_T i_fvb = 0 ; i_fvb < f_vblk_len ; i_fvb++) {
                off_t fpos = ftello(fi);
                if (1 != fread(fbuf, read_size, 1, fi)) {
                    RETURN_STATUS("cannot read %llu*%d bytes at %lld in '%s': %s",
                            (long long unsigned)fblk_fp16_size, 
                            nkeep_fp16, (long long int)fpos, fname_buf,
                            strerror(errno));
                }
                crc32 = crc32_fast(fbuf, read_size, crc32);
                if (i < i1 && i_fvb == f_vblk_ind[i]) { /* copy to storage */
#if READ_BLK_OMP
#pragma omp parallel for
#endif
                    for (int m1 = 0 ; m1 < nkeep_fp16 ; m1++) {
                        void *fbuf_m1 = (void *)fbuf + m1 * fblk_fp16_size;
                        void *vbuf_m1 = (void *)vbuf + m1 * fblk_single_size;
                        if (do_swapendian)  
                            swap_endian(fbuf_m1, HSIZE, fblk_fp16_size / HSIZE);
                        unpack_fp16_float_array((float *)vbuf_m1, fbuf_m1,
                                fblk_rlen, df_rlen);
                        int m = m1 + nkeep_single;
                        qlm_evecdwfcl_copy_block(vl,
                                d->vec_data + m * vl->vec_size, 
                                l_vblk_ind[i], i_ab, 0, df_rlen, vbuf_m1);
                    }
                    i++;    /* look for next local block */
                }
            }
            assert(i == i1);
        }
        /**/printf("[%04d] evbH_t[%04lld] = %.3fs\n", QDP_this_node, file_vol, TIMING_CHECK(evbH_t));

        /* TODO read blk_coeff */
        /* all array blocks (5th dim) */
        TIMING_DECLSTART(cbvSH_t); 
        {
            LONG_T read_size = fcblk_col_size;
            assert(read_size <= fbuf_size);
            for (int jvec = 0 ; jvec < n_eig ; jvec++) {
                off_t fpos = ftello(fi);
                if (1 != fread(fbuf, read_size, 1, fi)) {
                    RETURN_STATUS("cannot read %llu bytes at %lld in '%s': %s",
                            (long long unsigned)read_size, (long long int)fpos, 
                            fname_buf, strerror(errno));
                }
                crc32 = crc32_fast(fbuf, read_size, crc32);
#if READ_BLK_OMP
#pragma omp parallel for
#endif
                for (int i_b = 0 ; i_b < vl->arr_nb * di ; i_b++) {
                    /* [ b=[arrb,volb], {m=single..,fp16..}] */
                    int i = i0 + i_b % di,
                        i_ab = i_b / di;
                    LONG_T i_fb = f_vblk_ind[i] + i_ab * f_vblk_len,
                           i_lb = QLM_INDEX_BLOCK(vl, l_vblk_ind[i], i_ab, 0);

                    float *fbuf_b = (void *)fbuf + i_fb * f_coeff_size;
                    void  *fbuf_b_fp16 = (void *)fbuf_b + f_coeff_single_size;
                    float *vbuf_b = (float *)vbuf + i_b * f_coeff_fp16_rlen ;
                    if (do_swapendian) {
                        swap_endian((char *)fbuf_b, RSIZE, 
                                f_coeff_single_size / RSIZE);
                        swap_endian((char *)fbuf_b_fp16, HSIZE, 
                                f_coeff_fp16_size / HSIZE);
                    }
                    unpack_fp16_float_array(vbuf_b, fbuf_b_fp16,
                            f_coeff_fp16_rlen, cblk_share_exp);
                    /* TODO support double and remove safeguard */
                    assert(QLM_PREC_SINGLE == vl->prec);      
                    float complex *cblk_jb = (float complex *)d->blk_coeff
                            + jvec * d->cblkvec_num_len + i_lb;
                    LONG_T mstride = d->cblk_num_len;
                    for (int m = 0 ; m < nkeep_single ; m++) {
                        cblk_jb[m * mstride] = fbuf_b[2*m] + I*fbuf_b[2*m+1];
                    }

                    for (int m1 = 0 ; m1 < nkeep_fp16 ; m1++) {
                        int m = m1 + nkeep_single;
                        cblk_jb[m * mstride] = vbuf_b[2*m1] + I*vbuf_b[2*m1+1];
                    }
                }
            }
        }
        /**/printf("[%04d] cbvSH_t[%04lld] = %.3fs\n", QDP_this_node, file_vol, TIMING_CHECK(cbvSH_t));

        if (crc32 != em.crc32[file_vol]) {
#if 1
            RETURN_STATUS("crc32 mismatch in '%s': %X(calc) != %X(meta)",
                    fname_buf, crc32, em.crc32[file_vol]);
#else
            fprintf(stderr, "[%04d] crc32 mismatch in '%s': %X(calc) != %X(meta)\n",
                    S->node, fname_buf, crc32, em.crc32[file_vol]);
#endif
        }
        fclose(fi);     fi = NULL;
        i0 = i1;    /* next file */
    }
    if (NULL != n_eval)
        *n_eval = em.neig;
    int isnan_vd = 0,
        isnan_cb = 0;
    for (int j = 0 ; j < d->nvec_basis ; j++)
        for (int i = 0 ; i < vl->vec_num_len ; i++) {
            float complex c = ((float complex *)d->vec_data)[i + vl->vec_num_len * j];
            float cr = crealf(c),
                  ci = cimagf(c);
            isnan_vd = isnan_vd || isnan(cr) || isnan(ci)  || isinf(cr) || isinf(ci);
//            if (isnan(cr) || isnan(ci)) printf("[%04d] vd(%5d,%5d):nan\n", S->node, i, j);
//            if (isinf(cr) || isinf(ci)) printf("[%04d] vd(%5d,%5d):nan\n", S->node, i, j);
        }
    for (int j = 0 ; j < d->nvec ; j++)
        for (int i = 0 ; i < d->cblkvec_num_len ; i++) {
            float complex c = ((float complex *)d->blk_coeff)[i + d->cblkvec_num_len * j];
            float cr = crealf(c),
                  ci = cimagf(c);
            isnan_cb = isnan_cb ||isnan(cr) || isnan(ci)  || isinf(cr) || isinf(ci);
//            if (isnan(cr) || isnan(ci)) printf("[%04d] cb(%5d,%5d):nan\n", S->node, i, j);
//            if (isinf(cr) || isinf(ci)) printf("[%04d] cb(%5d,%5d):nan\n", S->node, i, j);
        }
    if (isnan_vd)
        fprintf(stderr, "[%04d] WARNING vd=nan\n", S->node);
    if (isnan_cb)
        fprintf(stderr, "[%04d] WARNING cb=nan\n", S->node);


clearerr_1:
    free_not_null(em.crc32);
    free_not_null(f_vblk_ind);
    free_not_null(file_ind);
    free_not_null(l_vblk_ind);
    free_not_null(fbuf);
    free_not_null(vbuf);
    if (NULL != fi) fclose(fi);
    return status;
#undef RETURN_STATUS
}


/* simple Qlua wrapper
   qcd.latmat.read_df_blk_clehner(
        lm_df, meta_file, evec_file, base_path, 
            {file_geom=<{fx,fy,fz,ft}>,     -- default=net_geom
             dir_stride=<files_per_dir>,    -- default=file_cnt
             bigendian=false})              -- default=false
 */
int
q_qlm_evecdwfcl_read_compressed(lua_State *L)
{
    const char *status = NULL;
    double *eval = NULL;
    int *opt_fgeom = NULL;
    int n_eval = 0;
    mVecReal *qv = NULL;

    mQlm *md = qlua_checkQlm(L, 1, NULL);
    qlmData *d = md->d;
    mLattice *S = d->vl.S;
    int ndim = S->rank;
    const char *meta_file = qlua_checkstring(L, 2, "meta_file");
    const char *eval_file = qlua_checkstring(L, 3, "eval_file");
    const char *base_path = qlua_checkstring(L, 4, "base_path");
    int oidx = 5;
    const int *file_geom = S->net;
    int dir_stride = -1;
    int bigendian = 1;
    if (qlua_checkopt_paramtable(L, oidx)) {
        if (qlua_tabpushopt_key(L, oidx, "file_geom")) {
            opt_fgeom = qlua_checkintarray(L, -1, ndim, NULL);
            if (NULL != opt_fgeom) 
                file_geom = opt_fgeom;
            lua_pop(L, 1);
        }
        dir_stride = qlua_tabkey_intopt(L, oidx, "dir_stride", -1);
        bigendian = qlua_tabkey_boolopt(L, oidx, "bigendian", 0);
    }
    int file_cnt = 1;
    for (int k = 0 ; k < ndim ; k++)
        file_cnt *= file_geom[k];
    if (dir_stride <= 0) 
        dir_stride = file_cnt;

    if (NULL != (status = qlm_evecdwfcl_read_compressed(d, file_geom, 
            meta_file, base_path, dir_stride, bigendian, &n_eval)))
        goto clearerr_1;

    if (NULL == (eval = malloc(n_eval * sizeof(eval[0])))) {
        status = "cannot allocate storage for eval";
        goto clearerr_1;
    }
    if (NULL != (status = qlm_evecdwfcl_read_eval(n_eval, eval, eval_file)))
        goto clearerr_1;
    if (NULL == (qv = qlua_newVecReal(L, n_eval))) {
        status = "cannot allocate vector";
        goto clearerr_1;
    }
    for (int i = 0 ; i < n_eval ; i++)
        qv->val[i] = eval[i];

clearerr_1:
    free_not_null(opt_fgeom);
    free_not_null(eval);
    if (NULL != status)
        luaL_error(L, status);
    return 1;   /* eval(vecReal) */
}
