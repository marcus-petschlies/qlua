#ifndef QLM_VQDP_H__72E756E7_F5D3_4EBB_B82F_CEDB83371A6C
#define QLM_VQDP_H__72E756E7_F5D3_4EBB_B82F_CEDB83371A6C

#include "qlm.h"                /* DEPS */
#include "qlm_layout.h"         /* DEPS */


void *qdp1_create(mLattice *S, qlmLatField lftype, int nc);
void qdp1_destroy(qlmLatField lftype, int nc, void *x);
void *qdp1_expose(qlmLatField lftype, int nc, void *x);
int qdp1_reset(qlmLatField lftype, int nc, void *x);
void *qdp1_push(lua_State *L, int Sidx, qlmLatField lftype, int nc);
void *qdp1_check(lua_State *L, int idx, mLattice *S, qlmLatField lftype, int nc);
void **qdp1_check_array(lua_State *L, int idx, int len, void **res_, mLattice *S, qlmLatField lftype, int nc);

typedef struct {
    qlmLatField lftype;
    int nc;
    int is_array;   /* whether corresponds to a Qlua table, even if arr_len==1 */
    int arr_len;    /* number of elements, ==1 if !is_array */
    int is_exposed; /* if qla-expposed */
    struct {
        void *p;    /* qdp ptr */
        void *q;    /* qla ptr (exposed) */
    } x[0];
} vqdp;

vqdp *vqdp_alloc(qlmLatField lftype, int is_array, int arr_len, int nc);
void vqdp_reset(vqdp *vx);
void vqdp_free(vqdp *vx);
#if 0 /* commented out because `vqdp' is only a presentation of 
         lattice objects stored on stack; should not be created except by 
         Lua-push operation or destroyed except by garbate collector */
int vqdp_create(mLattice *S, vqdp *x);
void vqdp_destroy(vqdp *x);
#endif

void vqdp_expose(vqdp *vx);
void vqdp_reset(vqdp *vx);
void vqdp_push(lua_State *L, int Sidx, vqdp *vx);
#if 0
typedef struct {void *ptr;} qlua_check_nocolor_s;
qlua_check_nocolor_s qlua_check_nocolor_s_NULL = {NULL};
qlua_check_nocolor_s*
qlua_check_nocolor(lua_State *L, int idx, mLattice *S, int nc)
{
    luaL_error(L, "compiled without nc=%d support", nc);
    return &qlua_check_nocolor_s_NULL;
}
#endif

typedef void (*field_copy)(void *restrict dst_, LONG_T dst_idx, 
        const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl);

vqdp *qlm_check_vqdp(lua_State *L, int idx, const qlmVecLayout *vl);
vqdp *qlm_new_vqdp(lua_State *L, int Sidx, const qlmVecLayout *vl);
int qlm_import_latvec_vqdp(qlmVecLayout *vl, void *restrict dst, const vqdp *vx);
int qlm_export_latvec_vqdp(vqdp *vx, qlmVecLayout *vl, const void * restrict src);

#endif/*QLM_VQDP_H__72E756E7_F5D3_4EBB_B82F_CEDB83371A6C*/
