#ifndef QLM_LAYOUT_H__56669C49_147A_4905_87D6_520455405A58
#define QLM_LAYOUT_H__56669C49_147A_4905_87D6_520455405A58
#include "qlm.h"

void index_i2x(int ndim, const int *restrict dim, 
 int *restrict x, LONG_T ilex);
void index_x2i(int ndim, const int *restrict dim, 
 LONG_T *restrict ilex, const int *restrict x);
void index_i2x_eo(int ndim, const int *restrict dim, 
 int *restrict x, LONG_T ilex_eo, int par);
void index_x2i_eo(int ndim, const int *restrict dim, 
        LONG_T *restrict ilex, int *restrict par, const int *restrict x);

/* qdp lattice site layout */

/* qdp lattice site layout */
void site_layout_x2i_qdp(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl);

void site_layout_i2x_qdp(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl);
void site_layout_x2i_lex(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl);
void site_layout_i2x_lex(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl);
void site_layout_x2i_blk(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl);
void site_layout_i2x_blk(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl);
void site_layout_x2i_lex_eo(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl);
void site_layout_i2x_lex_eo(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl);
void site_layout_x2i_blk_eo(LONG_T *restrict vs_idx, const int *restrict vs_coord, const qlmVecLayout *vl);
void site_layout_i2x_blk_eo(int *restrict vs_coord, const LONG_T vs_idx, const qlmVecLayout *vl);

/* prototypes for coord <-> linear index conversion */
typedef void (*site_layout_i2x)(int *restrict x, const LONG_T site_idx, const qlmVecLayout *vl);
typedef void (*site_layout_x2i)(LONG_T *restrict site_idx, const int *restrict x, const qlmVecLayout *vl);

site_layout_x2i get_site_layout_x2i(const qlmVecLayout *vl, qlmLayout layout);
site_layout_i2x get_site_layout_i2x(const qlmVecLayout *vl, qlmLayout layout);

int qlm_elem_x2i(const qlmVecLayout *vl, LONG_T *ind, const int *x, int iarr, int i_ib);

#define QLM_INDEX_BLOCK(vl, i_xb, i_ab, i_ib) (\
        (i_ib) + (vl->int_nb)*(\
        (i_ab) + (vl->arr_nb)*(\
        (i_xb) + (vl->vol_blk_len)*(0))))
#define QLM_INDEX_ELEM(vl, i_xb, i_x1, i_ab, i_a1, i_ib) (\
        (i_a1) + (vl->arr_block)*(\
        (i_x1) + (vl->blk_site_len)*(\
        QLM_INDEX_BLOCK(vl, i_xb, i_ab, i_ib))))
#define QLM_INDEX_V(c, nc)  (c)
#define QLM_INDEX_M(c,c2, nc)  ((c) + (nc)*(c2))
#define QLM_INDEX_D(c,s, nc,ns)  ((s) + (ns)*(c))
#define QLM_INDEX_P(c,s,c2,s2, nc,ns)  ((s) + (ns)*((c) + (nc)*((s2) + (ns)*(c2))))

int 
qlm_iogeom_map_lex(int ndim, 
        const int lat_dim[],        /* size of lat in sites */
        const int lat_vdst_dim[],   /* size of lat in vdst units */
        const int lat_vdst_c[],     /* vdst coord in lat */
        const int lat_vsrc_dim[],   /* size of lat in vsrc units */
        LONG_T *map_len,
        LONG_T **p_vdst_i,          /* table of indices in dst volume */
        LONG_T **p_vsrc_i,          /* table of indices in src volumes */
        LONG_T **p_vsrc_v           /* list of src volumes */
        );


#endif/*QLM_LAYOUT_H__56669C49_147A_4905_87D6_520455405A58*/
