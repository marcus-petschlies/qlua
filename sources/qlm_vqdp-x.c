/* Let the ugliness begin! */
#define cplx_qla2int(int_x,qla_x) int_x = QLA_real(qla_x) + I*QLA_imag(qla_x)
#define cplx_int2qla(qla_x,int_x) QLA_c_eq_r_plus_ir((qla_x), creal(int_x), cimag(int_x))
#define pcint pint complex

/* conversion between internal representation and qdp/qla 
    * support arrays (eg 5d fermions = list of 4d fermions)
    * support blocking over internal index (eg left/right blocing in qop-mg)
    qla2int:
    src_    pointer to QLA array from exposed qdp field
    src_idx offset index in QLA array
    dst_    pointer to QLM vector destination interpreted as 
            (double|float){complex} depending on arg
    dst_idx offset index in QLM vector
    int2qla: src_ <-> dst_

    iint    internal dim block index
    arg_    now: interpreted as qlmVecLayout
 */
static void
Qpp(fc_qla2int_R)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    const QT(_Real) *qx = (QT(_Real) *)src_ + src_idx;
    pint *ix = ((pint *)dst_) + dst_idx * vl->elem_num_len;
    *ix = *qx;
}
static void
Qpp(fc_int2qla_R)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    pint *ix = ((pint *)src_) + src_idx * vl->elem_num_len;
    QT(_Real) *qx = (QT(_Real) *)dst_ + dst_idx;
    *qx = *ix;
}

static void
Qpp(fc_qla2int_C)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    QT(_Complex) *qx = (QT(_Complex) *)src_ + src_idx;
    pcint *ix = (pcint *)dst_ + dst_idx * vl->elem_num_len;
    cplx_qla2int(*ix, *qx);
}
static void
Qpp(fc_int2qla_C)(void *restrict dst_, LONG_T dst_idx, const void *restrict src_, LONG_T src_idx, int iint, const qlmVecLayout *vl)
{
    assert(0 == iint);
    pcint *ix = (pcint *)src_ + src_idx * vl->elem_num_len;
    QT(_Complex) *qx = (QT(_Complex) *)dst_ + dst_idx;
    cplx_int2qla(*qx, *ix);
}
#undef QLM_INDEX_C

/* TODO C */

#define Qnc 2
#define Qns 4
#define Qppc(x) Qpp(x##2)
#define QTc(x) QT(2##x)
#include "qlm_vqdp-y.c"

#define Qnc 3
#define Qns 4
#define Qppc(x) Qpp(x##3)
#define QTc(x) QT(3##x)
#include "qlm_vqdp-y.c"

#define Qnc (vl->nc)
#define Qns 4
#define Qppc(x) Qpp(x##N)
#define QTc(x) QT(N##x)
#include "qlm_vqdp-y.c"

#undef Qpp
#undef pint
#undef QT

