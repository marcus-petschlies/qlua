/* support for "latmat" - matrices (Lat*internal)*ivec
   - eigenspaces
   - multigrid bases

   TODO maintain variable types of blocking: 
    in addition to the current 4d-coord-block, add half-spinor as in qop mg
 */
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "qdp_d2.h"
#include "qdp_d3.h"
#include "qdp_dn.h"

#include "modules.h"                                                 /* DEPS */
#include "qlua.h"                                                    /* DEPS */
#include "qcomplex.h"                                                /* DEPS */
#include "qvector.h"                                                 /* DEPS */
#include "lattice.h"                                                 /* DEPS */
#include "qlayout.h"                                                 /* DEPS */
#include "qvector.h"                                                 /* DEPS */
#include "qmatrix.h"                                                 /* DEPS */

#include "latreal.h"                                                 /* DEPS */
#include "latcomplex.h"                                              /* DEPS */
#include "latcolvec.h"                                               /* DEPS */
#include "latcolmat.h"                                               /* DEPS */
#include "latdirferm.h"                                              /* DEPS */
#include "latdirprop.h"                                              /* DEPS */

//#include <cblas.h>        /* sic! use gsl_cblas.h instead */
#include <gsl/gsl_cblas.h>
//#include <lapacke.h>

#include "qlm.h"                                                     /* DEPS */
#include "qlm_layout.h"                                              /* DEPS */
#include "qlm_vqdp.h"                                                /* DEPS */
#include "qlm_extra.h"                                               /* DEPS */

qlmError qlm_error;
const char *
qlm_strerror(qlmError e) 
{
    switch (e) {
    case QLM_ERR_SUCCESS        : return "success" ;
    case QLM_ERR_ENOMEM         : return "not enough memory" ;
    case QLM_ERR_INVALID_VEC    : return "invalid vector index" ;
    case QLM_ERR_INVALID_FIELD  : return "incorrect lattice field" ;
    case QLM_ERR_INVALID_STATE  : return "invalid state for the action" ;
    case QLM_ERR_INVALID_PARAM  : return "invalid param for the action" ;
    case QLM_ERR_TYPE_MISMATCH  : return "mismatched metadata" ;
    case QLM_ERR_GEOM_MISMATCH  : return "mismatched geometry" ;
    case QLM_ERR_ENUM_ERROR     : return "unsupported constant value (internal error)";
    case QLM_ERR_GEOM_SITE      : return "operation on an incorrect site";
    }
    /* should not get here : handle all cases above */
    return NULL;
}

static LONG_T  
qlm_field_num_len(const qlmVecLayout *vl)
{
    switch (vl->lftype) {
    case QLM_LATREAL        : 
    case QLM_LATCOMPLEX     :   return 1;
    case QLM_LATCOLVEC      :   return vl->nc;
    case QLM_LATCOLMAT      :   return vl->nc * vl->nc ;
    case QLM_LATDIRFERM     :   return vl->nc * vl->ns ;
    case QLM_LATDIRPROP     :   return (vl->nc * vl->nc) * (vl->ns * vl->ns); 
    case QLM_LAT_NONE       :   return 0;
    }
    return 0;
}

static LONG_T 
qlm_elem_num_len(const qlmVecLayout *vl)
{
    LONG_T fl = qlm_field_num_len(vl);
    assert(0 == fl % vl->int_nb);
    /* subdivide internal dim into blocks */
    return (fl / vl->int_nb);
}
const char *
qlm_lftype_str(qlmLatField lftype) {
    switch(lftype) {
    case QLM_LATREAL    : return "lattice.real"            ; break ;
    case QLM_LATCOMPLEX : return "lattice.complex"         ; break ;
    case QLM_LATCOLVEC  : return "lattice.color.vector"    ; break ;
    case QLM_LATCOLMAT  : return "lattice.color.matrix"    ; break ;
    case QLM_LATDIRFERM : return "lattice.dirac.fermion"   ; break ;
    case QLM_LATDIRPROP : return "lattice.dirac.propagator"; break ;
    default : return "<none>";
    }
}
qlmLatField 
qlm_str2lftype(const char *str) 
{
    if      (!strcmp("lattice.real"            , str)) return QLM_LATREAL     ;
    else if (!strcmp("lattice.complex"         , str)) return QLM_LATCOMPLEX  ;
    else if (!strcmp("lattice.color.vector"    , str)) return QLM_LATCOLVEC   ;
    else if (!strcmp("lattice.color.matrix"    , str)) return QLM_LATCOLMAT   ;
    else if (!strcmp("lattice.dirac.fermion"   , str)) return QLM_LATDIRFERM  ;
    else if (!strcmp("lattice.dirac.propagator", str)) return QLM_LATDIRPROP  ;
    else {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return QLM_LAT_NONE; 
    }
}
qlmPrec
qlm_str2prec(const char *str)
{
    if      (!strcmp("single", str)) return QLM_PREC_SINGLE;
    else if (!strcmp("double", str)) return QLM_PREC_DOUBLE;
    else {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return QLM_PREC_NONE;
    }
}
qlmSublat 
qlm_str2sublat(const char *str)
{
    if      (!strcmp("full", str)) return QLM_SUBLAT_FULL;
    else if (!strcmp("even", str)) return QLM_SUBLAT_EVEN;
    else if (!strcmp("odd" , str)) return QLM_SUBLAT_ODD;
    else {
        qlm_error = QLM_SUBLAT_NONE;
        return QLM_PREC_NONE;
    }
}

/*! initialize the qlm structure 

  XXX important change from storing (1 site of 1 field) * arr_len per generalized "site" :
      "indivisible" elem now means (1 site of 1 field)/ n_intenal_blocks
      to allow compatibility with blocking in array and internal dimensions
  XXX if !is_blocked, block size defaults to local volume, arr_len, and 
      full internal index for unified striding compatible with blocked version
 */ 
int
qlm_veclayout_init(
        qlmVecLayout *vl,
        mLattice *S, 
        qlmSublat sublat, 
        qlmLatField lftype, 
        int is_array, 
        int arr_len, 
        int ns, 
        int nc,
        qlmPrec prec, 
        int is_blocked, 
        const int blk_site_dim[], 
        int arr_block,
        int int_nb)
{
    int xlo[QLUA_MAX_LATTICE_RANK], 
        xhi[QLUA_MAX_LATTICE_RANK];

    vl->S        = S;
    vl->ndim     = S->rank;
    vl->lftype   = lftype;
    vl->sublat   = sublat;
    vl->prec     = prec;
    vl->domain   = qlm_domain(lftype);
    vl->ns       = ns;
    vl->nc       = nc;
    vl->is_array = is_array;
    vl->layout       = (is_blocked ? QLM_LAYOUT_BLKLEX : QLM_LAYOUT_XLEX);

    if (!vl->is_array)   
        arr_len = 1;            /* default */
    assert(0 < arr_len);
    if (!is_blocked || arr_block <= 0) 
        arr_block = arr_len;    /* default */
    if (0 != arr_len % arr_block) {
        qlm_error = QLM_ERR_GEOM_MISMATCH;
        return 1;
    }
    assert(0 < arr_block);
    vl->arr_len  = arr_len;
    vl->arr_block= arr_block;
    vl->arr_nb   = arr_len / arr_block;

    if (!is_blocked) int_nb      = 1;
    assert(0 < int_nb);
    vl->int_nb   = int_nb;
    assert(1 == vl->int_nb);/* only support nb=1 for now */

    /* compute vector,volume,block,site dim/sizes */
    vl->num_size     = qlm_num_reals(vl->domain) * qlm_real_size(prec);
    vl->elem_num_len = qlm_elem_num_len(vl);
    vl->elem_size    = vl->num_size * vl->elem_num_len;

    /* FIXME these must be removed; kept only as legacy to make sense of the old code */
    vl->site_num_len = vl->elem_num_len * vl->arr_block;   /* FIXME remove */
    vl->site_size    = vl->site_num_len * vl->num_size;    /* FIXME remove */

    qlua_sublattice(xlo, xhi, QDP_this_node, (void *)S);
    vl->x0_parity = 0;
    vl->vol_site_len = 1;
    for (int i = 0; i < vl->ndim ; i++) {
        vl->x0[i] = xlo[i];
        vl->x0_parity ^= (vl->x0[i] & 1);
        vl->vol_site_dim[i] = xhi[i] - xlo[i];
        vl->vol_site_len *= vl->vol_site_dim[i];

    }
    if (IS_SUBLAT_EOPC(sublat)) {
        if (0 != vl->vol_site_len % 2) {
            qlm_error = QLM_ERR_GEOM_MISMATCH;
            return 1;
        }
        vl->vol_site_len /= 2;
    }
    
    /* XXX number of elems = vol(_eo?) * int_nb * arr_len */
    vl->site_elem_len= vl->arr_block;
    /* vec_elem_len == vol_site_len *site_elem_len * (arr_nb *int_nb) ; 
       how to name (arr_nb*int_nb) ?*/
    vl->vec_elem_len = vl->vol_site_len * (vl->arr_len * vl->int_nb); 
    vl->vec_num_len  = vl->vec_elem_len * vl->elem_num_len;
    vl->vec_size     = vl->vec_num_len * vl->num_size;
    
    /* initialize blocks */
    if (!is_blocked) {
        /* safe values: exactly 1 block */
        for (int i = 0; i < vl->ndim ; i++) {
            vl->blk_site_dim[i]  = vl->vol_site_dim[i];
            vl->vol_blk_dim[i]   = 1;
        }
        vl->blk_site_len = vl->vol_site_len;
        vl->vol_blk_len  = 1;
    }
    else {
        if (NULL == blk_site_dim) {
            qlm_error = QLM_ERR_GEOM_MISMATCH;
            return 1;
        }
        vl->blk_site_len = 1;
        vl->vol_blk_len  = 1;
        for (int i = 0; i < vl->ndim ; i++) {
            vl->blk_site_dim[i] = blk_site_dim[i];
            vl->blk_site_len *= vl->blk_site_dim[i];
            /* subdivide vecdim into blkdim */
            if (0 != vl->vol_site_dim[i] % vl->blk_site_dim[i]) {
                qlm_error = QLM_ERR_GEOM_MISMATCH;
                return 1;
            }
            vl->vol_blk_dim[i] = vl->vol_site_dim[i] / vl->blk_site_dim[i];
            vl->vol_blk_len  *= vl->vol_blk_dim[i];
        }
        /* if eopc, check that block volume is even */
        if (IS_SUBLAT_EOPC(vl->sublat)) {
            if (0 != vl->blk_site_len % 2) {
                qlm_error = QLM_ERR_GEOM_MISMATCH;
                return 1;
            }
            vl->blk_site_len /= 2;
        }
    }
    vl->blk_elem_len = vl->blk_site_len * vl->site_elem_len; /* x(blocked array dim) */
    vl->blk_num_len  = vl->blk_elem_len * vl->elem_num_len;
    vl->blk_size     = vl->blk_num_len * vl->num_size;
    vl->vec_blk_len  = vl->vol_blk_len * (vl->arr_nb * vl->int_nb);

    return 0;
}

const char * 
qlm_veclayout_cmp(const qlmVecLayout *vl,
    mLattice *S,                /* if dnc: set NULL */
    qlmSublat sublat,           /* if dnc: set QLM_SUBLAT_NONE */
    qlmLatField lftype,         /* if dnc: set QLM_LAT_NONE */
    int is_array,               /* if dnc: set -1; arr_len is ignored */
    int arr_len,                /* if dnc: set -1 */
    int ns, int nc,             /* if dnc: set -1 */
    qlmPrec prec,               /* if dnc: set QLM_PREC_NONE */
    const int *blk_site_dim,    /* if dnc: set NULL */
    int arr_block, int int_nb   /* if dnc: set -1 */
    )
#define CMP_DEF(x, xdef) do { if ((xdef) != (x) && ((vl->x) != (x))) return #x; } while(0)
{
    CMP_DEF(S, NULL);
    CMP_DEF(sublat, QLM_SUBLAT_NONE);
    CMP_DEF(lftype, QLM_LAT_NONE);
    if (0 <= is_array) {
        if (    (0 == is_array &&  vl->is_array) ||
                (0  < is_array && !vl->is_array))
            return "is_array";
        CMP_DEF(arr_len, -1);
    }
    CMP_DEF(ns, -1);
    CMP_DEF(nc, -1);
    CMP_DEF(prec, QLM_PREC_NONE);
    CMP_DEF(arr_block, -1);
    CMP_DEF(int_nb, -1);
    if (NULL != blk_site_dim) {
        for (int d = 0 ; d <= vl->ndim ; d++) 
            if (vl->blk_site_dim[d] != blk_site_dim[d])
                return "blk_site_dim";
    }
    return NULL;    /* OK */
#undef CMP_DEF_M1
}

qlmData *
qlm_data_alloc(
        mLattice *S, 
        qlmSublat sublat, 
        int nvec, 
        qlmLatField lftype, 
        int is_array, 
        int arr_len, 
        int ns, 
        int nc,
        qlmPrec prec, 
        int is_blocked, 
        const int blk_site_dim[], 
        int arr_block,
        int int_nb,
        int nvec_basis)
{
    assert( (!is_blocked && nvec_basis == nvec) || 
            ( is_blocked && nvec_basis <= nvec) );

    LONG_T vec_data_size, 
           blk_coeff_size;
    qlmData *d  = NULL;
    qlmVecLayout *vl = NULL;

    d           = malloc(sizeof(qlmData));
    if (NULL == d) {
        qlm_error = QLM_ERR_ENOMEM;
        return NULL;
    }
    /* set pointers so that it is safe to call qlm_data_free for cleanup */
    d->vec_data = d->blk_coeff = d->workM = d->workN 
        = d->work_blk = d->grbuf = NULL;
    /* init layout */
    vl  = &d->vl;
    if (qlm_veclayout_init(vl, S, sublat, lftype, 
            is_array, arr_len, ns, nc, prec, 
            is_blocked, blk_site_dim, arr_block, int_nb)) 
        goto clearerr_1;
    
    d->nvec         = nvec;
    d->nvec_basis   = nvec_basis;
    d->is_blocked   = is_blocked;

    vec_data_size   = d->nvec_basis * vl->vec_size;
    d->vec_data     = malloc(vec_data_size);
    d->workM        = malloc(vl->vec_size);
    d->workN        = malloc(d->nvec * vl->num_size);
    /* grbuf is used only for QMP_sum_double */
    d->grbuf_size   = d->nvec * qlm_num_reals(vl->domain) * sizeof(double);
    d->grbuf        = malloc(d->grbuf_size);
    if (   NULL == d->vec_data
        || NULL == d->workM
        || NULL == d->workN
        || NULL == d->grbuf
            ) {
        qlm_error = QLM_ERR_ENOMEM;
        goto clearerr_1;
    }
    memset(d->vec_data, 0, vec_data_size);

    d->cblk_num_len     = vl->vec_blk_len;
    d->cblk_size        = vl->num_size * d->cblk_num_len;
    d->cblkvec_num_len  = d->nvec_basis * vl->vec_blk_len;
    d->cblkvec_size     = d->cblkvec_num_len * vl->num_size;

    if (is_blocked) {
        blk_coeff_size = d->nvec * d->cblkvec_size;
        d->blk_coeff    = malloc(blk_coeff_size);
        d->work_blk     = malloc(d->cblkvec_size);
        if (    NULL == d->blk_coeff ||
                NULL == d->work_blk) {
            qlm_error = QLM_ERR_ENOMEM;
            goto clearerr_1;
        }
        memset(d->blk_coeff, 0, blk_coeff_size);
    } 

    return d;

clearerr_1:
    qlm_data_free(d);
    return NULL;
}

void qlm_data_free(qlmData *qlm)
{
    if (NULL == qlm) return;
    free_not_null(qlm->vec_data);
    free_not_null(qlm->blk_coeff);
    free_not_null(qlm->workM);
    free_not_null(qlm->workN);
    free_not_null(qlm->work_blk);
    free_not_null(qlm->grbuf);
    free(qlm);
}

/* TODO move to library */
/* print array of "type"s separated by delimeters
   XXX USE WITH CARE - MOSTLY FOR DEBUGGING */
#define len_arr(a)   (sizeof(a)/sizeof(a[0]))
#define snprintf_arr(str_, size_, format, delim, type, ptr_, n_) \
do { \
    int size = size_, dlen, delim_len = strlen(delim); \
    type *ptr = ptr_; \
    char *str = str_; \
    for (int n = n_ ; 0 < n && 0 < size ; n--, ptr++) { \
        dlen = snprintf(str, size, format, *ptr); \
        size -= dlen;  str += dlen; \
        if (size <= 0) break; \
        if (1 < n) { \
            if (delim_len < size) { \
                strncpy(str, delim, size); size -= delim_len;  str += delim_len; \
            } else break; \
        }\
    }\
} while(0)





/*! import latvec from Qlua stack -0+0 */
int
qlm_import_latvec_qlua(lua_State *L, qlmVecLayout *vl, void *restrict dst, int src_idx)
{
    vqdp *vec = qlm_check_vqdp(L, src_idx, vl);
    if (NULL == vec) {
        vqdp_free(vec);
        return luaL_error(L, "expect vtype at #%d; %s", src_idx, qlm_strerror(qlm_error));
    }
    vqdp_expose(vec);
    if (qlm_import_latvec_vqdp(vl, dst, vec)) {
        vqdp_reset(vec);
        vqdp_free(vec);
        return luaL_error(L, "import error: %s", qlm_strerror(qlm_error));
    }
    vqdp_reset(vec);
    vqdp_free(vec);
    return 0;
}

/*! export(push) latvec to Qlua stack -0+1 */
int
qlm_export_latvec_qlua(lua_State *L, qlmVecLayout *vl, int Sidx, const void * restrict src)
{
    vqdp *vec = qlm_new_vqdp(L, Sidx, vl);
    if (NULL == vec) {
        vqdp_free(vec);
        return luaL_error(L, "failed to create lattice object (qlm_new_vqdp); %s", 
                qlm_strerror(qlm_error));
    }
    vqdp_expose(vec);
    if (qlm_export_latvec_vqdp(vec, vl, src)) {
        vqdp_reset(vec);
        vqdp_free(vec);
        return luaL_error(L, "export error: %s", qlm_strerror(qlm_error));
    }
    vqdp_reset(vec);
    vqdp_free(vec);
    return 0;    
}

#define cplx_qla2int(int_x,qla_x) int_x = QLA_real(qla_x) + I*QLA_imag(qla_x)
#define cplx_int2qla(qla_x,int_x) QLA_c_eq_r_plus_ir((qla_x), creal(int_x), cimag(int_x))

/*! import vector from Qlua stack -0+0 */
int 
qlm_import_vector_qlua(lua_State *L, qlmVecLayout *vl, 
        LONG_T len, void *restrict dst, int idx)
{
    if (QLM_REAL == vl->domain) {
        mVecReal *q_v = qlua_checkVecReal(L, idx);
        if (len < q_v->size)
            return luaL_error(L, "vector import: insufficient buffer");
        if (QLM_PREC_SINGLE == vl->prec) { /* s */
            float *v = dst;
            for (LONG_T i = 0 ; i < q_v->size ; i++) v[i] = q_v->val[i];
            for (LONG_T i = q_v->size ; i < len ; i++) v[i] = 0.;
        } else if(QLM_PREC_DOUBLE== vl->prec) { /* d */
            double *v = dst;
            for (LONG_T i = 0 ; i < q_v->size ; i++) v[i] = q_v->val[i];
            for (LONG_T i = q_v->size ; i < len ; i++) v[i] = 0.;
        } else return luaL_error(L, "unsupported domain/precision");
    } else if (QLM_CPLX == vl->domain) {
        mVecComplex *q_v = qlua_checkVecComplex(L, idx);
        if (len < q_v->size)
            return luaL_error(L, "vector import: insufficient buffer");
        if (QLM_PREC_SINGLE == vl->prec) { /* c */
            float complex *v = dst;
            for (LONG_T i = 0 ; i < q_v->size ; i++) cplx_qla2int(v[i], q_v->val[i]);
            for (LONG_T i = q_v->size ; i < len ; i++) v[i] = 0.;
        } else if (QLM_PREC_DOUBLE== vl->prec) { /* z */
            double complex *v = dst;
            for (LONG_T i = 0 ; i < q_v->size ; i++) cplx_qla2int(v[i], q_v->val[i]);
            for (LONG_T i = q_v->size ; i < len ; i++) v[i] = 0.;
        } else return luaL_error(L, "unsupported domain/precision");
    } else return luaL_error(L, "unsupported domain");
    return 0;
}

/*! export(push) vector to Qlua stack -0+1 */
int 
qlm_export_vector_qlua(lua_State *L, qlmVecLayout *vl, 
        LONG_T len, const void *restrict src)
{
    if (QLM_REAL == vl->domain) {
        mVecReal *q_v = qlua_newVecReal(L, len);
        if (NULL == q_v) return luaL_error(L, "not enough memory");
        memset(q_v->val, 0, q_v->size * sizeof(q_v->val[0]));
        if (QLM_PREC_SINGLE == vl->prec) { /* s */
            const float *v = src;
            for (LONG_T i = 0 ; i < len ; i++) q_v->val[i] = v[i];
        } else if(QLM_PREC_DOUBLE== vl->prec) { /* d */
            const double *v = src;
            for (LONG_T i = 0 ; i < len ; i++) q_v->val[i] = v[i];
        } else return luaL_error(L, "unsupported domain/precision");
    } else if (QLM_CPLX == vl->domain) {
        mVecComplex *q_v = qlua_newVecComplex(L, len);
        if (NULL == q_v) return luaL_error(L, "not enough memory");
        memset(q_v->val, 0, q_v->size * sizeof(q_v->val[0]));
        if (QLM_PREC_SINGLE == vl->prec) { /* c */
            const float complex *v = src;
            for (LONG_T i = 0 ; i < len ; i++) cplx_int2qla(q_v->val[i], v[i]);
        } else if (QLM_PREC_DOUBLE== vl->prec) { /* z */
            const double complex *v = src;
            for (LONG_T i = 0 ; i < len ; i++) cplx_int2qla(q_v->val[i], v[i]);
        } else return luaL_error(L, "unsupported domain/precision");
    } else return luaL_error(L, "unsupported domain");
    return 0;
}

static int 
qlm_gslmatrix_set(qlmVecLayout *vl, void *matr_, int i, int j, const void *restrict x_)
{
    if (QLM_REAL == vl->domain) {
        double r;
        if (QLM_PREC_SINGLE == vl->prec) r = *(float *)x_;
        else if(QLM_PREC_DOUBLE== vl->prec) r = *(double *)x_;
        else assert(NULL == "impossible");
        gsl_matrix_set((gsl_matrix *)matr_, i, j, r);
    } else if (QLM_CPLX == vl->domain) {
        double complex c;
        if (QLM_PREC_SINGLE == vl->prec) c = *(float complex *)x_;
        else if (QLM_PREC_DOUBLE== vl->prec) c = *(double complex *)x_;
        else assert(NULL == "impossible");
        gsl_matrix_complex_set((gsl_matrix_complex *)matr_, i, j, 
                    gsl_complex_rect(creal(c), cimag(c)));
    } else assert(NULL == "impossible");
    return 0;
}
static int 
qlm_gslmatrix_get(qlmVecLayout *vl, void *restrict x_, void *matr_, int i, int j)
{
    if (QLM_REAL == vl->domain) {
        double r = gsl_matrix_get((gsl_matrix *)matr_, i, j);
        if (QLM_PREC_SINGLE == vl->prec) *(float *)x_ = r;
        else if(QLM_PREC_DOUBLE== vl->prec) *(double *)x_ = r;
        else assert(NULL == "impossible");
    } else if (QLM_CPLX == vl->domain) {
        gsl_complex c = gsl_matrix_complex_get((gsl_matrix_complex *)matr_, i, j);
        if (QLM_PREC_SINGLE == vl->prec) 
            *(float complex *)x_ = GSL_REAL(c) + I*GSL_IMAG(c);
        else if (QLM_PREC_DOUBLE== vl->prec)
            *(double complex *)x_ = GSL_REAL(c) + I*GSL_IMAG(c);
        else assert(NULL == "impossible");
    } else 
        assert(NULL == "impossible");
    return 0;
}
static void
qlm_gslmatrix_rset(qlmVecLayout *vl, void *matr_, int i, int j, double x)
{
    if (QLM_REAL == vl->domain)
        gsl_matrix_set((gsl_matrix *)matr_, i, j, x);
    else if (QLM_CPLX == vl->domain)
        gsl_matrix_complex_set((gsl_matrix_complex *)matr_, i, j, gsl_complex_rect(x, 0.));
    else assert(NULL == "impossible");
}
static double
qlm_gslmatrix_rget(qlmVecLayout *vl, void *matr_, int i, int j)
{
    if (QLM_REAL == vl->domain)
        return gsl_matrix_get((gsl_matrix *)matr_, i, j);
    else if (QLM_CPLX == vl->domain)
        return GSL_REAL(gsl_matrix_complex_get((gsl_matrix_complex *)matr_, i, j));
    else assert(NULL == "impossible");
    return 0;
}
static void
qlm_ptr_rset(qlmVecLayout *vl, void *x_, double r)
{
    if (QLM_REAL == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) *(float *)x_ = r;
        else if(QLM_PREC_DOUBLE== vl->prec) *(double *)x_ = r;
        else assert(NULL == "impossible");
    } else if (QLM_CPLX == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) *(float complex *)x_ = r;
        else if(QLM_PREC_DOUBLE== vl->prec) *(double complex *)x_ = r;
        else assert(NULL == "impossible");
    } else assert(NULL == "impossible");
}
static double
qlm_ptr_rget(qlmVecLayout *vl, void *x_)
{
    if (QLM_REAL == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) return *(float *)x_;
        else if(QLM_PREC_DOUBLE== vl->prec) return *(double *)x_;
        else assert(NULL == "impossible");
    } else if (QLM_CPLX == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) return crealf(*(float complex *)x_);
        else if(QLM_PREC_DOUBLE== vl->prec) return creal(*(double complex *)x_);
        else assert(NULL == "impossible");
    } else assert(NULL == "impossible");
    return 0;
}

/***********************************************************************
 * internal linear algebra 
 ***********************************************************************/
//    if (QLM_REAL == d->domain) {
//        if (QLM_PREC_SINGLE == d->prec) { /* s */
//        } else if(QLM_PREC_DOUBLE== d->prec) { /* d */
//        } else return luaL_error(L, "unsupported domain/precision");
//    } else if (QLM_CPLX == d->domain) {
//        if (QLM_PREC_SINGLE == d->prec) { /* c */
//        } else if (QLM_PREC_DOUBLE== d->prec) { /* z */
//        } else return luaL_error(L, "unsupported domain/precision");
//    } else return luaL_error(L, "unsupported domain");
static int
qlm_global_sum_(qlmData *d, LONG_T len_num, void *buf_)
{
    qlmVecLayout *vl = &d->vl;
    if ((long long)d->grbuf_size < (long long)(len_num 
                * qlm_num_reals(vl->domain) * sizeof(double))) {
        qlm_error = QLM_ERR_ENOMEM; 
        return 1;
    }
    assert((long long)(len_num * qlm_num_reals(vl->domain) * sizeof(double))
            <= (long long)d->grbuf_size);
    double *grbuf = d->grbuf;
    assert(NULL != grbuf);

    if (QLM_REAL == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* s */
            float *buf = (float *)buf_;
            for (LONG_T j = 0 ; j < len_num ; j++) grbuf[j] = buf[j];
            QMP_sum_double_array(grbuf, len_num);
            for (LONG_T j = 0 ; j < len_num ; j++) buf[j] = grbuf[j];
        } else if(QLM_PREC_DOUBLE== vl->prec) { /* d */
            double *buf = (double *)buf_;
            for (LONG_T j = 0 ; j < len_num ; j++) grbuf[j] = buf[j];
            QMP_sum_double_array(grbuf, len_num);
            for (LONG_T j = 0 ; j < len_num ; j++) buf[j] = grbuf[j];
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else if (QLM_CPLX == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* c */
            float complex *buf = (float complex *)buf_;
            for (LONG_T j = 0 ; j < len_num ; j++) {
                grbuf[2*j  ] = crealf(buf[j]);
                grbuf[2*j+1] = cimagf(buf[j]);
            }
            QMP_sum_double_array(grbuf, 2 * len_num);
            for (LONG_T j = 0 ; j < len_num ; j++) 
                buf[j] = grbuf[2*j] + I * grbuf[2*j+1];
        } else if (QLM_PREC_DOUBLE== vl->prec) { /* z */
            double complex *buf = (double complex *)buf_;
            for (LONG_T j = 0 ; j < len_num ; j++) {
                grbuf[2*j  ] = creal(buf[j]);
                grbuf[2*j+1] = cimag(buf[j]);
            }
            QMP_sum_double_array(grbuf, 2 * len_num);
            for (LONG_T j = 0 ; j < len_num ; j++) 
                buf[j] = grbuf[2*j] + I * grbuf[2*j+1];
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 1;
    }
    return 0;
}
/*! prec/domain-dependent blas matrix*vector */
int 
qlm_gemv(qlmVecLayout *vl, int m, int n, 
        const void *restrict alpha, const void *restrict a, int lda, const void *x, int incx,
        const void *restrict beta, void *restrict y, int incy)
{
    if (QLM_REAL == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* s */
            cblas_sgemv(CblasColMajor, CblasNoTrans, m, n,
                    *(const float *)alpha, a, lda, x, incx,
                    *(const float *)beta, y, incy);
        } else if(QLM_PREC_DOUBLE== vl->prec) { /* vl */
            cblas_dgemv(CblasColMajor, CblasNoTrans, m, n,
                    *(const double *)alpha, a, lda, x, incx,
                    *(const double *)beta, y, incy);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else if (QLM_CPLX == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* c */
            cblas_cgemv(CblasColMajor, CblasNoTrans, m, n,
                    alpha, a, lda, x, incx, beta, y, incy);
        } else if (QLM_PREC_DOUBLE== vl->prec) { /* z */
            cblas_zgemv(CblasColMajor, CblasNoTrans, m, n,
                    alpha, a, lda, x, incx, beta, y, incy);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 1;
    }
    return 0;
}

/*! prec/domain-dependent blas matrix^H*vector */
int 
qlm_gemxv(qlmVecLayout *vl, int m, int n, 
        const void *restrict alpha, const void *restrict a, int lda, const void *x, int incx,
        const void *restrict beta, void *restrict y, int incy)
{
    if (QLM_REAL == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* s */
            cblas_sgemv(CblasColMajor, CblasTrans, m, n,
                    *(const float *)alpha, a, lda, x, incx,
                    *(const float *)beta, y, incy);
        } else if(QLM_PREC_DOUBLE== vl->prec) { /* d */
            cblas_dgemv(CblasColMajor, CblasTrans, m, n,
                    *(const double *)alpha, a, lda, x, incx,
                    *(const double *)beta, y, incy);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else if (QLM_CPLX == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* c */
            cblas_cgemv(CblasColMajor, CblasConjTrans, m, n,
                    alpha, a, lda, x, incx, beta, y, incy);
        } else if (QLM_PREC_DOUBLE== vl->prec) { /* z */
            cblas_zgemv(CblasColMajor, CblasConjTrans, m, n,
                    alpha, a, lda, x, incx, beta, y, incy);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 1;
    }
    return 0;
}
int 
qlm_scal(qlmVecLayout *vl, int n, const void *alpha, void *restrict x, int incx)
{
    if (QLM_REAL == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* s */
            cblas_sscal(n, *(const float *)alpha, x, incx);
        } else if(QLM_PREC_DOUBLE== vl->prec) { /* d */
            cblas_dscal(n, *(const double *)alpha, x, incx);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else if (QLM_CPLX == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* c */
            cblas_cscal(n, alpha, x, incx);
        } else if (QLM_PREC_DOUBLE== vl->prec) { /* z */
            cblas_zscal(n, alpha, x, incx);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 1;
    }
    return 0;
}
int 
qlm_rscal(qlmVecLayout *vl, int n, double alpha, void *restrict x, int incx)
{
    if (QLM_REAL == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* s */
            cblas_sscal(n, alpha, x, incx);
        } else if(QLM_PREC_DOUBLE== vl->prec) { /* d */
            cblas_dscal(n, alpha, x, incx);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else if (QLM_CPLX == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* c */
            cblas_csscal(n, alpha, x, incx);
        } else if (QLM_PREC_DOUBLE== vl->prec) { /* z */
            cblas_zdscal(n, alpha, x, incx);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 1;
    }
    return 0;
}
double
qlm_nrm2(qlmVecLayout *vl, int n, const void *restrict x, int incx)
{
    if (QLM_REAL == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* s */
            return cblas_snrm2(n, x, incx);
        } else if(QLM_PREC_DOUBLE== vl->prec) { /* d */
            return cblas_dnrm2(n, x, incx);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 0;
        }
    } else if (QLM_CPLX == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* c */
            return cblas_scnrm2(n, x, incx);
        } else if (QLM_PREC_DOUBLE== vl->prec) { /* z */
            return cblas_dznrm2(n, x, incx);
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 0;
        }
    } else {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 0;
    }
    return 0;
}
/*! multiplication element by element */
int 
qlm_mult_vector_(const qlmVecLayout *vl, int n,
        void *restrict *x_, const void *restrict y_)
{
    if (QLM_REAL == vl->domain) {

        if (QLM_PREC_SINGLE == vl->prec) { /* s */
            float *x = (float *)x_;
            const float *y = (const float *)y_;
#pragma omp parallel for
            for (int i = 0 ; i < n ; i++) x[i] *= y[i];
            return 0;
        } else if(QLM_PREC_DOUBLE== vl->prec) { /* d */
            double *x = (double *)x_;
            const double *y = (const double *)y_;
#pragma omp parallel for
            for (int i = 0 ; i < n ; i++) x[i] *= y[i];
            return 0;
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else if (QLM_CPLX == vl->domain) {
        if (QLM_PREC_SINGLE == vl->prec) { /* c */
            float complex *x = (float complex *)x_;
            const float complex *y = (const float complex *)y_;
#pragma omp parallel for
            for (int i = 0 ; i < n ; i++) x[i] *= y[i];
            return 0;
        } else if (QLM_PREC_DOUBLE== vl->prec) { /* z */
            double complex *x = (double complex *)x_;
            const double complex *y = (const double complex *)y_;
#pragma omp parallel for
            for (int i = 0 ; i < n ; i++) x[i] *= y[i];
            return 0;
        } else {
            qlm_error = QLM_ERR_ENUM_ERROR;
            return 1;
        }
    } else {
        qlm_error = QLM_ERR_ENUM_ERROR;
        return 1;
    }
    return 0;
}


float          sZero = 0., sOne = 1., sMone = -1.;
double         dZero = 0., dOne = 1., dMone = -1.;
float complex  cZero = 0., cOne = 1., cMone = -1.;
double complex zZero = 0., zOne = 1., zMone = -1.;


/* XXX if is_blocked, B is 3-index col-major matrix in vec_data:
        B[m,b,q] = vec_data [q + Nq*b + Nq*Nb*m]
        C[j,m,b] = blk_coeff[b + Nb*m + Nb*Nm*j]
        wblk[m,b]= work_blk [b + Nb*m]
        loop over b (no summation): do matrix*vector with b fixed: 
            use gemv with lda = Nq*Nb, offset=Nq*b */

/*! reconstruct from blocks using block-basis range [mvec0:mvec1)
    y_{m(b)} = sum_q B_{m(b)q}^* v_{(b)q}, for each b 
 */
int 
qlm_latmv_block_partial_(qlmData *d, int mvec0, int mvec1,
        const void *restrict alpha, const void *restrict x_,
        const void *restrict beta, void *restrict lat_y_)
{
    assert(d->is_blocked);
    assert(0 <= mvec0);
    assert(mvec1 <= d->nvec_basis);
    assert(mvec0 < mvec1);

    qlmVecLayout *vl = &d->vl;
    LONG_T lenB = vl->vec_blk_len,  /* number of blocks Nb */
           lenQ = vl->blk_num_len;  /* length of block Nq */
    int    len_b_orig = mvec1 - mvec0;  /* number of orig.basis vectors to use */
    LONG_T vs_  = vl->vec_size,
           bs_  = vl->blk_size,
           ns_  = vl->num_size;
    const void *vdata_ = d->vec_data + mvec0 * vs_;

#pragma omp parallel for
    for (LONG_T ib = 0 ; ib < lenB ; ib++) 
        qlm_gemv(vl, lenQ, len_b_orig,
                alpha, vdata_ + ib * bs_, lenQ * lenB, 
                x_ + ib * ns_, lenB, 
                beta, lat_y_ + ib * bs_, 1);

    return 0;
}
int 
qlm_latmv_block_(qlmData *d,
        const void *restrict alpha, const void *restrict x_,
        const void *restrict beta, void *restrict lat_y_)
{
    return qlm_latmv_block_partial_(d, 0, d->nvec_basis, 
            alpha, x_, beta, lat_y_);
}

/*! project onto blocks using block-basis range [mvec0:mvec1)
    y_{m(b)} = sum_q B_{m(b)q}^* v_{(b)q}, for each b */
int 
qlm_latmxv_block_partial_(qlmData *d, int mvec0, int mvec1,
        const void *restrict alpha, const void *restrict lat_x_,
        const void *restrict beta, void *restrict y_)
{
    assert(d->is_blocked);
    assert(0 <= mvec0);
    assert(mvec1 <= d->nvec_basis);
    assert(mvec0 < mvec1);

    qlmVecLayout *vl = &d->vl;
    LONG_T lenB = vl->vec_blk_len,  /* number of blocks Nb */
           lenQ = vl->blk_num_len;  /* length of block Nq */
    int    len_b_orig = mvec1 - mvec0;  /* number of orig.basis vectors to use */
    LONG_T vs_  = vl->vec_size,
           bs_  = vl->blk_size,
           ns_  = vl->num_size;
    const void *vdata_ = d->vec_data + mvec0 * vs_;

#pragma omp parallel for
    for (LONG_T ib = 0 ; ib < lenB ; ib++) 
        qlm_gemxv(vl, lenQ, len_b_orig,
                alpha, vdata_ + ib * bs_, lenQ * lenB, 
                lat_x_ + ib * bs_, 1, 
                beta, y_ + ib * ns_, lenB);

    return 0;
}
int 
qlm_latmxv_block_(qlmData *d, 
        const void *restrict alpha, const void *restrict lat_x_,
        const void *restrict beta, void *restrict y_)
{
    return qlm_latmxv_block_partial_(d, 0, d->nvec_basis, 
            alpha, lat_x_, beta, y_);
}

/*! orthogonalize within blocks using block-basis range [mvec0:mvec1) 
    (really only a combination lf latmxv_block and latmv_block in a common loop)
    save coeffs of orig_vector into cblk_d[0:mvec_end, b], cblk_v[b]
 */
int 
qlm_ortho_block_partial_(qlmData *d, int mvec0, int mvec1, 
        void *restrict v_, void *restrict cblk_v_)
{  
    assert(d->is_blocked);

    qlmVecLayout *vl = &d->vl;
    LONG_T lenB = vl->vec_blk_len,  /* number of blocks Nb */
           lenQ = vl->blk_num_len,  /* length of block Nq */
           lenM = mvec1 - mvec0;
    LONG_T vs_ = vl->vec_size,
           bs_ = vl->blk_size,
           ns_ = vl->num_size;
    const void *vdata_= d->vec_data + mvec0 * vs_;
    for (LONG_T ib = 0 ; ib < lenB ; ib++) {
        qlm_gemxv(vl, lenQ, lenM, 
                qlm_pone(vl), vdata_ + ib * bs_, lenQ * lenB,
                v_ + ib * bs_, 1, 
                qlm_pzero(vl), cblk_v_ + ib * ns_, lenB);
        /* no global sum: blocks are local */
        qlm_gemv(vl, lenQ, lenM,
                qlm_pmone(vl), vdata_ + ib * bs_, lenQ * lenB,
                cblk_v_ + ib * ns_, lenB,
                qlm_pone(vl), v_ + ib * bs_, 1);
    }
    return 0;
}

/*! insert block-basis vector [mvec] orthogonalizing it against 
    block-basis vectors [0,mvec) and storing coeffs in blk_coeff
    all blocks in b_bblk_ must be linearly independent from prev. blocks

    XXX modifies vec_data, blk_coeff
 */
int 
qlm_insert_block_basis_(qlmData *d, int mvec, const void *restrict v_)
{
    assert(d->is_blocked);
    assert(0 <= mvec);
    assert(mvec < d->nvec_basis);

    qlmVecLayout *vl = &d->vl;
    LONG_T lenB = vl->vec_blk_len,  /* number of blocks Nb */
           lenQ = vl->blk_num_len;  /* length of block Nq */
    LONG_T ns_  = vl->num_size,
           bs_  = vl->blk_size,
           cbs_ = d->cblkvec_size,
           vs_  = vl->vec_size;
    void *v_store_  = d->vec_data + mvec * vs_,
         *cblk_v_   = d->blk_coeff + mvec * cbs_;
    /* check that no vector has been inserted here */
    double nrm = qlm_nrm2(vl, d->cblkvec_num_len, cblk_v_, 1);
    assert(0 == nrm);
    memcpy(v_store_, v_, vs_);
    qlm_ortho_block_partial_(d, 0, mvec, v_store_, cblk_v_);
    for (LONG_T ib = 0 ; ib < lenB ; ib++) {
        double bnrm = qlm_nrm2(vl, lenQ, v_store_ + ib * bs_, 1);
        assert(0 < bnrm);
        /* store diagonal elem. of R[ib] */ 
        qlm_rscal(vl, lenQ, 1. / bnrm, v_store_ + ib * bs_, 1);
        qlm_ptr_rset(vl, cblk_v_ + (ib + mvec * lenB) * ns_, bnrm);
    }
    d->last_nvec = mvec;
    return 0;
}

/*! insert extra vector: decompose v_ in the block basis and store coefficients 
    XXX modifies blk_coeff
 */
int 
qlm_insert_block_extra_(qlmData *d, int mvec, const void *restrict v_)
{
    assert(d->is_blocked);
    assert(d->nvec_basis <= mvec);
    assert(mvec < d->nvec);

    qlmVecLayout *vl = &d->vl;
    void *cblk_v_ = d->blk_coeff + mvec * d->cblkvec_size;
    d->last_nvec = mvec;
    /* TODO check that block-basis is full? */
    return qlm_latmxv_block_(d, qlm_pone(vl), v_, qlm_pzero(vl), cblk_v_);
}

/*! convert vector layout 
    return 0 on OK, 1 on fail (qlm_error set) 

    layout      if not NONE, override layout in d
 */
#if 0
int 
qlm_conv_layout(qlmData *d, qlmLayout new_layout)
{
    assert(NULL == "not implemented");  /* TODO apply vec->vol change appropriately */
    if (d->layout == new_layout)
        return 0;
    LONG_T *idx_dst = NULL;
    void *vdata_;
    void *wk_   = d->workM;
    LONG_T vs_  = d->vec_size,
           ss_  = d->site_size,
           vs_idx_src;
    int vs_coord[QLUA_MAX_LATTICE_RANK];
    LONG_T ivec;

    site_layout_x2i src_x2i = get_site_layout_x2i(d, d->layout);
    site_layout_i2x dst_i2x = get_site_layout_i2x(d, new_layout);

    /* build index */
    if (NULL == (idx_dst = malloc(d->vec_site_len * sizeof(idx_dst[0])))) {
        qlm_error = QLM_ERR_ENOMEM;
        goto clearerr_1;
    }
    for (LONG_T vs_idx_dst = 0 ; vs_idx_dst < d->vec_site_len ; vs_idx_dst++) {
        dst_i2x(vs_coord, vs_idx_dst, d);
        src_x2i(&vs_idx_src, vs_coord, d);
        idx_dst[vs_idx_src] = vs_idx_dst;
        if (vs_idx_src < 0) {
            qlm_error = QLM_ERR_GEOM_SITE;
            goto clearerr_1;
        }
    }
    /* transpose sites */
    for (ivec = 0, vdata_ = d->vec_data ; ivec < d->nvec_basis ; ivec++, 
            vdata_ += vs_) {
        memcpy(wk_, vdata_, vs_);
        for (LONG_T i = 0 ; i < d->vec_site_len ; i++)
            memcpy(vdata_ + idx_dst[i] * ss_, wk_ + i * ss_, ss_);
    }
    
    free(idx_dst);
    d->layout = new_layout;
    return 0;

clearerr_1:
    free_not_null(idx_dst);
    return 1;
}

/*! convert non-block-basis to block-basis with nvec_basis block-basis vectors
    
    have_nvec   number of vectors (from #0) that have been inserted 
                (the object does not keep track of it)

    XXX modifies vec_data, blk_coeff
 */
int 
qlm_conv_to_block(qlmData *d, 
        const int blk_site_dim[], qlmLayout layout,
        int nvec_basis, int have_nvec)
{
    assert(NULL == "not implemented"); /* TODO apply change vec->vol appropriately */
    if (d->is_blocked) {
        qlm_error = QLM_ERR_INVALID_STATE;
        goto clearerr_1;
    }
    if (    d->nvec < d->nvec_basis ||
            d->nvec < have_nvec     ||
            NULL == blk_site_dim) {
        qlm_error = QLM_ERR_INVALID_PARAM;
        goto clearerr_1;
    }
    switch (layout) {
    /* only one layout type is supported */
    case QLM_LAYOUT_BLKLEX: break;
    default : 
        qlm_error = QLM_ERR_INVALID_PARAM;
        goto clearerr_1;
    }
    if (d->layout != layout)
        qlm_conv_layout(d, layout);

    d->is_blocked = 1;
    d->nvec_basis = nvec_basis;

    /* FIXME this code is repeated: make a separate function */
    d->blk_site_len = 1;
    d->vol_blk_len  = 1;
    for (int i = 0; i < d->ndim ; i++) {
        d->blk_site_dim[i] = blk_site_dim[i];
        d->blk_site_len *= d->blk_site_dim[i];
        /* subdivide vecdim into blkdim */
        if (0 != d->vol_site_dim[i] % d->blk_site_dim[i]) {
            qlm_error = QLM_ERR_GEOM_MISMATCH;
            goto clearerr_1;
        }
        d->vol_blk_dim[i] = d->vol_site_dim[i] / d->blk_site_dim[i];
        d->vol_blk_len  *= d->vol_blk_dim[i];
    }
    if (IS_SUBLAT_EOPC(d->sublat)) {
        if (0 != d->blk_site_len % 2) {
            qlm_error = QLM_ERR_GEOM_MISMATCH;
            goto clearerr_1;
        }
        d->blk_site_len /= 2;
    } 
//    d->blk_num_len  = d->site_num_len * d->blk_site_len;
    d->blk_size     = d->num_size * d->blk_num_len;
    d->cblkvec_num_len = d->nvec_basis * d->vec_blk_len;
    d->cblkvec_size = d->num_size * d->cblkvec_num_len;

    LONG_T blk_coeff_size = d->nvec * d->cblkvec_size;
    d->blk_coeff    = malloc(blk_coeff_size);
    d->work_blk     = malloc(d->cblkvec_size);
    if (    NULL == d->blk_coeff || 
            NULL == d->work_blk) {
        qlm_error = QLM_ERR_ENOMEM;
        goto clearerr_1;
    }
    memset(d->blk_coeff, 0, blk_coeff_size);

    void *vdata_ = d->vec_data,
         *wk_    = d->workM;        /* XXX workspace */
    LONG_T vs_ = d->vec_size;
    /* extract and reinsert vectors as block-basis or regular 
       XXX block-qr coefficients are saved to blk_coeff automatically */
    /* FIXME do all inplace without memcpy, save workspace ? */
    for (int ivec = 0 ; ivec < have_nvec && ivec < d->nvec ; ivec++) {
        assert(NULL == "FIXME change site layout before reinserting vectors");
        memcpy(wk_, vdata_ + ivec *vs_, vs_);
        if (ivec < d->nvec_basis)
            qlm_insert_block_basis_(d, ivec, wk_);
        else
            qlm_insert_block_extra_(d, ivec, wk_);
    }
    return 0;

clearerr_1:
    return 1;
}
#endif

/*! reconstruct from basis vectors ((d->vec_data; also d->blk_coeff if is_blocked)
    BLAS.lev2-like synopsis: lat_y <- alpha * latmat.x + beta * lat_y 
    XXX uses uses work_blk (if is_blocked)
 */
int
qlm_latmv_(qlmData *d, 
        const void *restrict alpha, const void *restrict x_,
        const void *restrict beta, void *restrict lat_y_)
{   
    qlmVecLayout *vl = &d->vl;
    void *wblk_ = d->work_blk;  /* XXX wkspace if is_blocked */
    int status = 0;
    LONG_T lenM = vl->vec_num_len,  /* col(vertical) size of latmat Ns=Nb*Nq */
           lenN = d->nvec;         /* row(horizon.) size of latmat Nj */
    LONG_T len_b_blk  = d->cblkvec_num_len;   /* dim of blocked basis, Nm*Nb */
    const void *cblk_   = d->blk_coeff,
               *vdata_  = d->vec_data;

    if (d->is_blocked) {
        /* w_{mb} = sum_{j} C_{jmb} c_j */
        qlm_gemv(vl, len_b_blk, lenN, 
                qlm_pone(vl), cblk_, len_b_blk, x_, 1, qlm_pzero(vl), wblk_, 1);
        /* laty_{(b)q} = \sum_q B_{m(b)q} w_{m(b)} */
        if (0 != (status = qlm_latmv_block_(d, alpha, wblk_, beta, lat_y_)))
            return status;
    } else {
        /* laty_{bq} = sum_{j} B_{jbq}^* c_j */
        qlm_gemv(vl, lenM, lenN,
                alpha, vdata_, lenM, x_, 1, beta, lat_y_, 1);
    }
    
    return 0;
}

/*! project onto basis vectors (d->vec_data; also d->blk_coeff if is_blocked)
    BLAS.lev2-like synopsis: y <- alpha * latmat^H . lat_x + beta * y 
    XXX uses work_blk (if is_blocked)
    */
int 
qlm_latmxv_(qlmData *d, 
        const void *restrict alpha, const void *restrict lat_x_, 
        const void *restrict beta, void *restrict y_)
{   
    qlmVecLayout *vl = &d->vl;
    void *wblk_ = d->work_blk;  /* XXX workspace (if is_blocked) */
    int status = 0;
    LONG_T lenM = vl->vec_num_len,  /* col(vertical) size of latmat Ns=Nb*Nq */
           lenN = d->nvec;         /* row(horizon.) size of latmat Nj */
    LONG_T len_b_blk  = d->cblkvec_num_len;   /* dim of blocked basis, Nm*Nb */
    const void *cblk_    = d->blk_coeff,
               *vdata_   = d->vec_data;

    if (d->is_blocked) {    
        /* w_{m(b)} = \sum_q B_{m(b)q}^* latx_{(b)q} */
        if (0 != (status = qlm_latmxv_block_(d, qlm_pone(vl), lat_x_, 
                        qlm_pzero(vl), wblk_)))
            return status;
        /* c_j = sum_{mb} C_{jmb}^* w_{mb} */
        qlm_gemxv(vl, len_b_blk, lenN,
                alpha, cblk_, len_b_blk, wblk_, 1, beta, y_, 1);
    } else {    
        /* c_j = sum_{bq} B_{jbq}^* latx_{bq} */
        qlm_gemxv(vl, lenM, lenN, 
                alpha, vdata_, lenM, lat_x_, 1, beta, y_, 1);
    }
    if (0 !=  (status = qlm_global_sum_(d, lenN, y_)))
        return status;

    return 0;
}
    
/*! apply operator lat_x <- M . Diag{eval} . M^H . lat_x 
    lat_dst_ and lat_src_ may overlap
    XXX uses workN
 */
int 
qlm_latop_diag_(qlmData *d, 
        const void *restrict alpha, int n_eval, const void *restrict eval, 
        const void *lat_src_, const void *restrict beta, void *lat_dst_)
{
    int status = 0;
    qlmVecLayout *vl = &d->vl;
    assert(d->nvec <= n_eval);
    if (n_eval < d->nvec)
        return 1;
    if (0 != (status = qlm_latmxv_(d, qlm_pone(vl), lat_src_, 
                    qlm_pzero(vl), d->workN)))
        return status;
    if (0 != (status = qlm_mult_vector_(vl, d->nvec, d->workN, eval)))
        return status;
    if (0 != (status = qlm_latmv_(d, alpha, d->workN, beta, lat_dst_)))
        return status;
    return 0;
}


/***********************************************************************
 * interface
 ***********************************************************************/
static const char qlm_str[] = "latmat";
static const char qlm_name[] = "latmat";
static const char qlm_meta_name[] = "qcd.latmat";

/******** latmat methods ********/    
static int
q_qlm_fmt(lua_State *L)
{
    char fmt[256];
    char fmt_array[16]  = "";
    char fmt_sublat[16] = "";
    char fmt_block[64]  = "";
    char fmt_layout[64] = "";
    char fmt_field[64]  = "";
    const char *fmt_prec = NULL;
    const char *lftype_str = "";
    mQlm *c = qlua_checkQlm(L, 1, NULL);
    qlmData *d = c->d;
    qlmVecLayout *vl = &d->vl;
    lftype_str = qlm_lftype_str(vl->lftype);
    if (vl->is_array)
        snprintf(fmt_array, sizeof(fmt_array),
                " array[%d]", vl->arr_len);
    if (IS_SUBLAT_EOPC(vl->sublat)) 
        snprintf(fmt_sublat, sizeof(fmt_sublat), ",eo(%d)", SUBLAT_PARITY(vl->sublat));
    if (d->is_blocked) {
        snprintf_arr(fmt_block, sizeof(fmt_block), "%d", ",", 
                int, vl->blk_site_dim, vl->ndim);
        if (! vl->is_array)
            snprintf(fmt_layout, sizeof(fmt_layout), ",BLOCK({%s};nbasis=%d)", 
                    fmt_block, d->nvec_basis);
        else 
            snprintf(fmt_layout, sizeof(fmt_layout), ",BLOCK({%s},ab=%d,n_ib=%d;nbasis=%d)", 
                    fmt_block, vl->arr_block, vl->int_nb, d->nvec_basis);
    }
    fmt_prec = (QLM_PREC_SINGLE == vl->prec ? "single" : 
               (QLM_PREC_DOUBLE == vl->prec ? "double" : "UNKNOWN-PREC"));
    snprintf(fmt_field, sizeof(fmt_field), " nc=%d ns=%d", vl->nc, vl->ns);

    snprintf(fmt, sizeof(fmt), 
            "%s(%dx %s%s%s%s%s %s)", qlm_str, d->nvec, lftype_str, 
            fmt_array, fmt_field, fmt_sublat, fmt_layout, fmt_prec);
    lua_pushstring(L, fmt);

    return 1;
}
static int
q_qlm_gc(lua_State *L)
{
    mQlm *c = qlua_checkQlm(L, 1, NULL);

    if (NULL != c->d) {
        qlm_data_free(c->d);
        c->d = NULL;
    }

    return 0;
}
static int 
q_qlm_index(lua_State *L)
{
    mQlm *c; 
    c = qlua_checkQlm(L, 1, NULL);
    const char *what_str = luaL_checkstring(L, 2);

    if (!strcmp("lattice", what_str)) {
        lua_getfenv(L, 1);
        lua_getfield(L, -1, "lattice");
        return 1;
    } 
    /* add other read-only fields */
    else {
        return luaL_error(L, "invalid field: %s", what_str);
    }
}
static int 
q_qlm_is_array(lua_State *L)
{
    mQlm *c = qlua_checkQlm(L, 1, NULL);
    lua_pushboolean(L, c->d->vl.is_array);
    return 1;
}
static int q_qlm_array_len(lua_State *L)
{
    mQlm *c = qlua_checkQlm(L, 1, NULL);
    lua_pushinteger(L, c->d->vl.arr_len);
    return 1;
}
static int q_qlm_get_lattice(lua_State *L) 
{
    mQlm *c = NULL;
    c = qlua_checkQlm(L, 1, NULL);
    lua_getfenv(L, 1);
    lua_getfield(L, -1, "lattice");
    return 1;
}
static int q_qlm_get_nvec(lua_State *L)
{
    mQlm *c = qlua_checkQlm(L, 1, NULL);
    lua_pushinteger(L, c->d->nvec);
    return 1;
}


/*! [lua] latmat:insert(ivec, VEC) 
    XXX uses workM (if is_blocked) */
static int 
q_qlm_insert(lua_State *L)
{
    int status = 0;
    mQlm *c = qlua_checkQlm(L, 1, NULL);
    qlmData *d = c->d;

    int ivec = luaL_checkint(L, 2);
    if (ivec < 0 || d->nvec <= ivec) 
        luaL_error(L, "index %d is outside range [0;%d)", ivec, d->nvec);

    if (d->is_blocked) {
        if (qlm_import_latvec_qlua(L, &d->vl, d->workM, 3))
            return luaL_error(L, "latvec import: %s", qlm_strerror(qlm_error));
        if (ivec < d->nvec_basis) 
            status = qlm_insert_block_basis_(d, ivec, d->workM);
        else 
            status = qlm_insert_block_extra_(d, ivec, d->workM);
        if (status)
            luaL_error(L, "latvec block_insert(%d): %s", 
                    ivec, qlm_strerror(qlm_error));
    }
    else {
        if (qlm_import_latvec_qlua(L, &d->vl, d->vec_data + ivec * d->vl.vec_size, 3))
            return luaL_error(L, "latvec import: %s", qlm_strerror(qlm_error));
    }
    return 0;   /* no return */
}

/*! [lua] latmat:extract(ivec) ; return a (table of) latfields 
    XXX uses workM (if is_blocked) */
static int 
q_qlm_extract(lua_State *L)
{
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;
    mLattice *S = NULL;
    S = qlua_ObjLattice(L, 1);
    int Sidx    = lua_gettop(L);

    int ivec = luaL_checkint(L, 2);
    if (ivec < 0 || d->nvec <= ivec) 
        luaL_error(L, "index %d is outside range [0;%d)", ivec, d->nvec);

    if (d->is_blocked) {
        if (qlm_latmv_block_(d, qlm_pone(&d->vl), d->blk_coeff + ivec * d->cblkvec_size, 
                    qlm_pzero(&d->vl), d->workM))
            luaL_error(L, "latvec recon_block: %s", qlm_strerror(qlm_error));
        if (qlm_export_latvec_qlua(L, &d->vl, Sidx, d->workM))
            return luaL_error(L, "latvec export: %s", qlm_strerror(qlm_error));
    } else {
        if (qlm_export_latvec_qlua(L, &d->vl, Sidx, d->vec_data + ivec * d->vl.vec_size))
            return luaL_error(L, "latvec export: %s", qlm_strerror(qlm_error));
    }

    return 1;   /* return exported vector */
}
/*! [lua] latmat:extract_block(ivec) ; return a (table of) block-basis latfields
    */
static int 
q_qlm_extract_block(lua_State *L)
{
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;
    mLattice *S = NULL;
    S = qlua_ObjLattice(L, 1);
    int Sidx    = lua_gettop(L);

    int ivec = luaL_checkint(L, 2);
    if (ivec < 0 || d->nvec_basis <= ivec) 
        luaL_error(L, "index %d is outside range [0;%d)", ivec, d->nvec_basis);

    if (! d->is_blocked)
        return 0;   /* no block-basis */
    else {
        if (qlm_export_latvec_qlua(L, &d->vl, Sidx, d->vec_data + ivec * d->vl.vec_size))
            return luaL_error(L, "latvec export_block: %s", qlm_strerror(qlm_error));
    }

    return 1;   /* return exported vector */
}

static void
qlm_keep_range(qlmData *d, int j0, int j1)
{
    void *vdata_ = NULL;
    LONG_T vs_ = 0 ;
    if (d->is_blocked) {
        vdata_  = d->blk_coeff;
        vs_     = d->cblkvec_size;
    } else {
        vdata_  = d->vec_data;
        vs_     = d->vl.vec_size;
    }
    if (j0 < 0) j0 = 0;
    if (d->nvec < j1) j1 = d->nvec;
    if (j0 < j1) {
        if (0 < j0) {
            for (int j = 0 ; j < j1 - j0 ; j++)
                memcpy(vdata_ + j * vs_, vdata_ + (j0 + j) * vs_, vs_);
        }
        d->last_nvec = j1 - j0;
    } else
        d->last_nvec = 0;    /* drop all */
    memset(vdata_ + d->last_nvec * vs_, 0, (d->nvec - d->last_nvec) * vs_);
}
static int
q_keep_range(lua_State *L)
{
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;

    int j0 = qlua_check_intopt(L, 2, 0);
    int j1 = qlua_check_intopt(L, 3, d->nvec);
    if (   j0 < 0 || d->nvec <= j0
        || j1 < 0 || d->nvec <= j1 
        || j1 <= j0)
        luaL_error(L, "bad range=[%d;%d)", j0, j1);

    if (j0 < j1)
        qlm_keep_range(d, j0, j1);

    return 0;
}

static void
qlm_drop_range(qlmData *d, int j0, int j1)
{
    void *vdata_ = NULL;
    LONG_T vs_ = 0 ;
    if (d->is_blocked) {
        vdata_  = d->blk_coeff;
        vs_     = d->cblkvec_size;
    } else {
        vdata_  = d->vec_data;
        vs_     = d->vl.vec_size;
    }
    if (j0 < 0) j0 = 0;
    if (d->nvec < j1) j1 = d->nvec;
    if (j0 < j1) {
        for (int j = 0 ; j < d->nvec - j1; j++)
            memcpy(vdata_ + (j0 + j) * vs_, vdata_ + (j1 + j) * vs_, vs_);
        d->last_nvec = d->nvec - (j1 - j0);    /* FIXME correct only if dropping from full basis; 
                                                        semantics of last_nvec?? */
    }
    memset(vdata_ + d->last_nvec * vs_, 0, (d->nvec - d->last_nvec) * vs_);
}
static int
q_drop_range(lua_State *L)
{
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;

    int j0 = qlua_check_intopt(L, 2, 0);
    int j1 = qlua_check_intopt(L, 3, d->nvec);
    if (   j0 < 0 || d->nvec <= j0
        || j1 < 0 || d->nvec <= j1)
        luaL_error(L, "bad range=[%d;%d)", j0, j1);

    if (j0 < j1)
        qlm_drop_range(d, j0, j1);

    return 0;
}

/*! QR with globally reduced dotproducts 
    XXX modifies vec_data (if !is_blocked), blk_coeff (if is_blocked)
    XXX uses workN
 */
static int 
q_qr_inplace(lua_State *L)
{
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;
    qlmVecLayout *vl = &d->vl;

    void *vdata_= NULL,
         *wk_   = d->workN;     /* XXX workspace */
    LONG_T lenM, vs_,
           ns_  = vl->num_size,
           lenN = d->nvec;
    if (d->is_blocked) {
        vdata_  = d->blk_coeff;
        lenM    = d->cblkvec_num_len;
        vs_     = d->cblkvec_size;
    } else {
        vdata_  = d->vec_data;
        lenM    = vl->vec_num_len;
        vs_     = vl->vec_size;
    }

    /* push matrix to stack */
    void *matr_  = NULL;
    if (QLM_REAL == vl->domain) {
        mMatReal *q_m = qlua_newMatReal(L, lenN, lenN);
        if (NULL == q_m) return luaL_error(L, "not enough memory");
        gsl_matrix_set_zero(q_m->m);
        matr_   = (void *)q_m->m;
    } else if (QLM_CPLX == vl->domain) {
        mMatComplex *q_m = qlua_newMatComplex(L, lenN, lenN);
        if (NULL == q_m) return luaL_error(L, "not enough memory");
        gsl_matrix_complex_set_zero(q_m->m);
        matr_   = (void *)q_m->m;
    }
    /* perform QR */
    for (LONG_T k = 0 ; k < lenN ; k++) {
        if (0 < k) {/* Gram-Schmidt with global reduction */
            qlm_gemxv(vl, lenM, k, 
                    qlm_pone(vl),  vdata_, lenM, vdata_ + k * vs_, 1, 
                    qlm_pzero(vl), wk_, 1);
            if (qlm_global_sum_(d, k, wk_)) luaL_error(L, "qlm_global_sum_ error");;
            for (LONG_T k1 = 0 ; k1 < k ; k1++)
                qlm_gslmatrix_set(vl, matr_, k1, k, wk_ + k1 * ns_);
            qlm_gemv(vl, lenM, k,
                    qlm_pmone(vl), vdata_, lenM, wk_, 1,
                    qlm_pone(vl),  vdata_ + k * vs_, 1);
        }
        double nrm = qlm_nrm2(vl, lenM, vdata_ + k * vs_, 1);
        double nrm2 = nrm * nrm;
        QMP_sum_double(&nrm2);
        assert(0 < nrm2);
        nrm = sqrt(nrm2);
        qlm_gslmatrix_rset(vl, matr_, k, k, nrm);
        qlm_rscal(vl, lenM, 1. / nrm, vdata_ + k * vs_, 1);
    }

    return 1;
}

/*! multiplication by R matrix from right (ie from QR decomp)
    elements below/left of diagnoal are ignored 
    XXX modifies vec_data (if !is_blocked), blk_coeff (if is_blocked)
    XXX uses workN
 */
static int
q_dotr_inplace(lua_State *L)
{
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;
    qlmVecLayout *vl = &d->vl;
    
    void *vdata_= NULL,
         *wk_   = d->workN; /* XXX workspace */
    LONG_T lenM, vs_,
           ns_  = vl->num_size,
           lenN = d->nvec;
    if (d->is_blocked) {
        vdata_  = d->blk_coeff;
        lenM    = d->cblkvec_num_len;
        vs_     = d->cblkvec_size;
    } else {
        vdata_  = d->vec_data;
        lenM    = vl->vec_num_len;
        vs_     = vl->vec_size;
    }

    /* get matrix from stack */
    void *matr_  = NULL;
    if (QLM_REAL == vl->domain) {
        mMatReal *q_m = qlua_checkMatReal(L, 2);
        if (NULL == q_m) return luaL_error(L, "expect matrix.real");
        if (q_m->l_size != lenN || q_m->r_size != lenN) 
            return luaL_error(L, "matrix size mismatch");
        matr_   = (void *)q_m->m;
    } else if (QLM_CPLX == vl->domain) {
        mMatComplex *q_m = qlua_checkMatComplex(L, 2);
        if (NULL == q_m) return luaL_error(L, "expect matrix.complex");
        if (q_m->l_size != lenN || q_m->r_size != lenN) 
            return luaL_error(L, "matrix size mismatch");
        matr_   = (void *)q_m->m;
    }
    /* perform *R */
    for (LONG_T k = lenN ; k-- ; ) {
        for (LONG_T k1 = 0 ; k1 <= k ; k1++)
            qlm_gslmatrix_get(vl, wk_ + k1 * ns_, matr_, k1, k);
        /* k==0 is special because BLAS gemv does nothing if ncol=0 */
        if (0 == k) 
            qlm_scal(vl, lenM, wk_ + k * ns_, vdata_ + k * vs_, 1);
        else
            qlm_gemv(vl, lenM, k,
                    qlm_pone(vl), vdata_, lenM, wk_, 1,
                    wk_ + k * ns_, vdata_ +  k * vs_, 1);
    }

    return 0;
}

/*! qlm:vec_proj(latv)->a 
    
   XXX uses workM, workN */
static int
q_vec_proj(lua_State *L)
{   /* no malloc/free */
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;
    qlmVecLayout *vl = &d->vl;
    int lenN = d->nvec;
    
    if (qlm_import_latvec_qlua(L, vl, d->workM, 2))
        return luaL_error(L, "latvec import: %s", qlm_strerror(qlm_error));
    if (qlm_latmxv_(d, qlm_pone(vl), d->workM, qlm_pzero(vl), d->workN))
        luaL_error(L, "latvec projection: %s", qlm_strerror(qlm_error));
    if (qlm_export_vector_qlua(L, vl, lenN, d->workN))
        luaL_error(L, "vector export: %s", qlm_strerror(qlm_error));
    return 1;
}

/*! qlm:recon_vec(a)->latv 
    XXX uses workM, workN */
static int
q_recon_vec(lua_State *L)
{   /* no malloc/free */
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;
    qlmVecLayout *vl = &d->vl;
    int lenN = d->nvec;
    mLattice *S = NULL;
    S = qlua_ObjLattice(L, 1);
    int Sidx    = lua_gettop(L);

    if (qlm_import_vector_qlua(L, vl, lenN, d->workN, 2))
        luaL_error(L, "vector import: %s", qlm_strerror(qlm_error));
    if (qlm_latmv_(d, qlm_pone(vl), d->workN, qlm_pzero(vl), d->workM))
        luaL_error(L, "latvec reconstruction: %s", qlm_strerror(qlm_error));
    if (qlm_export_latvec_qlua(L, vl, Sidx, d->workM))
        return luaL_error(L, "latvec export: %s", qlm_strerror(qlm_error));
    return 1;
}

/*! project onto vecspace in latmat
    dst and src may overlap
 */
int 
qlm_latvec_proj(qlmData *d, void *restrict v_, void *restrict c_v_)
{   
    assert(NULL != d);
    assert(NULL != v_);
    qlmVecLayout *vl = &d->vl;
    int status = 0;
    /* calc projection */
    if (0 != (status = qlm_latmxv_(d, qlm_pone(vl), v_, qlm_pzero(vl), c_v_)))
        return status;
    /* reconstruct latvec */   
    if (0 != (status = qlm_latmv_(d, qlm_pone(vl), c_v_, qlm_pzero(vl), v_)))
        return status;
    return status;
}
/*! orthogonalize wrt vecspace in latmat
    dst and src may overlap
 */
int 
qlm_latvec_ortho(qlmData *d, void *restrict v_, void *restrict c_v_)
{
    assert(NULL != d);
    assert(NULL != v_);
    qlmVecLayout *vl = &d->vl;
    int status = 0;
    /* calc projection */
    if (0 != (status = qlm_latmxv_(d, qlm_pone(vl), v_, qlm_pzero(vl), c_v_)))
        return status;
    /* reconstruct latvec */   
    if (0 != (status = qlm_latmv_(d, qlm_pmone(vl), c_v_, qlm_pone(vl), v_)))
        return status;
    return status;
}

/*! qlm:proj(latv)->latv 
    XXX uses workM, workN */
static int
q_proj(lua_State *L)
{   /* no malloc/free */
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;
    qlmVecLayout *vl = &d->vl;
    mLattice *S = NULL;
    S = qlua_ObjLattice(L, 1);
    int Sidx    = lua_gettop(L);
    
    if (qlm_import_latvec_qlua(L, vl, d->workM, 2))
        return luaL_error(L, "latvec import: %s", qlm_strerror(qlm_error));
    if (qlm_latvec_proj(d, d->workM, d->workN))
        luaL_error(L, "vector projection: %s", qlm_strerror(qlm_error));
    if (qlm_export_latvec_qlua(L, vl, Sidx, d->workM))
        return luaL_error(L, "latvec export: %s", qlm_strerror(qlm_error));
    return 1;
}
/*! qlm:ortho(latv)->latv 
    XXX uses workM, workN */
static int
q_ortho(lua_State *L)
{   /* no malloc/free */
    mQlm *c     = qlua_checkQlm(L, 1, NULL);
    qlmData *d  = c->d;
    qlmVecLayout *vl = &d->vl;
    mLattice *S = NULL;
    S = qlua_ObjLattice(L, 1);
    int Sidx    = lua_gettop(L);
    
    if (qlm_import_latvec_qlua(L, vl, d->workM, 2))
        return luaL_error(L, "latvec import: %s", qlm_strerror(qlm_error));
    if (qlm_latvec_ortho(d, d->workM, d->workN))
        luaL_error(L, "vector projection: %s", qlm_strerror(qlm_error));
    if (qlm_export_latvec_qlua(L, vl, Sidx, d->workM))
        return luaL_error(L, "latvec export: %s", qlm_strerror(qlm_error));
    return 1;
}


static struct luaL_Reg mtQlm[] = {
    { "__tostring",                 q_qlm_fmt                       },
    { "__gc",                       q_qlm_gc                        },
//    { "__index",                    q_qlm_index     },
    { "extract",                    q_qlm_extract               },
    { "extract_block",              q_qlm_extract_block         },
    { "insert",                     q_qlm_insert                },
// TODO   { "insert_ortho",                     q_qlm_insert_ortho                },
    { "qr_inplace_",                q_qr_inplace                },
    { "dotr_inplace_",              q_dotr_inplace              },
    
    { "drop_range",                 q_drop_range                },
    { "keep_range",                 q_keep_range                },

    { "vec_proj",                   q_vec_proj                  },
    { "recon_vec",                  q_recon_vec                 },
    { "ortho",                      q_ortho                     },
    { "proj",                       q_proj                      },

//    { "copy",                       q_qlm_copy                      },
//    { "read_compressed",            q_qlm_read_compressed           },
    { NULL,                         NULL                            }
};

mQlm *
qlua_checkQlm(lua_State *L, int idx, mLattice *S) {
    mQlm *c = qlua_checkLatticeType(L, idx, qLatmat, qlm_meta_name);
    if (NULL != S) {
        mLattice *S1 = qlua_ObjLattice(L, idx);
        if (S1->id != S->id)
            luaL_error(L, "%s on a wrong lattice", qlm_meta_name);
        lua_pop(L, 1);
    }
        
    if (NULL == c->d)
        luaL_error(L, "NULL qlm object");
    
    return c;
}

/* lattice object is stored in the userdata's env */
mQlm *
qlua_newQlm(lua_State *L, int Sidx, qlmData *d)
{
    mQlm *c = lua_newuserdata(L, sizeof(mQlm));
    c->d    = d;
    qlua_createLatticeTable(L, Sidx, mtQlm, qLatmat, qlm_meta_name);
    if ( !lua_setmetatable(L, -2) )
        luaL_error(L, "cannot set meta");

    /* initialize environment */
    lua_newtable(L);
    lua_pushvalue(L, Sidx);
    lua_setfield(L, -2, "lattice");
    /* more values to store in env */
    if (! lua_setfenv(L, -2))
        luaL_error(L, "cannot set env");

    /* TODO */
    return c;
}
/* qcd.latmat.create(lat, type, nvec, 
        {array=<arr_len>, ns=<ns>, nc=<nc>, 
        prec[ision]="single"|"double", 
        sublat="even"|"odd"|"full"},
        block={bx,by,bz,bt, [array=<array_blk>], [int=<int_blk_type>]},
        nvec_basis=<nvec_basis>})
 */
#define QLM_NC_DEFAULT  3
#define QLM_NS_DEFAULT  4
static int
q_qlm_create(lua_State *L)
{
    if (lua_gettop(L) < 3)
        return qlua_badconstr(L, qlm_str);
    mLattice *S = qlua_checkLattice(L, 1);
    int nvec = 0; 
    qlmLatField lftype;
    const char *lftype_str = luaL_checkstring(L, 2);
    if (QLM_LAT_NONE == (lftype = qlm_str2lftype(lftype_str)))
        luaL_error(L, "bad lftype='%s'", lftype_str);
   
    if ((nvec = luaL_checkinteger(L, 3))<= 0)
        luaL_error(L, "bad nvec=%d", nvec);

    int is_array = 0, 
        arr_len = 1;
    int is_blocked = 0,
        nvec_basis = nvec,
        arr_block = -1,
        int_nb  = 1;
    const char *int_blk_type = NULL;
    int *bs_dim = NULL;
    int ns = QLM_NS_DEFAULT, 
        nc = QLM_NC_DEFAULT;
    qlmPrec prec = QLM_PREC_SINGLE;
    qlmSublat sublat = QLM_SUBLAT_FULL;

    const int oidx = 4;       /* tabopt index */
    if (qlua_checkopt_paramtable(L, oidx)) {
        const char *opt_str;
        opt_str = qlua_tabkey_stringopt(L, oidx, "sublat", "full");
        if (QLM_SUBLAT_NONE == (sublat = qlm_str2sublat(opt_str)))
            luaL_error(L, "bad sublat='%s'", opt_str);

        opt_str = qlua_tabkey_stringopt(L, oidx, "prec", "double");
        if (QLM_PREC_NONE == (prec = qlm_str2prec(opt_str)))
            luaL_error(L, "bad prec='%s'", opt_str);

        is_array = qlua_tabkey_present(L, oidx, "array");
        if (is_array) {
            if ((arr_len = qlua_tabkey_intopt(L, oidx, "array", -1)) <= 0)
                luaL_error(L, "bad arr_len=%d", arr_len);
        } else arr_len = 1;

        is_blocked = qlua_tabkey_present(L, oidx, "block");
        if (is_blocked) {
            qlua_tabkey(L, oidx, "block");
            bs_dim = qlua_checkintarray(L, -1, S->rank, NULL);
            lua_pop(L, 1);
            if (NULL == bs_dim) 
                luaL_error(L, "bad blocksize");
            for (int i = 0 ; i < S->rank ; i++) 
                if (bs_dim[i] <= 0)
                    luaL_error(L, "bad blocksize[%d]=%d\n", i, bs_dim[i]);

            nvec_basis = qlua_tabkey_int(L, oidx, "nvec_basis");
            if (nvec_basis <= 0 || nvec < nvec_basis)
                luaL_error(L, "bad nvec_basis=%d", nvec_basis);
            /* array blocking */
            if (is_array) {
                arr_block = qlua_tabkey_intopt(L, oidx, "arr_block", -1);
                if (-1 == arr_block) arr_block = arr_len;   /* default */
                if (0 != arr_len % arr_block)
                    return luaL_error(L, "bad arr_block=%d", arr_block);
            }
            /* internal dim blocking */
            if (NULL != (int_blk_type = qlua_tabkey_stringopt(
                            L, oidx, "int", NULL))) {
                return luaL_error(L, 
                        "internal dim blocking '%s' not implemented", 
                        int_blk_type);
            }
        }

        if ((ns = qlua_tabkey_intopt(L, oidx, "ns", QLM_NS_DEFAULT)) <= 0)
            luaL_error(L, "bad Ns=%d", ns);
        if ((nc = qlua_tabkey_intopt(L, oidx, "nc", QLM_NC_DEFAULT)) <= 0)
            luaL_error(L, "bad Nc=%d", nc);
    }
    qlmData *d  = qlm_data_alloc(S, sublat, nvec, lftype, 
            is_array, arr_len, ns, nc, prec, 
            is_blocked, bs_dim, arr_block, int_nb, nvec_basis);
    if (NULL == d) 
        luaL_error(L, "qlm_data_alloc: return NULL: %s", qlm_strerror(qlm_error));

    mQlm *c = NULL;
    c  = qlua_newQlm(L, 1, d);

    free_not_null(bs_dim);
    return 1;
}

/* qlm_sandbox(what_str, ...) */
int q_qlm_sandbox(lua_State *L)
{
    const char *what_str = luaL_checkstring(L, 1);
    if (!strcmp("replace_func_env", what_str)) {
        if (lua_gettop(L) < 3)
            return luaL_error(L, "%s: expect 3 args", what_str);
        lua_pushvalue(L, 2);
        lua_gettable(L, LUA_ENVIRONINDEX);
        lua_pushvalue(L, 2);
        lua_pushvalue(L, 3);
        lua_settable(L, LUA_ENVIRONINDEX);
        return 1;
    } else if (!strcmp("print_geom", what_str)) {
        int lo[QLUA_MAX_LATTICE_RANK],
            hi[QLUA_MAX_LATTICE_RANK],
            nD[QLUA_MAX_LATTICE_RANK];

        mLattice *S = qlua_checkLattice(L, 2);
        node2coord(nD, QDP_this_node, S);
        qlua_sublattice(lo, hi, QDP_this_node, S);
        
        char buf[1024], fmt_nD[1024], fmt_lo[128], fmt_hi[128];
        snprintf_arr(fmt_nD, sizeof(fmt_nD), "%2d", ",", int, nD, S->rank);
        snprintf_arr(fmt_lo, sizeof(fmt_lo), "%2d", ",", int, lo, S->rank);
        snprintf_arr(fmt_hi, sizeof(fmt_hi), "%2d", ",", int, hi, S->rank);
        snprintf(buf, sizeof(buf), "[%4d] {%s} : {%s}..{%s}", 
                 QDP_this_node, fmt_lo, fmt_hi, fmt_nD);
        printf("%s\n", buf);
        return 0;
    } else {
        return luaL_error(L, "unknown what_str='%s'", what_str);
    }
    /* should not get here */
    return 0;
}

static int
do_thrash_cache(long long len)
{
    char *buf = NULL;
    if (len <= 0) 
        return 0 ;
    if (NULL == (buf = malloc(len)))
        return 1;
    /* write ... */
    memset((void *)buf, 0, len);
    /* ... and read */
    long long s = 0;
    volatile char *c = buf ; 
    for (long long i = len; i-- ; )
        s += *c++;
    free(buf);
    return 0;
}
static void 
la_init_linear(void *restrict x, long long xlen, char pd, 
        double x0_re, double x1_re, double x0_im, double x1_im)
{
    if (xlen <= 0) return;
    double dx_re = (x1_re - x0_re) / xlen,
           dx_im = (x1_im - x0_im) / xlen;
    double xc_re = x0_re,
           xc_im = x0_im;
    if (pd == 's') {
        float *xc = (float *)x;
        for (long long i = xlen ; i-- ; xc_re += dx_re)
            *xc++ = xc_re;
    } else if (pd == 'd') {
        double *xc = (double *)x;
        for (long long i = xlen ; i-- ; xc_re += dx_re)
            *xc++ = xc_re;
    } else if (pd == 'c') {
        float complex *xc = (float complex *)x;
        for (long long i = xlen ; i-- ; xc_re += dx_re, xc_im += dx_im)
            *xc++ = xc_re + I * xc_im;
    } else if (pd == 'z') {
        double complex *xc = (double complex *)x;
        for (long long i = xlen ; i-- ; xc_re += dx_re, xc_im += dx_im)
            *xc++ = xc_re + I * xc_im;
    } else assert(NULL == "impossible");
}


/* latmat.la_benchmark(
        "gemm"|"gemv", 
        "s"|"d"|"c"|"z", 
        m, n, [k],
        { thrash_cache_kb=0       -- kbytes of cache to read/write
        })

    no transpose/conj
 */
static int 
q_la_benchmark(lua_State *L)
{
    static char status_st_[256];
    const char *status = NULL;
#define RETURN_STATUS(...) do { \
    snprintf(status_st_, sizeof(status_st_), __VA_ARGS__); \
    status = status_st_; \
    goto clearerr_1; \
} while(0)

    const char *what = luaL_checkstring(L, 1);
    const char *prec_domain = luaL_checkstring(L, 2);
    int m, n, k = 1;
    int oidx;
    m = luaL_checkinteger(L, 3);
    n = luaL_checkinteger(L, 4);
    void *a = NULL,
         *b = NULL,
         *c = NULL;
    long long alen, blen, clen;
    if (!strcmp("gemm", what)) {
        oidx = 6;
        k    = luaL_checkinteger(L, 5);
        alen = m * k;
        blen = n * k;
        clen = m * n;
    } else if (!strcmp("gemv", what)) {
        oidx = 5;
        alen = m * n,
        blen = n,
        clen = m;
    } else { RETURN_STATUS("unknown bm='%s'", what); }

    int thrash_cache_kb = 0;
    if (qlua_checkopt_paramtable(L, oidx)) {
        thrash_cache_kb = qlua_tabkey_intopt(L, oidx, "thrash_cache_kb", 0);
    }

    int rsize, nsize;
    if (!strcmp("s", prec_domain)) {
        rsize = nsize = sizeof(float);
    } else if (!strcmp("d", prec_domain)) {
        rsize = nsize = sizeof(double);
    } else if (!strcmp("c", prec_domain)) {
        rsize = sizeof(float);
        nsize = 2 * sizeof(float);
    } else if (!strcmp("z", prec_domain)) {
        rsize = sizeof(double);
        nsize = 2 * sizeof(double);
    } else { RETURN_STATUS("unknown pd='%s'", prec_domain); }

    a = malloc(alen * nsize);
    b = malloc(blen * nsize);
    c = malloc(clen * nsize);
    if (NULL == a || NULL == b || NULL == c) { 
        RETURN_STATUS("not enough memory");
    }
    TIMING_DECLSTART(cur_t);
#if 1
    la_init_linear(a, alen, prec_domain[0], 1.234, 2.345, 3.456, 4.567);
    la_init_linear(b, blen, prec_domain[0], 1.234, 2.345, 3.456, 4.567);
    la_init_linear(c, clen, prec_domain[0], 1.234, 2.345, 3.456, 4.567); 
#else
    memset(a, 0, alen * nsize);
    memset(b, 0, blen * nsize);
    memset(c, 0, clen * nsize);
#endif
    if (0 < thrash_cache_kb)
        do_thrash_cache(1024 * thrash_cache_kb);

    if (0 == QDP_this_node) 
        printf("latmat.la_benchmark setup = %.3f\n", TIMING_STOP(cur_t));

    TIMING_DECLSTART(bm_t);
    float c1[2] = {1.234, 2.345};
    float c2[2] = {3.456, 4.567};
    double z1[2] = {1.234, 2.345};
    double z2[2] = {3.456, 4.567};
    if (!strcmp("gemm", what)) {    /* c = alpha * a.b + beta * c */
        if (!strcmp("s", prec_domain)) 
            cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 
                    c1[0], a, m, b, k, c2[0], c, m);
        else if (!strcmp("d", prec_domain))
            cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 
                    z1[0], a, m, b, k, z2[0], c, m);
        else if (!strcmp("c", prec_domain)) 
            cblas_cgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 
                    c1, a, m, b, k, c2, c, m);
        else if (!strcmp("z", prec_domain))
            cblas_zgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 
                    z1, a, m, b, k, z2, c, m);
        else assert(NULL == "impossible");  /* already checked */
    } else if (!strcmp("gemv", what)) { /* c = alpha * a.b + beta * c */
        if (!strcmp("s", prec_domain)) 
            cblas_sgemv(CblasColMajor, CblasNoTrans, m, n, 
                    c1[0], a, m, b, 1, c2[0], c, 1);
        else if (!strcmp("d", prec_domain))
            cblas_dgemv(CblasColMajor, CblasNoTrans, m, n,
                    z1[0], a, m, b, 1, z2[0], c, 1);
        else if (!strcmp("c", prec_domain))
            cblas_cgemv(CblasColMajor, CblasNoTrans, m, n,
                    c1, a, m, b, 1, c2, c, 1);
        else if (!strcmp("z", prec_domain))
            cblas_zgemv(CblasColMajor, CblasNoTrans, m, n,
                    z1, a, m, b, 1, z2, c, 1);
        else assert(NULL == "impossible");  /* already checked */
    } else assert(NULL == "impossible");    /* already checked */

    if (0 == QDP_this_node) 
        printf("latmat.la_benchmark run = %.3f\n", TIMING_CHECK(bm_t));

clearerr_1:
    free_not_null(a);
    free_not_null(b);
    free_not_null(c);
    if (NULL != status) 
        return luaL_error(L, status);
    return 0;
}

static int
qlm_selftest()
{
//    return 1;   /* not implemented */
    return 0;
}

static struct luaL_Reg fQlm[] = {
    { "create",                 q_qlm_create     },
    { "read_df_blk_clehner",    q_qlm_evecdwfcl_read_compressed     },
    { "la_benchmark",           q_la_benchmark     },
    { "qlm_sandbox",            q_qlm_sandbox    },
    { NULL,            NULL             }
};

int init_qlm(lua_State *L)
{
    int status;
    if (0 != (status = qlm_selftest()))
        return status;     /* internal checks for layouts, etc */

    lua_getglobal(L, qcdlib);
    lua_newtable(L);
    luaL_register(L, NULL, fQlm);
    lua_setfield(L, -2, qlm_name);
    lua_pop(L, 1);
    return 0;
}

void fini_qlm(void)
{
}
