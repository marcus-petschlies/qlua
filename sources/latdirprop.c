#include "modules.h"                                                 /* DEPS */
#include "qlua.h"                                                    /* DEPS */
#include "qcomplex.h"                                                /* DEPS */
#include "lattice.h"                                                 /* DEPS */
#include "latrandom.h"                                               /* DEPS */
#include "latreal.h"                                                 /* DEPS */
#include "latcomplex.h"                                              /* DEPS */
#include "latcolmat.h"                                               /* DEPS */
#include "latdirferm.h"                                              /* DEPS */
#include "latdirprop.h"                                              /* DEPS */
#include "latcolvec.h"                                               /* DEPS */
#include "seqdirprop.h"                                              /* DEPS */
#include "latint.h"                                                  /* DEPS */
#include "latmulti.h"                                                /* DEPS */
#include "qmp.h"
#include "qla.h"

#if USE_Nc2
#define QNc  '2'
#define Qcolors "2"
#define Qs(a)   a ## 2
#define Qx(a,b)  a ## 2 ## b
#define QC(x)    2
#define QNC(x)
#include "latdirprop-x.c"                                           /* DEPS */
#endif

#if USE_Nc3
#define QNc  '3'
#define Qcolors "3"
#define Qs(a)   a ## 3
#define Qx(a,b)  a ## 3 ## b
#define QC(x)    3
#define QNC(x)
#include "latdirprop-x.c"                                           /* DEPS */
#endif

#if USE_NcN
#define QNc  'N'
#define Qcolors "N"
#define Qs(a)   a ## N
#define Qx(a,b)  a ## N ## b
#define QC(x)    (x)->nc
#define QNC(x)   (x),
#include "latdirprop-x.c"                                           /* DEPS */
#endif

static int
do_gaussian(lua_State *L, mLatRandom *a, mLattice *S, int nc)
{
    switch (nc) {
#if USE_Nc2
    case 2: {
        mLatDirProp2 *r = qlua_newLatDirProp2(L, lua_gettop(L), 2);
        CALL_QDP(L);
        QDP_D2_P_eq_gaussian_S(r->ptr, a->ptr, *S->qss);
        return 1;
    }
#endif
#if USE_Nc3
    case 3: {
        mLatDirProp3 *r = qlua_newLatDirProp3(L, lua_gettop(L), 3);
        CALL_QDP(L);
        QDP_D3_P_eq_gaussian_S(r->ptr, a->ptr, *S->qss);
        return 1;
    }
#endif
#if USE_NcN
    default: {
        mLatDirPropN *r = qlua_newLatDirPropN(L, lua_gettop(L), nc);
        CALL_QDP(L);
        QDP_DN_P_eq_gaussian_S(r->ptr, a->ptr, *S->qss);
        return 1;
    }
#else
    default:
        return luaL_error(L, "bad number of colors");
#endif
    }
}

/* Random vectors of default colors
 * S:gaussian_DiracPropagator()
 */
int
q_P_gaussian(lua_State *L)
{
    mLatRandom *a = qlua_checkLatRandom(L, 1, NULL);
    mLattice *S = qlua_ObjLattice(L, 1);

    return do_gaussian(L, a, S, S->nc);
}

/* Random vectors of NC colors
 * S:gaussian_DiracPropagatorN(nc)
 */
int
q_P_gaussian_N(lua_State *L)
{
    mLatRandom *a = qlua_checkLatRandom(L, 1, NULL);
    mLattice *S = qlua_ObjLattice(L, 1);
    int nc = luaL_checkint(L, 2);

    if (nc < 1)
        return luaL_error(L, "bad number of colors");

    return do_gaussian(L, a, S, nc);
}

/* Lattice Dirac Propagators:
 *  L:DiracPropagator()             -- zero propagator in default colors
 *  L:DiracPropagator(p)            -- constant propagator fill
 *  L:DiracPropagator(P)            -- lattice propagator copy
 *  L:DiracPropagator(M)            -- default colors, P[.,k,.,k] = M for all k
 *  L:DiracPropagator(D,{c=i,d=j})  -- default colors, P[i,j,.,.] = D
 */
static int
q_latdirprop(lua_State *L)
{
    mLattice *S = qlua_checkLattice(L, 1);

    if (lua_gettop(L) == 2) {
        switch (qlua_qtype(L, 2)) {
#if USE_Nc2
        case qSeqDirProp2: return q_latdirprop_seq_2(L, S, 2);
        case qLatDirProp2: return q_latdirprop_lat_2(L, S, 2);
        case qLatColMat2:  return q_latdirprop_mat_2(L, S, 2);
#endif
#if USE_Nc3
        case qSeqDirProp3: return q_latdirprop_seq_3(L, S, 3);
        case qLatDirProp3: return q_latdirprop_lat_3(L, S, 3);
        case qLatColMat3:  return q_latdirprop_mat_3(L, S, 3);
#endif
#if USE_NcN
        case qSeqDirPropN: {
            mSeqDirPropN *v = qlua_checkSeqDirPropN(L, 2, -1);
            return q_latdirprop_seq_N(L, S, v->nc);
        }
        case qLatDirPropN: {
            mLatDirPropN *v = qlua_checkLatDirPropN(L, 2, S, -1);
            return q_latdirprop_lat_N(L, S, v->nc);
        }
        case qLatColMatN: {
            mLatColMatN *v = qlua_checkLatColMatN(L, 2, S, -1);
            return q_latdirprop_mat_N(L, S, v->nc);
        }
#endif
        default:
            break;
        }
    }
    switch (S->nc) {
#if USE_Nc2
    case 2: return q_latdirprop_2(L, S, 2, 0);
#endif
#if USE_Nc3
    case 3: return q_latdirprop_3(L, S, 3, 0);
#endif
#if USE_NcN
    default: return q_latdirprop_N(L, S, S->nc, 0);
#else
    default: return luaL_error(L, "bad number of colors");
#endif
    }
}

/* Lattice Dirac Propagators:
 *  L:DiracPropagatorN(n)              -- zero propagator in n colors
 *  L:DiracPropagatorN(n,M)            -- n colors, P[.,k,.,k] = M for all k
 *  L:DiracPropagatorN(n,D,{c=i,d=j})  -- n colors, P[i,j,.,.] = D
 */
static int
q_latdirpropN(lua_State *L)
{
#if USE_Nc2 || USE_Nc3 || USE_NcN
    mLattice *S = qlua_checkLattice(L, 1);
#endif
    int nc = luaL_checkint(L, 2);

    switch (nc) {
#if USE_Nc2
    case 2: return q_latdirprop_2(L, S, 2, 1);
#endif
#if USE_Nc3
    case 3: return q_latdirprop_3(L, S, 3, 1);
#endif
#if USE_NcN
    default: return q_latdirprop_N(L, S, nc, 1);
#else
    default: return luaL_error(L, "bad number of colors");
#endif
    }
}

#if USE_Nc3
typedef struct {
    QLA_D3_DiracPropagator *a;
    QLA_D3_DiracPropagator *b;
} QQ_arg;

/* TODO force to unroll loops over color&spin indices? */
#define do_QQ_contract_func(contract_idx,A,B,C,D)\
    static void do_QQc ## contract_idx(QLA_D3_DiracPropagator *q3, \
                                       int idx,                         \
                                       void *env)                       \
 {                                                                      \
     QQ_arg *arg = env;                                                 \
     QLA_D3_DiracPropagator *q1 = &arg->a[idx];                         \
     QLA_D3_DiracPropagator *q2 = &arg->b[idx];                         \
     static const int eps[3][3] = { { 0, 1, 2}, { 1, 2, 0}, { 2, 0, 1} }; \
     int p_a, p_b;                                                      \
     for (p_a = 0; p_a < 3; p_a++) { /* Nc */                           \
         int i1 = eps[p_a][0], j1 = eps[p_a][1], k1 = eps[p_a][2];      \
         for (p_b = 0; p_b < 3; p_b++) { /* Nc */                       \
             int i2 = eps[p_b][0], j2 = eps[p_b][1], k2 = eps[p_b][2];  \
             int a, b, c;                                               \
             for (a = 0; a < QDP_Ns; a++) {                             \
                 for (b = 0; b < QDP_Ns; b++) {                         \
                     QLA_Complex s3;                                    \
                     QLA_c_eq_r(s3, 0.0);                               \
                     for (c = 0; c < QDP_Ns; c++) {                     \
                         QLA_c_peq_c_times_c(s3,                        \
                                             QLA_elem_P(*q1,i1,(A),i2,(B)), \
                                             QLA_elem_P(*q2,j1,(C),j2,(D))); \
                         QLA_c_meq_c_times_c(s3,                        \
                                             QLA_elem_P(*q1,i1,(A),j2,(B)), \
                                             QLA_elem_P(*q2,j1,(C),i2,(D))); \
                         QLA_c_meq_c_times_c(s3,                        \
                                             QLA_elem_P(*q1,j1,(A),i2,(B)), \
                                             QLA_elem_P(*q2,i1,(C),j2,(D))); \
                         QLA_c_peq_c_times_c(s3,                        \
                                             QLA_elem_P(*q1,j1,(A),j2,(B)), \
                                             QLA_elem_P(*q2,i1,(C),i2,(D))); \
                     }                                                  \
                     QLA_c_eq_c(QLA_elem_P(*q3,k2,a,k1,b), s3);         \
                 }                                                      \
             }                                                          \
         }                                                              \
     }                                                                  \
 }
do_QQ_contract_func(12, c,c,a,b);
do_QQ_contract_func(13, c,a,c,b);
do_QQ_contract_func(14, c,a,b,c);
do_QQ_contract_func(23, a,c,c,b);
do_QQ_contract_func(24, a,c,b,c);
do_QQ_contract_func(34, a,b,c,c);
#undef do_QQ_contract_func

static int
q_su3contract_general(lua_State *L, 
                      void (*do_contract)(QLA_D3_DiracPropagator *,int,void *))
{
    mLatDirProp3 *a = qlua_checkLatDirProp3(L, 1, NULL, 3); /* the 1st arg */
    mLattice *S = qlua_ObjLattice(L, 1);
    mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
    mLatDirProp3 *r = qlua_newLatDirProp3(L, lua_gettop(L), 3); 
    QQ_arg arg;

    CALL_QDP(L);
    arg.a = QDP_D3_expose_P(a->ptr);
    arg.b = QDP_D3_expose_P(b->ptr);
    QDP_D3_P_eq_funcia(r->ptr, do_contract, &arg, *S->qss);
    QDP_D3_reset_P(a->ptr);
    QDP_D3_reset_P(b->ptr);

    return 1;
}
#define q_su3contract_func(contract_idx) \
static int q_su3contract ## contract_idx(lua_State *L) \
{ \
    return q_su3contract_general(L, do_QQc ## contract_idx); \
}
q_su3contract_func(12);
q_su3contract_func(13);
q_su3contract_func(14);
q_su3contract_func(23);
q_su3contract_func(24);
q_su3contract_func(34);
#undef q_su3contract_func

/*************************************************************
 * additional reduction functions 
 *************************************************************/
#if USE_NcN

typedef struct {
    QLA_D3_DiracFermion *a;
    QLA_D3_DiracPropagator *b;
} V1_arg;

typedef struct {
    QLA_D3_DiracFermion *a;
    QLA_D3_DiracFermion *b;
    QLA_D3_DiracPropagator *c;
} Vffp_arg;

typedef struct {
    QLA_DN_ColorVector(9*QDP_Ns, (*a));
    QLA_D3_DiracPropagator *b;
} V2_arg;

typedef struct {
    QLA_DN_ColorVector(9*QDP_Ns*QDP_Ns*QDP_Ns, (*a));
    QLA_D3_DiracPropagator *b;
} V2o_arg;


typedef struct {
    QLA_D3_DiracPropagator *a;
    QLA_D3_DiracPropagator *b;
} V5_arg;

typedef struct {
    QLA_DN_ColorVector(9*QDP_Ns*QDP_Ns*QDP_Ns*QDP_Ns, (*a));
    QLA_D3_DiracPropagator *b;
} V6_arg;

/*************************************************************
 * function V1:
 *   reduction of a DiracFermion (intended: stochastic source or stochastic
 *   propagator) and a DiracPropagator (intended: point-to-all fwd or sequential
 *   propagator) to a color vector with Nc^2xNs components
 *
 * use qdp_dn_profile.h:
 *
 * QDP_DN_V_eq_funciat(QDP_Nc,_func,_args,_subset)
 *
 * void QDP_DN_V_eq_funcia(QDP_DN_ColorVector *dest, void (*func)(int nc, QLA_DN_ColorVector(nc, (*dest)), int index, void *args), void *args, QDP_Subset subset);
 *
 *
 *************************************************************/
static void do_su3contract_v1 (int nc, QLA_DN_ColorVector( nc, (*v1)), int idx, void *env ) {
/*************************************************************
 * use QLA DN_ColorVector
 *************************************************************/

  V1_arg *arg = env;
  QLA_D3_DiracFermion    *f = &arg->a[idx];
  QLA_D3_DiracPropagator *q = &arg->b[idx];
  static const int eps[3][3] = { { 0, 1, 2}, { 1, 2, 0}, { 2, 0, 1} };
  int k1, p_b;

  for (k1 = 0; k1 < 3; k1++) { /* Nc */

    for (p_b = 0; p_b < 3; p_b++) { /* Nc */

      int i2 = eps[p_b][0], j2 = eps[p_b][1], k2 = eps[p_b][2];
      int a, c;

      for (a = 0; a < QDP_Ns; a++) {

        QLA_Complex s3; 
        QLA_c_eq_r(s3, 0.0);

        for (c = 0; c < QDP_Ns; c++) {

          /*************************************************************
           * s3 += epsilon_{k2,i2,j2} f[i2, c] * q[j2, c; k1, a]
           *************************************************************/

          QLA_c_peq_c_times_c(s3, QLA_elem_D(*f,i2,c), QLA_elem_P(*q, j2, c, k1, a) );
          QLA_c_meq_c_times_c(s3, QLA_elem_D(*f,j2,c), QLA_elem_P(*q, i2, c, k1, a) );
        }  /* end of loop on spinor index c */
        /*************************************************************
         * v1[k1,k2,a] = v1[d] <- s3
         *************************************************************/
        int d = QDP_Ns * ( 3 * k2 + k1 ) + a;
        QLA_c_eq_c( QLA_DN_elem_V(*v1, d), s3);
      }  /* end of loop on a */
    }  /* end of loop on permutation index p_b */
  }  /* end of loop on color index k1 */
#if 0
#endif  /* of if 0 */

}  /* end of do_su3contract_v1 */

/*************************************************************
 * function V2:
 * reduction of DN color vector v1 with a DiracPropagator to DN
 * color vector v2
 *************************************************************/
static void do_su3contract_v2 (int nc, QLA_DN_ColorVector( nc, (*v2)), int idx, void *env ) {

  V2_arg *arg = env;
  QLA_DN_ColorVector( QDP_Ns*9, (*v1) );
  QLA_D3_DiracPropagator *q2;
  v1 = &arg->a[idx];
  q2 = &arg->b[idx];
  static const int eps[3][3] = { { 0, 1, 2}, { 1, 2, 0}, { 2, 0, 1} };
  int p_b;

    for (p_b = 0; p_b < 3; p_b++) { /* Nc */

      int i2 = eps[p_b][0], j2 = eps[p_b][1], k2 = eps[p_b][2];
      int a, b, c;

      for (a = 0; a < QDP_Ns; a++) {
      for (b = 0; b < QDP_Ns; b++) {
      for (c = 0; c < QDP_Ns; c++) {

        QLA_Complex s3;
        QLA_c_eq_r(s3, 0.0);

        int k1;
        for (k1 = 0; k1 < 3; k1++) { /* Nc */

          /*************************************************************
           * s3 += epsilon_{k2,i2,j2} v1[k1, i2, a] * q2[ k1, b; j2, c]
           *************************************************************/

          QLA_c_peq_c_times_c(s3, QLA_DN_elem_V(*v1, QDP_Ns*(3*k1+i2)+a), QLA_elem_P(*q2, k1, b, j2, c) );
          QLA_c_meq_c_times_c(s3, QLA_DN_elem_V(*v1, QDP_Ns*(3*k1+j2)+a), QLA_elem_P(*q2, k1, b, i2, c) );
        }  /* end of loop on spinor index c */
        /*************************************************************
         * v2[a,b,c,k2] = s3
         *************************************************************/
        int d = 3 * ( QDP_Ns *( QDP_Ns * a + b ) + c ) + k2;
        QLA_c_eq_c( QLA_DN_elem_V(*v2, d), s3);
      }  /* end of loop on spin index b */
      }  /* end of loop on spin index a */
    }  /* end of loop on permutation index p_b */
  }  /* end of loop on color index k1 */
}  /* end of do_su3contract_v2 */


/*************************************************************
 * function V3:
 *   reduction of a DiracFermion (intended: stochastic source or stochastic
 *   propagator) and a DiracPropagator (intended: point-to-all fwd or sequential
 *   propagator) to a color vector with NcxNs components
 *
 *************************************************************/
static void do_su3contract_v3 (int nc, QLA_DN_ColorVector( nc, (*v3)), int idx, void *env ) {

  V1_arg *arg = env;
  QLA_D3_DiracFermion    *f = &arg->a[idx];
  QLA_D3_DiracPropagator *q = &arg->b[idx];
  int k, a;

  for (k = 0; k < 3; k++) { /* Nc */
    for (a = 0; a < QDP_Ns; a++) { /* Ns */

      int d = QDP_Ns * k + a;
      int l, b;
      QLA_Complex s3; 
      QLA_c_eq_r(s3, 0.0);

      for (l = 0; l < 3; l++) {  /* Nc */
        for (b = 0; b < QDP_Ns; b++) {  /* Ns */

          /*************************************************************
           * s3 += f[ l, b] * q[ l, b; k, a]
           *************************************************************/

          QLA_c_peq_ca_times_c(s3, QLA_elem_D(*f,l,b), QLA_elem_P(*q, l, b, k, a) );
        }  /* end of loop on spinor index b */
      }  /* end of loop on color index l */

      /*************************************************************
       * v3[k,a] = v3[d] <- s3
       *************************************************************/
      QLA_c_eq_c( QLA_DN_elem_V(*v3, d), s3);
    }  /* end of loop on spinor index a */
  }  /* end of loop on color index k */
#if 0
#endif  /* of if 0 */

}  /* end of do_su3contract_v3 */

/*************************************************************/
/*************************************************************/

static void do_su3contract_v4aux ( QLA_D3_DiracPropagator *q3, int idx, void *env ) {

  V5_arg *arg = env;
  QLA_D3_DiracPropagator *q1 = &arg->a[idx];
  QLA_D3_DiracPropagator *q2 = &arg->b[idx];
  static const int eps[3][3] = { { 0, 1, 2}, { 1, 2, 0}, { 2, 0, 1} };
  int p_b, p_a;

  for (p_a = 0; p_a < 3; p_a++) { /* Nc */
    int i1 = eps[p_a][0], j1 = eps[p_a][1], k1 = eps[p_a][2];

    for (p_b = 0; p_b < 3; p_b++) { /* Nc */

      int i2 = eps[p_b][0], j2 = eps[p_b][1], k2 = eps[p_b][2];

      int a, b, c;

      for (a = 0; a < QDP_Ns; a++) {
      for (b = 0; b < QDP_Ns; b++) {

        QLA_Complex s3; 
        QLA_c_eq_r(s3, 0.0);

        for (c = 0; c < QDP_Ns; c++) {

          /*************************************************************
           * s3 += epsilon_{k2,i2,j2} epsilon_{k1,i1,j1} q1[i1,c,i2,a] * q2[j1,c,j2,b]
           *************************************************************/

          QLA_c_peq_c_times_c(s3, QLA_elem_P(*q1,i1,c,i2,a), QLA_elem_P(*q2, j1, c, j2, b) );

          QLA_c_meq_c_times_c(s3, QLA_elem_P(*q1,j1,c,i2,a), QLA_elem_P(*q2, i1, c, j2, b) );

          QLA_c_peq_c_times_c(s3, QLA_elem_P(*q1,j1,c,j2,a), QLA_elem_P(*q2, i1, c, i2, b) );

          QLA_c_meq_c_times_c(s3, QLA_elem_P(*q1,i1,c,j2,a), QLA_elem_P(*q2, j1, c, i2, b) );

        }  /* end of loop on spinor index c */

        /*************************************************************
         * q3[k1,a,k2,b] += s3
         *************************************************************/
        QLA_c_eq_c( QLA_D_elem_P(*q3, k1, a, k2, b), s3);

      }  /* end of loop on spinor index b */
      }  /* end of loop on spinor index a */
    }  /* end of loop on permutation index p_b */
  }  /* end of loop on permutation index p_a */

}  /* end of do_su3contract_v4aux */

/*************************************************************/
/*************************************************************/

/*************************************************************
 * function V4:
 * reduction of DiracPropagator3 from quarkContract13 with a 
 * DiracFermion3 to DN color vector v2
 *************************************************************/
static void do_su3contract_v4 (int nc, QLA_DN_ColorVector( nc, (*v2)), int idx, void *env ) {

  V1_arg *arg = env;
  QLA_D3_DiracFermion *v1;
  QLA_D3_DiracPropagator *q2;
  v1 = &arg->a[idx];
  q2 = &arg->b[idx];

  int a, b, c, l, m;

  for (a = 0; a < QDP_Ns; a++) {  /* spinor index of fermion */
  for (b = 0; b < QDP_Ns; b++) {  /* left spinor index of propagator */
  for (c = 0; c < QDP_Ns; c++) {  /* right spinor index of propagator */

    for ( l = 0; l < 3; l++ ) { 

      QLA_Complex s3;
      QLA_c_eq_r( s3, 0.0);

      for (m = 0; m < 3; m++) { /* Nc */
        /*************************************************************
         * s3 += v1[ m, a ] * q2[ m, b; l, c]
         *************************************************************/
        QLA_c_peq_c_times_c( s3, QLA_elem_D(*v1,m,a), QLA_elem_P(*q2, m, b, l, c) );
      }  /* end of loop on color index m */

      /*************************************************************
       * v4[a,b,c,l] = s3
       *************************************************************/
      int d = 3 * ( QDP_Ns *( QDP_Ns * a + b ) + c ) + l;
      QLA_c_eq_c( QLA_DN_elem_V(*v2, d), s3);
    }  /* end of loop on color index l */
  }  /* end of loop on spinor index c */
  }  /* end of loop on spin index b */
  }  /* end of loop on spin index a */
}  /* end of do_su3contract_v4 */

/*************************************************************/
/*************************************************************/

/*************************************************************
 * function T1:
 * reduction of DiracPropagator3 from quarkContract13 with a 
 * DiracFermion3 to DN color vector v2
 *
 * NOTE: q2 should be the outcome of contract13, i.e.
 *       color indices are switched
 *
 *       q2[k,c,l,b] = U^{rs}_{c,d} U^{uv}_{c,b} esp_{rul} eps_{svk}
 *
 *       so we do q1[k,a,l,c] * q2[k,c,l,b]
 *
 *************************************************************/
static void do_su3contract_t1 (int nc, QLA_DN_ColorVector( nc, (*v)), int idx, void *env ) {

  V5_arg *arg = env;
  QLA_D3_DiracPropagator *q1;
  QLA_D3_DiracPropagator *q2;
  q1 = &arg->a[idx];
  q2 = &arg->b[idx];

  int a, b, c, l, k;

  for (a = 0; a < QDP_Ns; a++) {  /* left-most spinor index */
  for (b = 0; b < QDP_Ns; b++) {  /* right-most spinor index */

    QLA_Complex s3;
    QLA_c_eq_r( s3, 0.0);

    for (c = 0; c < QDP_Ns; c++) {  /* spinor index for summation */

      for ( k = 0; k < 3; k++ ) { /* left color index */
      for ( l = 0; l < 3; l++ ) { /* right color index */

        /*************************************************************
         * s3 += q1[ k, a; l, c ] * q2[ k, c; l, b ]
         *************************************************************/
        QLA_c_peq_c_times_c( s3, QLA_elem_P(*q1, k, a, l, c), QLA_elem_P(*q2, k, c, l, b) );
      }  /* end of loop on color index l */
      }  /* end of loop on color index k */

    }  /* end of loop on spinor index c */

    /*************************************************************
     * v[a,b] = s3
     *************************************************************/
    int d = QDP_Ns * a + b;
    QLA_c_eq_c( QLA_DN_elem_V(*v, d), s3);

  }  /* end of loop on spin index b */
  }  /* end of loop on spin index a */

}  /* end of do_su3contract_t1 */

/*************************************************************/
/*************************************************************/

/*************************************************************
 * function T2:
 * reduction of DiracPropagator3 from quarkContract13 with a 
 * DiracFermion3 to DN color vector v2
 *
 * NOTE: q2 should be the outcome of contract13, i.e.
 *       color indices are switched
 *
 *       q2[k,c,l,b] = U^{rs}_{c,d} U^{uv}_{c,b} esp_{rul} eps_{svk}
 *
 *       so we do q1[k,a,l,c] * q2[k,c,l,b]
 *
 *************************************************************/
static void do_su3contract_t2 (int nc, QLA_DN_ColorVector( nc, (*v)), int idx, void *env ) {

  V5_arg *arg = env;
  QLA_D3_DiracPropagator *q1;
  QLA_D3_DiracPropagator *q2;
  q1 = &arg->a[idx];
  q2 = &arg->b[idx];

  int a, b, c, l, k;

  for (a = 0; a < QDP_Ns; a++) {  /* left-most spinor index */
  for (b = 0; b < QDP_Ns; b++) {  /* right-most spinor index */

    QLA_Complex s3;
    QLA_c_eq_r( s3, 0.0);

    for (c = 0; c < QDP_Ns; c++) {  /* spinor index for summation */

      for ( k = 0; k < 3; k++ ) { /* left color index */
      for ( l = 0; l < 3; l++ ) { /* right color index */

        /*************************************************************
         * s3 += q1[ k, a; l, c ] * q2[ k, c; l, b ]
         *************************************************************/
        QLA_c_peq_c_times_c( s3, QLA_elem_P(*q1, k, a, l, b), QLA_elem_P(*q2, k, c, l, c) );
      }  /* end of loop on color index l */
      }  /* end of loop on color index k */

    }  /* end of loop on spinor index c */

    /*************************************************************
     * v[a,b] = s3
     *************************************************************/
    int d = QDP_Ns * a + b;
    QLA_c_eq_c( QLA_DN_elem_V(*v, d), s3);

  }  /* end of loop on spin index b */
  }  /* end of loop on spin index a */

}  /* end of do_su3contract_t1 */

/*************************************************************/
/*************************************************************/

/*************************************************************
 * color trace or propagator
 *************************************************************/
static void do_su3colortrace (int nc, QLA_DN_ColorVector( nc, (*v)), int idx, void *env ) {

  QLA_D3_DiracPropagator *q1 = &( ((QLA_D3_DiracPropagator*)env)[idx] );

  int a, b, k;

  for (a = 0; a < QDP_Ns; a++) {  /* left-most spinor index */
  for (b = 0; b < QDP_Ns; b++) {  /* right-most spinor index */

    QLA_Complex s3;
    QLA_c_eq_r( s3, 0.0);

    for ( k = 0; k < 3; k++ ) { /* left color index */

      /*************************************************************
       * s3 += q1[ k, a; k, b ]
       *************************************************************/
        QLA_c_peq_c ( s3, QLA_elem_P(*q1, k, a, k, b));
    }  /* end of loop on color index k */

    /*************************************************************
     * v[a,b] = s3
     *************************************************************/
    int d = QDP_Ns * a + b;
    QLA_c_eq_c( QLA_DN_elem_V(*v, d), s3);

  }  /* end of loop on spin index b */
  }  /* end of loop on spin index a */

}  /* end of do_su3colortrace */

/*************************************************************/
/*************************************************************/


/*************************************************************
 * function V1:
 *   reduction of a DiracFermion (intended: stochastic source or stochastic
 *   propagator) and a DiracPropagator (intended: point-to-all fwd or sequential
 *   propagator) to a color vector with Nc^2xNs components
 *
 *************************************************************/
static void do_su3contract_v1_openspin (int nc, QLA_DN_ColorVector( nc, (*v1)), int idx, void *env ) {
/*************************************************************
 * use QLA DN_ColorVector
 *************************************************************/

  V1_arg *arg = env;
  QLA_D3_DiracFermion    *f = &arg->a[idx];
  QLA_D3_DiracPropagator *q = &arg->b[idx];
  static const int eps[3][3] = { { 0, 1, 2}, { 1, 2, 0}, { 2, 0, 1} };
  int k1, p_b;

  for (k1 = 0; k1 < 3; k1++) { /* Nc */

    for (p_b = 0; p_b < 3; p_b++) { /* Nc */

      int i2 = eps[p_b][0], j2 = eps[p_b][1], k2 = eps[p_b][2];
      int a, c1, c2;

      for (a = 0; a < QDP_Ns; a++) {

        for (c1 = 0; c1 < QDP_Ns; c1++) {
 
          for (c2 = 0; c2 < QDP_Ns; c2++) {

            QLA_Complex s3; 
            QLA_c_eq_r(s3, 0.0);

            /*************************************************************
             * s3 += epsilon_{k2,i2,j2} f[i2, c1] * q[j2, c2; k1, a]
             *************************************************************/
            QLA_c_peq_c_times_c(s3, QLA_elem_D(*f,i2,c1), QLA_elem_P(*q, j2, c2, k1, a) );
            QLA_c_meq_c_times_c(s3, QLA_elem_D(*f,j2,c1), QLA_elem_P(*q, i2, c2, k1, a) );
        
            /*************************************************************
             * v1[k1,k2,a] = v1[d] <- s3
             *************************************************************/
            int const d = 3 * ( 3 * ( QDP_Ns * ( QDP_Ns * c1 + c2 ) + a ) + k2 ) + k1;
            QLA_c_eq_c( QLA_DN_elem_V(*v1, d), s3);

          }  /* end of loop on propagator source spinor index c2 */
        }  /* end of loop on fermion spinor index c1 */
      }  /* end of loop on propagator sink spinor index a */
    }  /* end of loop on color permutation index p_b / color index k2 */
  }  /* end of loop on color index k1 */

}  /* end of do_su3contract_v1_openspin */

/*************************************************************
 * function V2:
 * reduction of DN color vector v1 with a DiracPropagator to DN
 * color vector v2
 *************************************************************/
static void do_su3contract_v2_openspin (int nc, QLA_DN_ColorVector( nc, (*v2)), int idx, void *env ) {

  V2o_arg *arg = env;  /* V2 type arg contains v1, so this is different here compared to do_su3contract_v2 */
  QLA_DN_ColorVector( QDP_Ns*QDP_Ns*QDP_Ns*9, (*v1) );
  QLA_D3_DiracPropagator *q2;
  v1 = &arg->a[idx];
  q2 = &arg->b[idx];
  static const int eps[3][3] = { { 0, 1, 2}, { 1, 2, 0}, { 2, 0, 1} };
  int p_b;

  for (p_b = 0; p_b < 3; p_b++) { /* Nc */

    int i2 = eps[p_b][0], j2 = eps[p_b][1], k2 = eps[p_b][2];
    int a1, a2, a3, b1, b2;

    for (a1 = 0; a1 < QDP_Ns; a1++) {
    for (a2 = 0; a2 < QDP_Ns; a2++) {
    for (a3 = 0; a3 < QDP_Ns; a3++) {

      for (b1 = 0; b1 < QDP_Ns; b1++) {
      for (b2 = 0; b2 < QDP_Ns; b2++) {

        QLA_Complex s3;
        QLA_c_eq_r(s3, 0.0);

        int k1;
        for (k1 = 0; k1 < 3; k1++) { /* Nc */

          int idxp = 3 * ( 3 * ( QDP_Ns * ( QDP_Ns * a1 + a2 ) + a3 ) + k1 ) + i2;
          int idxm = 3 * ( 3 * ( QDP_Ns * ( QDP_Ns * a1 + a2 ) + a3 ) + k1 ) + j2;

          /*************************************************************
           * s3 += epsilon_{k2,i2,j2} v1[k1, i2, a] * q2[ k1, b; j2, c]
           *************************************************************/

          QLA_c_peq_c_times_c(s3, QLA_DN_elem_V(*v1, idxp), QLA_elem_P(*q2, k1, b1, j2, b2) );
          QLA_c_meq_c_times_c(s3, QLA_DN_elem_V(*v1, idxm), QLA_elem_P(*q2, k1, b1, i2, b2) );

        }  /* end of loop on summation color index k1 */

        /*************************************************************
         * v2[a1,a2,a3,b1,b2,k2] = s3
         *************************************************************/
        int const d = 3 * ( QDP_Ns * ( QDP_Ns * ( QDP_Ns *( QDP_Ns * a1 + a2 ) + a3 ) + b1 ) + b2 ) + k2;
        QLA_c_eq_c( QLA_DN_elem_V(*v2, d), s3);

      }  /* end of loop on propagator sink spinor index b2 */
      }  /* end of loop on propagator source spinor index b1 */
    }  /* end of loop on v1 spinor index a3 */
    }  /* end of loop on v1 spinor index a2 */
    }  /* end of loop on v1 spinor index a1 */
  }  /* end of loop on permutation index p_b / open color index k2 */
}  /* end of do_su3contract_v2_openspin */

/*************************************************************
 * function V6:
 * reduction of 2 DiracFermion and one DiracPropagator 
 * to DN color vector v6
 *************************************************************/
static void do_su3contract_v6_openspin (int nc, QLA_DN_ColorVector( nc, (*v6)), int idx, void *env ) {

  Vffp_arg *arg = env;
  QLA_D3_DiracFermion    *f1 = &arg->a[idx];
  QLA_D3_DiracFermion    *f2 = &arg->b[idx];

  QLA_D3_DiracPropagator *q = &arg->c[idx];

  static const int eps[3][3] = { { 0, 1, 2}, { 1, 2, 0}, { 2, 0, 1} };
  int p_b, k1;
  int a1, a2, b1, b2;
 
  for (a1 = 0; a1 < QDP_Ns; a1++) {
  for (a2 = 0; a2 < QDP_Ns; a2++) {

    QLA_Complex c0, c1, c2;
    QLA_c_eq_r ( c0, 0.0);
    QLA_c_eq_r ( c1, 0.0);
    QLA_c_eq_r ( c2, 0.0);

//    for (p_b = 0; p_b < 3; p_b++) { /* Nc */
//      int i2 = eps[p_b][0], j2 = eps[p_b][1], k2 = eps[p_b][2];

      /*************************************************************
       * s_eff += epsilon_{k2,i2,j2} f1[ i2, a1] * f2[ j2, a2 ]
       * k2 fixed
       *************************************************************/
      QLA_c_eq_c_times_c  ( c0, QLA_elem_D(*f1,1,a1), QLA_elem_D(*f2, 2, a2 ) );
      QLA_c_meq_c_times_c ( c0, QLA_elem_D(*f1,2,a1), QLA_elem_D(*f2, 1, a2 ) );

      QLA_c_eq_c_times_c  ( c1, QLA_elem_D(*f1,2,a1), QLA_elem_D(*f2, 0, a2 ) );
      QLA_c_meq_c_times_c ( c1, QLA_elem_D(*f1,0,a1), QLA_elem_D(*f2, 2, a2 ) );

      QLA_c_eq_c_times_c  ( c2, QLA_elem_D(*f1,0,a1), QLA_elem_D(*f2, 1, a2 ) );
      QLA_c_meq_c_times_c ( c2, QLA_elem_D(*f1,1,a1), QLA_elem_D(*f2, 0, a2 ) );

      for (k1 = 0; k1 < 3; k1++) { /* Nc */

        for (b1 = 0; b1 < QDP_Ns; b1++) {
        for (b2 = 0; b2 < QDP_Ns; b2++) {

          QLA_Complex s3;
          QLA_c_eq_r ( s3, 0.0);
          /*************************************************************
           * s3 = ( epsilon f f)[a1,a2,k2] * q[ k2, b1; k1, b2]
           *************************************************************/
          QLA_c_peq_c_times_c( s3, c0, QLA_elem_P(*q, 0, b1, k1, b2) );
          QLA_c_peq_c_times_c( s3, c1, QLA_elem_P(*q, 1, b1, k1, b2) );
          QLA_c_peq_c_times_c( s3, c2, QLA_elem_P(*q, 2, b1, k1, b2) );

          /*************************************************************
           * v2[a1,a2,a3,b1,b2,k2] = s3
           *************************************************************/
          int const d = 3 * ( QDP_Ns * ( QDP_Ns * ( QDP_Ns * a1 + a2 ) + b1 ) + b2 ) + k1;
          QLA_c_eq_c( QLA_DN_elem_V(*v6, d), s3);

        }  /* end of loop on propagator source spinor index b2 */
        }  /* end of loop on propagator sink   spinor index b1 */
      }  /* end of loop on summation color index k1 */
//    }  /* end of loop on permutation index p_b / open color index k2 */
  }  /* end of loop on fermion 2 spinor index a2 */
  }  /* end of loop on fermion 1 spinor index a1 */
}  /* end of do_su3contract_v6_openspin */

/*************************************************************
 * function V3:
 *   reduction of a DiracFermion (intended: stochastic source or stochastic
 *   propagator) and a DiracPropagator (intended: point-to-all fwd or sequential
 *   propagator) to a color vector with NcxNs components
 *
 *************************************************************/
static void do_su3contract_v3_openspin (int nc, QLA_DN_ColorVector( nc, (*v3)), int idx, void *env ) {

  V1_arg *arg = env;
  QLA_D3_DiracFermion    *f = &arg->a[idx];
  QLA_D3_DiracPropagator *q = &arg->b[idx];
  int k, a1, b1, b2;

  for (k = 0; k < 3; k++) { /* Nc */

    for ( a1 = 0; a1 < QDP_Ns; a1++) { /* Ns */

      for ( b1 = 0; b1 < QDP_Ns; b1++) { /* Ns */
      for ( b2 = 0; b2 < QDP_Ns; b2++) { /* Ns */

        int const d = 3 * (  QDP_Ns * ( QDP_Ns * a1 + b1 ) + b2 ) + k;
        int l;
 
        QLA_Complex s3; 
        QLA_c_eq_r(s3, 0.0);

        for (l = 0; l < 3; l++) {  /* Nc */
          /*************************************************************
           * s3 += f[ l, b] * q[ l, b; k, a]
           *************************************************************/
          QLA_c_peq_ca_times_c(s3, QLA_elem_D(*f,l,a1), QLA_elem_P(*q, l, b1, k, b2) );
        }  /* end of loop on summation color index l */

        /*************************************************************
         * v3[k,a] = v3[d] <- s3
         *************************************************************/
        QLA_c_eq_c( QLA_DN_elem_V(*v3, d), s3);

      }  /* end of propagator source spinor index b2 */
      }  /* end of propagator sink   spinor index b1 */
    }  /* end of loop on fermion spinor index a1 */
  }  /* end of loop on propagator source color index k */

}  /* end of do_su3contract_v3_openspin */

/*************************************************************/
/*************************************************************/

/*************************************************************
 * QLUA wrapper function to call do_su3contract_v1
 *************************************************************/
static int q_su3contract_v1 (lua_State *L ) {

  mLatDirFerm3 *a = qlua_checkLatDirFerm3(L, 1, NULL, 3); /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v1 = qlua_newLatColVecN(L, lua_gettop(L), QDP_Ns*9 );

  V1_arg arg;

  CALL_QDP(L);
  arg.a = QDP_D3_expose_D(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);

  QDP_DN_V_eq_funcia( v1->ptr, do_su3contract_v1, &arg, *S->qss);
  QDP_D3_reset_D(a->ptr);
  QDP_D3_reset_P(b->ptr);
#if 0
#endif
  return 1;
}  /* end of q_su3contract_v1 */

/*************************************************************
 * QLUA wrapper function to call do_su3contract_v2
 *************************************************************/
static int q_su3contract_v2 (lua_State *L ) {

  mLatColVecN *a  = qlua_checkLatColVecN(L, 1, NULL, QDP_Ns*9 );
  mLattice *S     = qlua_ObjLattice(L, 1);
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v2 = qlua_newLatColVecN(L, lua_gettop(L), (QDP_Ns*QDP_Ns*QDP_Ns*3) );

  V2_arg arg;

  CALL_QDP(L);
  arg.a = QDP_DN_expose_V(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);
  QDP_DN_V_eq_funcia(v2->ptr, do_su3contract_v2, &arg, *S->qss);
  QDP_DN_reset_V(a->ptr);
  QDP_D3_reset_P(b->ptr);

  return 1;
}  /* end of q_su3contract_v2 */

/*************************************************************
 * QLUA wrapper function to call do_su3contract_v3
 *************************************************************/
static int q_su3contract_v3 (lua_State *L ) {

  mLatDirFerm3 *a = qlua_checkLatDirFerm3(L, 1, NULL, 3); /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v3 = qlua_newLatColVecN(L, lua_gettop(L), QDP_Ns*3 );

  V1_arg arg;

  CALL_QDP(L);
  arg.a = QDP_D3_expose_D(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);

  QDP_DN_V_eq_funcia( v3->ptr, do_su3contract_v3, &arg, *S->qss);
  QDP_D3_reset_D(a->ptr);
  QDP_D3_reset_P(b->ptr);

  return 1;
}  /* end of q_su3contract_v3 */

/*************************************************************
 * QLUA wrapper function to call do_su3contract_v4aux
 *************************************************************/
static int q_su3contract_v4aux (lua_State *L ) {

  mLatDirProp3 *a = qlua_checkLatDirProp3(L, 1, NULL, 3);  /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);  /* Lattice type */
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatDirProp3 *p = qlua_newLatDirProp3(L, lua_gettop(L), 3);

  V5_arg arg;  /* V5_arg, i.e. struct with two DiracPropagator3 */

  CALL_QDP(L);
  arg.a = QDP_D3_expose_P(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);

  QDP_D3_P_eq_funcia(p->ptr, do_su3contract_v4aux, &arg, *S->qss);

  QDP_D3_reset_P(a->ptr);
  QDP_D3_reset_P(b->ptr);

  return 1;
}  /* end of q_su3contract_v4 */


/*************************************************************
 * QLUA wrapper function to call do_su3contract_v4
 *************************************************************/
static int q_su3contract_v4 (lua_State *L ) {

  mLatDirFerm3 *a  = qlua_checkLatDirFerm3(L, 1, NULL, 3);  /* the 1st arg */
  mLattice *S      = qlua_ObjLattice(L, 1);  /* Lattice type */
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v2 = qlua_newLatColVecN(L, lua_gettop(L), (QDP_Ns*QDP_Ns*QDP_Ns*3) ); /* output, color vector of length N = Ns^3 x Nc */

  V1_arg arg;  /* V1_arg, i.e. struct with one DiracFermion3 and one DiracPropagator3 */

  CALL_QDP(L);
  arg.a = QDP_D3_expose_D(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);
  QDP_DN_V_eq_funcia(v2->ptr, do_su3contract_v4, &arg, *S->qss);
  QDP_D3_reset_D(a->ptr);
  QDP_D3_reset_P(b->ptr);

  return 1;
}  /* end of q_su3contract_v4 */

/*************************************************************/
/*************************************************************/

/*************************************************************
 * QLUA wrapper function to call do_su3contract_t1
 *************************************************************/
static int q_su3contract_t1 (lua_State *L ) {

  mLatDirProp3 *a = qlua_checkLatDirProp3(L, 1, NULL, 3);  /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);  /* Lattice type */
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v = qlua_newLatColVecN(L, lua_gettop(L), (QDP_Ns*QDP_Ns) ); /* output, color vector of length N = Ns^2 */

  V5_arg arg;  /* V5_arg, i.e. struct with two DiracPropagator3 */

  CALL_QDP(L);
  arg.a = QDP_D3_expose_P(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);
  QDP_DN_V_eq_funcia( v->ptr, do_su3contract_t1, &arg, *S->qss);
  QDP_D3_reset_P( a->ptr );
  QDP_D3_reset_P( b->ptr );

  return 1;
}  /* end of q_su3contract_t1 */


/*************************************************************/
/*************************************************************/

/*************************************************************
 * QLUA wrapper function to call do_su3contract_t2
 *************************************************************/
static int q_su3contract_t2 (lua_State *L ) {

  mLatDirProp3 *a = qlua_checkLatDirProp3(L, 1, NULL, 3);  /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);  /* Lattice type */
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v = qlua_newLatColVecN(L, lua_gettop(L), (QDP_Ns*QDP_Ns) ); /* output, color vector of length N = Ns^2 */

  V5_arg arg;  /* V5_arg, i.e. struct with two DiracPropagator3 */

  CALL_QDP(L);
  arg.a = QDP_D3_expose_P(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);
  QDP_DN_V_eq_funcia( v->ptr, do_su3contract_t2, &arg, *S->qss);
  QDP_D3_reset_P( a->ptr );
  QDP_D3_reset_P( b->ptr );

  return 1;
}  /* end of q_su3contract_t2 */

/*************************************************************/
/*************************************************************/

/*************************************************************
 * QLUA wrapper function to call do_su3colortrace
 *************************************************************/
static int q_su3colortrace (lua_State *L ) {

  mLatDirProp3 *a = qlua_checkLatDirProp3(L, 1, NULL, 3);  /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);  /* Lattice type */
  mLatColVecN *v = qlua_newLatColVecN(L, lua_gettop(L), (QDP_Ns*QDP_Ns) ); /* output, color vector of length N = Ns^2 */

  CALL_QDP(L);
  QDP_DN_V_eq_funcia( v->ptr, do_su3colortrace, QDP_D3_expose_P(a->ptr), *S->qss);
  QDP_D3_reset_P( a->ptr );

  return 1;
}  /* end of q_su3colortrace */

/*************************************************************/
/*************************************************************/

/*************************************************************
 * function PP:
 * reduction of two DiracPropagator3
 *
 *************************************************************/
static void do_su3contract_pp (int nc, QLA_DN_ColorVector( nc, (*v)), int idx, void *env ) {

  V5_arg *arg = env;  /* contains 2 DiracPropgator3 */
  QLA_D3_DiracPropagator *q1;
  QLA_D3_DiracPropagator *q2;
  q1 = &arg->a[idx];
  q2 = &arg->b[idx];

  static const int eps[3][3] = { { 0, 1, 2}, { 1, 2, 0}, { 2, 0, 1} };
    
  for ( int l1 = 0; l1 < QDP_Ns; l1++) {  /* left  spinor index */
  for ( int l2 = 0; l2 < QDP_Ns; l2++) {  /* right spinor index */

    for ( int k1 = 0; k1 < QDP_Ns; k1++) {  /* left  spinor index */
    for ( int k2 = 0; k2 < QDP_Ns; k2++) {  /* right spinor index */

      for ( int icol = 0; icol < 3; icol++) {  /* color index */

        int m1 = eps[icol][0];
        int m2 = eps[icol][1];
        int m3 = eps[icol][2];

      for ( int jcol = 0; jcol < 3; jcol++) {  /* color index */

        int n1 = eps[jcol][0];
        int n2 = eps[jcol][1];
        int n3 = eps[jcol][2];

        QLA_Complex s3;
        QLA_c_eq_r( s3, 0.0);

        /*************************************************************
         * s3 += q1[ k, a; l, c ] * q2[ k, c; l, b ]
         *************************************************************/
        QLA_c_peq_c_times_c( s3, QLA_elem_P ( *q1, m1, l1, n1, l2 ), QLA_elem_P ( *q2, m2, k1, n2, k2 ) );
        QLA_c_meq_c_times_c( s3, QLA_elem_P ( *q1, m2, l1, n1, l2 ), QLA_elem_P ( *q2, m1, k1, n2, k2 ) );
        QLA_c_meq_c_times_c( s3, QLA_elem_P ( *q1, m1, l1, n2, l2 ), QLA_elem_P ( *q2, m2, k1, n1, k2 ) );
        QLA_c_peq_c_times_c( s3, QLA_elem_P ( *q1, m2, l1, n2, l2 ), QLA_elem_P ( *q2, m1, k1, n1, k2 ) );

        /*************************************************************
         * v[a,b] = s3
         *************************************************************/
        int d = ( ( ( ( l1 * QDP_Ns + l2 ) * QDP_Ns + k1 ) * QDP_Ns + k2 ) * 3 + m3 ) * 3 + n3;
        QLA_c_eq_c( QLA_DN_elem_V(*v, d), s3);


      }}  /* end of loop on color indices icol, jcol */

    }}  /* end of loop on spinor indices k1, k2 */

  }}  /*  end of loop on spinor indices l1, l2 */

}  /* end of do_su3contract_pp */


/*************************************************************/
/*************************************************************/

/*************************************************************
 * function PPP:
 * reduction of three DiracPropagator3
 *
 * reduction of do_su3contract_pp output field with a DiracPropagator3
 *
 *************************************************************/
static void do_su3contract_ppp (int nc, QLA_DN_ColorVector( nc, (*v)), int idx, void *env ) {

  V6_arg *arg = env;
  QLA_DN_ColorVector( QDP_Ns*QDP_Ns*QDP_Ns*QDP_Ns*9, (*v1) );
  QLA_D3_DiracPropagator *q2;
  v1 = &arg->a[idx];
  q2 = &arg->b[idx];
  
  for ( int l1 = 0; l1 < QDP_Ns; l1++ ) {  /* spin index */
  for ( int l2 = 0; l2 < QDP_Ns; l2++ ) {  /* spin index */

    for ( int k1 = 0; k1 < QDP_Ns; k1++ ) {  /* spin index */
    for ( int k2 = 0; k2 < QDP_Ns; k2++ ) {  /* spin index */

      int d = ( ( l1 * QDP_Ns + l2 ) * QDP_Ns + k1 ) * QDP_Ns + k2;

      for ( int h1 = 0; h1 < QDP_Ns; h1++ ) {
      for ( int h2 = 0; h2 < QDP_Ns; h2++ ) {

        QLA_Complex s3;
        QLA_c_eq_r ( s3, 0.0 );

        for ( int a = 0; a < 3; a++ ) {

        for ( int b = 0; b < 3; b++ ) {

          int dd = ( 3 * d + a ) * 3 + b;

          /*************************************************************
           * s3 += 
           *************************************************************/
          QLA_c_peq_c_times_c ( s3, QLA_DN_elem_V(*v1, dd ), QLA_elem_P ( *q2, h1, a, h2, b ) );

        }}  /* end of loop on color index a, b */
         
        int dd = QDP_Ns * ( QDP_Ns * d + h1 ) + h2;

        QLA_c_eq_c (  QLA_DN_elem_V(*v, dd ), s3 );

      }}  /* end of loop on spin indices h1, h2 */

    }}  /* end of loop on spin indices k1, k2 */
    
  }}  /* end of loop on spin indices l11, l22 */

}  /* end of do_su3contract_ppp */


/*************************************************************/
/*************************************************************/

/*************************************************************
 * QLUA wrapper function to call do_su3contract_pp
 *************************************************************/
static int q_su3contract_pp (lua_State *L ) {

  mLatDirProp3 *a = qlua_checkLatDirProp3(L, 1, NULL, 3); /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v1 = qlua_newLatColVecN(L, lua_gettop(L), QDP_Ns*QDP_Ns*QDP_Ns*QDP_Ns*9 );

  V5_arg arg;  /* 2 DiracPropagator3 */

  CALL_QDP(L);
  arg.a = QDP_D3_expose_P ( a->ptr );
  arg.b = QDP_D3_expose_P ( b->ptr );

  QDP_DN_V_eq_funcia( v1->ptr, do_su3contract_pp, &arg, *S->qss);
  QDP_D3_reset_P ( a->ptr );
  QDP_D3_reset_P ( b->ptr );
#if 0
#endif
  return 1;
}  /* end of q_su3contract_pp */

/*************************************************************/
/*************************************************************/

/*************************************************************
 * QLUA wrapper function to call do_su3contract_ppp
 *************************************************************/
static int q_su3contract_ppp (lua_State *L ) {

  mLatColVecN *a  = qlua_checkLatColVecN(L, 1, NULL, QDP_Ns*QDP_Ns*QDP_Ns*QDP_Ns*9 );
  mLattice *S     = qlua_ObjLattice(L, 1);
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v2 = qlua_newLatColVecN(L, lua_gettop(L), ( QDP_Ns * QDP_Ns * QDP_Ns * QDP_Ns * QDP_Ns * QDP_Ns ) );

  V6_arg arg;

  CALL_QDP(L);
  arg.a = QDP_DN_expose_V ( a->ptr );
  arg.b = QDP_D3_expose_P ( b->ptr );
  QDP_DN_V_eq_funcia(v2->ptr, do_su3contract_ppp, &arg, *S->qss);
  QDP_DN_reset_V ( a->ptr );
  QDP_D3_reset_P ( b->ptr );

  return 1;
}  /* end of q_su3contract_ppp */


/*************************************************************/
/*************************************************************/

/*************************************************************
 * QLUA wrapper function to call do_su3contract_v1_openspin
 *************************************************************/
static int q_su3contract_v1_openspin (lua_State *L ) {

  mLatDirFerm3 *a = qlua_checkLatDirFerm3(L, 1, NULL, 3); /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v1 = qlua_newLatColVecN(L, lua_gettop(L), QDP_Ns*QDP_Ns*QDP_Ns*9 );

  V1_arg arg;

  CALL_QDP(L);
  arg.a = QDP_D3_expose_D(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);

  QDP_DN_V_eq_funcia( v1->ptr, do_su3contract_v1_openspin, &arg, *S->qss);
  QDP_D3_reset_D(a->ptr);
  QDP_D3_reset_P(b->ptr);

  return 1;
}  /* end of q_su3contract_v1_openspin */

/*************************************************************
 * QLUA wrapper function to call do_su3contract_v2_openspin
 *************************************************************/
static int q_su3contract_v2_openspin (lua_State *L ) {

  mLatColVecN *a  = qlua_checkLatColVecN(L, 1, NULL, QDP_Ns*QDP_Ns*QDP_Ns*9 );
  mLattice *S     = qlua_ObjLattice(L, 1);
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v2 = qlua_newLatColVecN(L, lua_gettop(L), (QDP_Ns*QDP_Ns*QDP_Ns*QDP_Ns*QDP_Ns*3) );

  V2o_arg arg;

  CALL_QDP(L);
  arg.a = QDP_DN_expose_V(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);
  QDP_DN_V_eq_funcia(v2->ptr, do_su3contract_v2_openspin, &arg, *S->qss);
  QDP_DN_reset_V(a->ptr);
  QDP_D3_reset_P(b->ptr);

  return 1;
}  /* end of q_su3contract_v2_openspin */

/*************************************************************
 * QLUA wrapper function to call do_su3contract_v6_openspin
 *************************************************************/
static int q_su3contract_v6_openspin (lua_State *L ) {

  mLatDirFerm3 *a = qlua_checkLatDirFerm3(L, 1, NULL, 3); /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);
  mLatDirFerm3 *b = qlua_checkLatDirFerm3(L, 2, S, 3); /* the 2nd arg */
  mLatDirProp3 *c = qlua_checkLatDirProp3(L, 3, S, 3); /* the 3rd arg */

  mLatColVecN *v6 = qlua_newLatColVecN(L, lua_gettop(L), (QDP_Ns*QDP_Ns*QDP_Ns*QDP_Ns*3) );

  Vffp_arg arg;

  CALL_QDP(L);
  arg.a = QDP_D3_expose_D(a->ptr);
  arg.b = QDP_D3_expose_D(b->ptr);
  arg.c = QDP_D3_expose_P(c->ptr);
  QDP_DN_V_eq_funcia(v6->ptr, do_su3contract_v6_openspin, &arg, *S->qss);
  QDP_D3_reset_D(a->ptr);
  QDP_D3_reset_D(b->ptr);
  QDP_D3_reset_P(c->ptr);

  return 1;
}  /* end of q_su3contract_v6_openspin */

/*************************************************************
 * QLUA wrapper function to call do_su3contract_v3_openspin
 *************************************************************/
static int q_su3contract_v3_openspin (lua_State *L ) {

  mLatDirFerm3 *a = qlua_checkLatDirFerm3(L, 1, NULL, 3); /* the 1st arg */
  mLattice *S     = qlua_ObjLattice(L, 1);
  mLatDirProp3 *b = qlua_checkLatDirProp3(L, 2, S, 3); /* the 2nd arg */
  mLatColVecN *v3 = qlua_newLatColVecN(L, lua_gettop(L), QDP_Ns*QDP_Ns*QDP_Ns*3 );

  V1_arg arg;

  CALL_QDP(L);
  arg.a = QDP_D3_expose_D(a->ptr);
  arg.b = QDP_D3_expose_P(b->ptr);

  QDP_DN_V_eq_funcia( v3->ptr, do_su3contract_v3_openspin, &arg, *S->qss);
  QDP_D3_reset_D(a->ptr);
  QDP_D3_reset_P(b->ptr);

  return 1;
}  /* end of q_su3contract_v3_openspin */

/*************************************************************/
/*************************************************************/

#endif /* USE_NcN */

#endif /* USE_Nc3 */

static struct luaL_Reg fLatDirProp[] = {
    { "DiracPropagator",   q_latdirprop  },
    { "DiracPropagatorN",  q_latdirpropN },
    { NULL,                NULL          }
};

static struct luaL_Reg fQCDDirProp[] = {
#if USE_Nc3
    { "quarkContract12",   q_su3contract12 },
    { "quarkContract13",   q_su3contract13 },
    { "quarkContract14",   q_su3contract14 },
    { "quarkContract23",   q_su3contract23 },
    { "quarkContract24",   q_su3contract24 },
    { "quarkContract34",   q_su3contract34 },
#  if USE_NcN
    { "quarkContractV1",   q_su3contract_v1},
    { "quarkContractV2",   q_su3contract_v2},
    { "quarkContractV3",   q_su3contract_v3},
    { "quarkContractV4",   q_su3contract_v4},
    { "quarkContractV4aux",q_su3contract_v4aux},
    { "quarkContractT1",   q_su3contract_t1},
    { "quarkContractT2",   q_su3contract_t2},
    { "quarkColortrace",   q_su3colortrace},
    { "quarkContractPP",   q_su3contract_pp},
    { "quarkContractPPP",  q_su3contract_ppp},
    { "quarkContractV1_openspin",   q_su3contract_v1_openspin},
    { "quarkContractV2_openspin",   q_su3contract_v2_openspin},
    { "quarkContractV3_openspin",   q_su3contract_v3_openspin},
    { "quarkContractV6_openspin",   q_su3contract_v6_openspin},
#  endif
#endif
    { NULL,                NULL            }
};

int
init_latdirprop(lua_State *L)
{
    luaL_register(L, qcdlib, fQCDDirProp);
    luaL_getmetatable(L, opLattice);
    luaL_register(L, NULL, fLatDirProp);
    lua_pop(L, 1);
#if USE_Nc2
    qlua_reg_op2(ops2);
    qlua_reg_dot(qLatDirProp2,  q_P_dot_2);
#endif
#if USE_Nc3
    qlua_reg_op2(ops3);
    qlua_reg_dot(qLatDirProp3,  q_P_dot_3);
#endif
#if USE_NcN
    qlua_reg_op2(opsN);
    qlua_reg_dot(qLatDirPropN,  q_P_dot_N);
#endif

    return 0;
}

void
fini_latdirprop(void)
{
}
