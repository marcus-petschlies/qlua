#include <math.h>
#include <complex.h>
#include <string.h>
#include <assert.h>

#include "qmp.h"
#include "lhpc-aff.h"

#include "modules.h"                                                /* DEPS */
#include "qlua.h"                                                   /* DEPS */
#include "lattice.h"                                                /* DEPS */
#include "extras.h"                                                 /* DEPS */
#include "extras_common.h"                                          /* DEPS */
#include "qparam.h"                                                 /* DEPS */
#include "latdirprop.h"

//#if 1 /* old version */
static double complex
calc_exp_iphase(const int coord[], const int c0[],
                const int latsize[], const int mom[])
{
    double phase = 0.;
    int i;
    for (i = 0; i < 4; i++)
        phase += mom[i] * (double)(coord[i] - c0[i]) / latsize[i];
    phase *= 2 * M_PI;
    return cos(phase) + I * sin(phase) ;
}




/* optimized version of building blocks
   compute tr(B^+ \Gamma_n F) [n=0..15] and projects on n_qext momenta
   select time interval [tsrc: tsnk] and does time reversal if time_rev==1
   save results to aff_w[aff_kpath . 'g%d/qx%d_qy%d_qz%d']
   Parameters:
    csrc = { xsrc, ysrc, zsrc, tsrc }
    tsnk
    qext[4 * i_qext + dir]  ext.mom components
    time_rev ==0 for proton_3, ==1 for proton_negpar_3
    bc_baryon_t =+/-1 boundary condition for baryon 2pt[sic!] function; =bc_quark^3
 */
static const char *
save_bb(lua_State *L,
        mLattice *S,
        mAffWriter *aff_w,
        const char *aff_kpath,
        QDP_D3_DiracPropagator *F,
        QDP_D3_DiracPropagator *B,
        const int *csrc,             /* [qRank] */
        int tsnk,
        int n_mom,
        const int *mom,             /* [n_mom][qRank] */
        int time_rev,                /* 1 to reverse, 0 to not */
        int t_axis,                  /* 0-based */
        double bc_baryon_t)
{
    /* TODO: replace this section with Gamma definitions in gamma_dgr.h
       gamma matrix parameterization for left multiplication:
       Gamma_n [i,j] = gamma_coeff[n][i] * \delta_{gamma_ind[n][i]==j}

                v[0]    a[0]*v[I[0]]
        Gamma * v[1] =  a[1]*v[I[1]]
                v[2]    a[2]*v[I[2]]
                v[3]    a[3]*v[I[3]]
        or
        (Gamma * X)_{ik} = a[i] * X[I[i],k]
     */
    double complex gamma_left_coeff[16][4] = {
        { 1, 1, 1, 1 },             /* G0 = 1 */
        { I, I,-I,-I },             /* G1 = g1 */
        {-1, 1, 1,-1 },             /* G2 = g2 */
        {-I, I,-I, I },             /* G3 = g1 g2 */
        { I,-I,-I, I },             /* G4 = g3 */
        {-1, 1,-1, 1 },             /* G5 = g1 g3 */
        {-I,-I,-I,-I },             /* G6 = g2 g3 */
        { 1, 1,-1,-1 },             /* G7 = g1 g2 g3 */
        { 1, 1, 1, 1 },             /* G8 = g4 */
        { I, I,-I,-I },             /* G9 = g1 g4 */
        {-1, 1, 1,-1 },             /* G10= g2 g4 */
        {-I, I,-I, I },             /* G11= g1 g2 g4 */
        { I,-I,-I, I },             /* G12= g3 g4 */
        {-1, 1,-1, 1 },             /* G13= g1 g3 g4 */
        {-I,-I,-I,-I },             /* G14= g2 g3 g4 */
        { 1, 1,-1,-1 },             /* G15= g1 g2 g3 g4 */
    };
    int gamma_left_ind[16][4] = {
        { 0, 1, 2, 3 },             /* G0 = 1 */
        { 3, 2, 1, 0 },             /* G1 = g1 */
        { 3, 2, 1, 0 },             /* G2 = g2 */
        { 0, 1, 2, 3 },             /* G3 = g1 g2 */
        { 2, 3, 0, 1 },             /* G4 = g3 */
        { 1, 0, 3, 2 },             /* G5 = g1 g3 */
        { 1, 0, 3, 2 },             /* G6 = g2 g3 */
        { 2, 3, 0, 1 },             /* G7 = g1 g2 g3 */
        { 2, 3, 0, 1 },             /* G8 = g4 */
        { 1, 0, 3, 2 },             /* G9 = g1 g4 */
        { 1, 0, 3, 2 },             /* G10= g2 g4 */
        { 2, 3, 0, 1 },             /* G11= g1 g2 g4 */
        { 0, 1, 2, 3 },             /* G12= g3 g4 */
        { 3, 2, 1, 0 },             /* G13= g1 g3 g4 */
        { 3, 2, 1, 0 },             /* G14= g2 g3 g4 */
        { 0, 1, 2, 3 },             /* G15= g1 g2 g3 g4 */
    };
#define get_mom(mom_list, i_mom) ((mom_list) + 4*(i_mom))
    if (4 != S->rank ||
            4 != QDP_Ns ||
            3 != t_axis) {
        return "not implemented for this dim, spin, color, or t-axis";
    }

    int latsize[4];
    QDP_latsize_L(S->lat, latsize);
    if (NULL == aff_w ||
            NULL == aff_kpath ||
            NULL == mom ||
            n_mom < 0) {
        return "incorrect pointer parameters";
    }
    int i;
    for (i = 0 ; i < S->rank; i++) {
        if (csrc[i] < 0 || latsize[i] <= csrc[i]) {
            return "incorrect source coordinates";
        }
    }
    if (tsnk < 0 || latsize[t_axis] <= tsnk) {
        return "incorrect sink t-coordinate";
    }

    if (n_mom <= 0)
        return NULL;       /* relax */

    int src_snk_dt = -1;
    int lt = latsize[t_axis];
    if (!time_rev) {
        src_snk_dt = (lt + tsnk - csrc[t_axis]) % lt;
    } else {
        src_snk_dt = (lt + csrc[t_axis] - tsnk) % lt;
    }

    int bb_arr_size = 16 * n_mom * (src_snk_dt + 1) * 2 * sizeof(double);
    double *bb_arr = qlua_malloc(L, bb_arr_size);
    memset(bb_arr, 0, bb_arr_size);
#define bb_real(i_gamma, i_mom) ((bb_arr) + (src_snk_dt + 1) * (0 + 2 * ((i_mom) + n_mom * (i_gamma))))
#define bb_imag(i_gamma, i_mom) ((bb_arr) + (src_snk_dt + 1) * (1 + 2 * ((i_mom) + n_mom * (i_gamma))))

    double complex *exp_iphase = qlua_malloc(L, n_mom * sizeof(double complex));

    int coord[4];
    double complex trc_FBd[4][4];
    QLA_D3_DiracPropagator *F_exp = QDP_D3_expose_P(F);
    QLA_D3_DiracPropagator *B_exp = QDP_D3_expose_P(B);

    int i_site;
    int sites = QDP_sites_on_node_L(S->lat);
    for (i_site = 0; i_site < sites; i_site++) {
        QDP_get_coords_L(S->lat, coord, QDP_this_node, i_site);

        int t = -1;
        if (!time_rev) {
            t = (lt + coord[t_axis] - csrc[t_axis]) % lt;
        } else {
            t = (lt + csrc[t_axis] - coord[t_axis]) % lt;
        }
        if (src_snk_dt < t)
            continue;

        /* precalc phases for inner contraction loop */
        int i_mom;
        for (i_mom = 0 ; i_mom < n_mom ; i_mom++) {
            exp_iphase[i_mom] = calc_exp_iphase(coord, csrc,
                    latsize, get_mom(mom, i_mom));
//            printf("%e+I*%e\n", creal(exp_iphase[i_mom]), cimag(exp_iphase[i_mom]));
        }

        /* compute trace_{color} [ F * B^\dag]
           [is,js]  = sum_{ic,jc,ks} F[ic,is; jc,ks] * (B[ic,js; jc,ks])^*
           is,js,ks - spin, ic,jc - color
         */
        int is, js, ks, ic, jc;
        for (is = 0; is < 4; is++) {
            for (js = 0; js < 4; js++) {
                QLA_D_Complex sum;
                QLA_c_eq_r(sum, 0);
                for (ks = 0; ks < 4; ks++) {
                    for (ic = 0; ic < 3 ; ic++)
                        for (jc = 0; jc < 3 ; jc++)
                            QLA_c_peq_c_times_ca(sum,
                                    QLA_elem_P(F_exp[i_site], ic,is, jc,ks),
                                    QLA_elem_P(B_exp[i_site], ic,js, jc,ks));
                }
                trc_FBd[is][js] = QLA_real(sum) + I*QLA_imag(sum);
            }
        }

        /* cycle over Gamma */
        int gn;
        for (gn = 0; gn < 16 ; gn++) {
            double complex sum = 0.;
            /* compute contractions Gamma(n) */
            for (is = 0; is < 4; is++)
                sum += gamma_left_coeff[gn][is] * trc_FBd[gamma_left_ind[gn][is]][is];
            /* mult. by phase and add to timeslice sum */
            for (i_mom = 0; i_mom < n_mom; i_mom++) {
                double complex aux = exp_iphase[i_mom] * sum;
                bb_real(gn, i_mom)[t] += creal(aux);
                bb_imag(gn, i_mom)[t] += cimag(aux);
            }
        }
    }

    qlua_free(L, exp_iphase);

    /* global sum */
    if (QMP_sum_double_array(bb_arr, bb_arr_size / sizeof(double))) {
        qlua_free(L, bb_arr);
        return "QMP_sum_double_array error";
    }

    /* save to AFF */
    if (aff_w->master) {
        struct AffNode_s *aff_top = NULL;
        aff_top = aff_writer_mkpath(aff_w->ptr, aff_w->dir, aff_kpath);
        if (NULL == aff_top) {
            qlua_free(L, bb_arr);
            return aff_writer_errstr(aff_w->ptr);
        }

        double complex *cplx_buf = qlua_malloc(L, (src_snk_dt + 1) * sizeof(double complex));
        char buf[200];
        int gn, i_mom, t;
        for (gn = 0; gn < 16; gn++)
            for (i_mom = 0; i_mom < n_mom; i_mom++) {
                /* copy & mult by bc, if necessary */
                const double *bb_re_cur = bb_real(gn, i_mom),
                             *bb_im_cur = bb_imag(gn, i_mom);
                if (!time_rev) {    /* no bc */
                    for (t = 0 ; t <= src_snk_dt; t++)
                        cplx_buf[t] = bb_re_cur[t] + I*bb_im_cur[t];
                } else {
                    if (gn < 8) {
                        for (t = 0 ; t <= src_snk_dt; t++)
                            cplx_buf[t] = bc_baryon_t * (bb_re_cur[t] + I*bb_im_cur[t]);
                    } else {
                        for (t = 0 ; t <= src_snk_dt; t++)
                            cplx_buf[t] = -bc_baryon_t * (bb_re_cur[t] + I*bb_im_cur[t]);
                    }
                }
                /* write to AFF */
                snprintf(buf, sizeof(buf), "g%d/qx%d_qy%d_qz%d",
                         gn, get_mom(mom, i_mom)[0],
                         get_mom(mom, i_mom)[1], get_mom(mom, i_mom)[2]);
                struct AffNode_s *node = aff_writer_mkpath(aff_w->ptr, aff_top, buf);
                if (NULL == node) {
                    qlua_free(L, bb_arr);
                    qlua_free(L, cplx_buf);
                    return aff_writer_errstr(aff_w->ptr);
                }
                if (aff_node_put_complex(aff_w->ptr, node, cplx_buf, src_snk_dt + 1)) {
                    qlua_free(L, bb_arr);
                    qlua_free(L, cplx_buf);
                    return aff_writer_errstr(aff_w->ptr);
                }
            }

        qlua_free(L, cplx_buf);
    }

#undef bb_real
#undef bb_imag
#undef get_mom
    qlua_free(L, bb_arr);
    QDP_D3_reset_P(F);
    QDP_D3_reset_P(B);
    return 0;
}
//#else /* OpenMP version */

static const char *
save_bb_fast(lua_State *L,
        mLattice *S,
        mAffWriter *aff_w,
        const char *aff_kpath,
        QDP_D3_DiracPropagator *F,
        QDP_D3_DiracPropagator *B,
        const int *csrc,            /* [ndim] */
        int tlen,                   /* XXX changed tsnk->tlen! */
        int n_mom,
        const int *mom3d_list,          /* [n_mom][ndim-1] */
        int time_rev,               /* 1 to reverse, 0 to not */
        int t_axis,                 /* 0-based */
        double bc_baryon_t)
{
#define NC      3
#define NS      4
#define NGAMMA  16
    /* TODO: replace this section with Gamma definitions in gamma_dgr.h
       gamma matrix parameterization for left multiplication:
       Gamma_n [i,j] = gamma_coeff[n][i] * \delta_{gamma_ind[n][i]==j}

                v[0]    a[0]*v[I[0]]
        Gamma * v[1] =  a[1]*v[I[1]]
                v[2]    a[2]*v[I[2]]
                v[3]    a[3]*v[I[3]]
        or
        (Gamma * X)_{ik} = a[i] * X[I[i],k]
     */
    double complex gamma_left_coeff[NGAMMA][NS] = {
        { 1, 1, 1, 1 },             /* G0 = 1 */
        { I, I,-I,-I },             /* G1 = g1 */
        {-1, 1, 1,-1 },             /* G2 = g2 */
        {-I, I,-I, I },             /* G3 = g1 g2 */
        { I,-I,-I, I },             /* G4 = g3 */
        {-1, 1,-1, 1 },             /* G5 = g1 g3 */
        {-I,-I,-I,-I },             /* G6 = g2 g3 */
        { 1, 1,-1,-1 },             /* G7 = g1 g2 g3 */
        { 1, 1, 1, 1 },             /* G8 = g4 */
        { I, I,-I,-I },             /* G9 = g1 g4 */
        {-1, 1, 1,-1 },             /* G10= g2 g4 */
        {-I, I,-I, I },             /* G11= g1 g2 g4 */
        { I,-I,-I, I },             /* G12= g3 g4 */
        {-1, 1,-1, 1 },             /* G13= g1 g3 g4 */
        {-I,-I,-I,-I },             /* G14= g2 g3 g4 */
        { 1, 1,-1,-1 },             /* G15= g1 g2 g3 g4 */
    };
    int gamma_left_ind[NGAMMA][NS] = {
        { 0, 1, 2, 3 },             /* G0 = 1 */
        { 3, 2, 1, 0 },             /* G1 = g1 */
        { 3, 2, 1, 0 },             /* G2 = g2 */
        { 0, 1, 2, 3 },             /* G3 = g1 g2 */
        { 2, 3, 0, 1 },             /* G4 = g3 */
        { 1, 0, 3, 2 },             /* G5 = g1 g3 */
        { 1, 0, 3, 2 },             /* G6 = g2 g3 */
        { 2, 3, 0, 1 },             /* G7 = g1 g2 g3 */
        { 2, 3, 0, 1 },             /* G8 = g4 */
        { 1, 0, 3, 2 },             /* G9 = g1 g4 */
        { 1, 0, 3, 2 },             /* G10= g2 g4 */
        { 2, 3, 0, 1 },             /* G11= g1 g2 g4 */
        { 0, 1, 2, 3 },             /* G12= g3 g4 */
        { 3, 2, 1, 0 },             /* G13= g1 g3 g4 */
        { 3, 2, 1, 0 },             /* G14= g2 g3 g4 */
        { 0, 1, 2, 3 },             /* G15= g1 g2 g3 g4 */
    };
    if (NULL == aff_w ||
            NULL == aff_kpath ||
            NULL == mom3d_list ||
            n_mom < 0) {
        return "incorrect pointer parameters";
    }
    const char *errstr = NULL;
    int ndim = S->rank;
    long long locvol_space, locvol_space2;
    assert(ndim <= QLUA_MAX_LATTICE_RANK);
    assert(t_axis < ndim);

    if (    4 != ndim ||
            NS != QDP_Ns ||
            ndim - 1  != t_axis) {  /* FIXME actually should work for other values... need testing */
        return "not implemented for this dim, spin, color, or t-axis";
    }

    int latsize[QLUA_MAX_LATTICE_RANK];
    int locdim[QLUA_MAX_LATTICE_RANK];
    int loc_c0[QLUA_MAX_LATTICE_RANK];
    int *x3_idx = NULL;
    double complex *bblat_arr = NULL;
    double complex *bbmom_arr = NULL;
    double complex *ph_matr = NULL;

    QDP_Lattice *lat = S->lat;
    long long n_sites = QDP_sites_on_node_L(lat);
    QDP_latsize_L(lat, latsize);
    for (int i = 0 ; i < ndim ; i++) {
        if (csrc[i] < 0 || latsize[i] <= csrc[i])
            return "incorrect source coordinates";
    }
    if (n_mom <= 0)
        return NULL;       /* relax */

//    int src_snk_dt = tlen - 1;
    int lt = latsize[t_axis];
    int tsrc = csrc[t_axis];
//    int tsnk = (!time_rev 
//            ? (tsrc + lt + src_snk_dt) % lt
//            : (tsrc + lt - src_snk_dt) % lt);

    calc_subgrid(S, locdim, loc_c0);
    x3_idx = locindex_space(lat, QDP_this_node, t_axis, locdim, loc_c0);
    int loc_lt  = locdim[t_axis];

    locvol_space = 1;
    for (int d = 0 ; d < ndim ; d++)
        if (d != t_axis) locvol_space *= locdim[d];
    assert(locvol_space * loc_lt == n_sites);

#define bblat(g, t, i_x3) (bblat_arr[(i_x3) + (locvol_space)*((g) + (NGAMMA)*(t))])
    int bblat_arr_size = locvol_space * tlen * NGAMMA * sizeof(bblat_arr[0]);
    bblat_arr = malloc(bblat_arr_size);
#define bbmom(g, t, i_mom) (bbmom_arr[(i_mom) + (n_mom)*((g) + (NGAMMA)*(t))])
    int bbmom_arr_size = n_mom * tlen * NGAMMA * sizeof(bbmom_arr[0]);
    bbmom_arr = malloc(bbmom_arr_size);
    /* planewave phase matrix, spatial loc.vol only [i_x3 + locvol_space * i_mom] */
    ph_matr = matr_exp_iphase_space(ndim, csrc, latsize, loc_c0, locdim,
            t_axis, n_mom, mom3d_list, &locvol_space2);
    assert(locvol_space2 == locvol_space);

    /* check mem alloc */
    if (    NULL == x3_idx ||
            NULL == bblat_arr ||
            NULL == bbmom_arr ||
            NULL == ph_matr) {
        errstr = "not enough memory";
        goto clearerr_0_0;
    }

    QLA_D3_DiracPropagator *F_exp = QDP_D3_expose_P(F);
    QLA_D3_DiracPropagator *B_exp = QDP_D3_expose_P(B);

    /* set to zeros because some [t] may be out of the subvol */
    memset(bblat_arr, 0, bblat_arr_size);
#pragma omp parallel for
    for (int i_site = 0; i_site < n_sites; i_site++) {
        int i_x3 = x3_idx[i_site];
        int coord[QLUA_MAX_LATTICE_RANK];
        QDP_get_coords_L(lat, coord, QDP_this_node, i_site);

        /* t relative to tsrc */
        int t = (!time_rev 
                ? (lt + coord[t_axis] - tsrc) % lt
                : (lt + tsrc - coord[t_axis]) % lt);
        if (tlen <= t)
            continue;

        /* compute trace_{color} [ F * B^\dag]
           [is,js]  = sum_{ic,jc,ks} F[ic,is; jc,ks] * (B[ic,js; jc,ks])^*
           is,js,ks - spin, ic,jc - color
         */
        double complex trc_FBd[NS][NS];
        QLA_D3_DiracPropagator *Fx = F_exp + i_site,
                               *Bx = B_exp + i_site;
        for (int is = 0 ; is < NS ; is++) {
            for (int js = 0 ; js < NS ; js++) {
                QLA_D_Complex sum;
                QLA_c_eq_r(sum, 0);
                for (int ks = 0 ; ks < NS ; ks++) {
                    for (int ic = 0 ; ic < NC ; ic++)
                        for (int jc = 0 ; jc < NC ; jc++)
                            QLA_c_peq_c_times_ca(sum,
                                    QLA_elem_P(*Fx, ic,is, jc,ks),
                                    QLA_elem_P(*Bx, ic,js, jc,ks));
                }
                trc_FBd[is][js] = QLA_real(sum) + I*QLA_imag(sum);
            }
        }

        /* cycle over Gamma */
        for (int gn = 0 ; gn < NGAMMA ; gn++) {
            double complex sum = 0.;
            /* compute contractions Gamma(n) */
            for (int is = 0 ; is < NS ; is++)
                sum += gamma_left_coeff[gn][is] * trc_FBd[gamma_left_ind[gn][is]][is];
            bblat(gn, t, i_x3) = sum;
        }
    }
    double zzero[2] = {0., 0.},
           zone[2]  = {1., 0.};

    /* set to zeros, just in case */
    memset(bbmom_arr, 0, bbmom_arr_size);
    /* FIXME multiply only the relevant part? 
       XXX be careful if trange wraps around: can be disjoint */
    cblas_zgemm(CblasColMajor, CblasTrans, CblasNoTrans, 
            n_mom, NGAMMA * tlen, locvol_space,
            zone, ph_matr, locvol_space, bblat_arr, locvol_space, 
            zzero, bbmom_arr, n_mom);

    /* global sum */
    if (QMP_sum_double_array((double *)bbmom_arr, bbmom_arr_size / sizeof(double))) {
        errstr = "QMP_sum_double_array error";
        goto clearerr_0_0;
    }

    /* sic! momentum striding : momenta are (ndim-1), with [t_axis] component skipped */
#define get_mom(i_mom) ((mom3d_list) + (ndim-1)*(i_mom))
#define get_momkey(i_mom) ((momkey_list) + (i_mom)*(momkey_len))
    /* save to AFF */
    if (aff_w->master) {
        char *momkey_list = NULL;
        double complex *cplx_buf = NULL;

        struct AffNode_s *aff_top = NULL;
        aff_top = aff_writer_mkpath(aff_w->ptr, aff_w->dir, aff_kpath);
        if (NULL == aff_top) {
            errstr = aff_writer_errstr(aff_w->ptr);
            goto clearerr_1_0;
        }
        
        cplx_buf = malloc(tlen * sizeof(cplx_buf[0]));
        int momkey_len  = 6*QLUA_MAX_LATTICE_RANK;
        momkey_list = malloc(momkey_len * n_mom);
        if (    NULL == cplx_buf ||
                NULL == momkey_list) {
            errstr = "not enough memory" ;
            goto clearerr_1_0;
        }
        /* generate momkey strings */
        for (int i_mom = 0 ; i_mom < n_mom ; i_mom++) {
            const int *mom = get_mom(i_mom);
            /* FIXME support arbitrary momenta: need to change momkey format */
            assert(4 == ndim);
            snprintf(get_momkey(i_mom), momkey_len,
                    "qx%d_qy%d_qz%d", mom[0], mom[1], mom[2]);
        }
        /* save data */
        char buf[64];
        for (int gn = 0; gn < 16; gn++) {
            snprintf(buf, sizeof(buf), "g%d", gn);
            struct AffNode_s *aff_gn = aff_writer_mkdir(aff_w->ptr, aff_top, buf);
            if (NULL == aff_gn) {
                errstr = aff_writer_errstr(aff_w->ptr);
                goto clearerr_1_0;
            }
            for (int i_mom = 0; i_mom < n_mom; i_mom++) {
                /* copy & mult by bc, if necessary */
                for (int t = 0 ; t < tlen ; t++)
                    cplx_buf[t] = bbmom(gn, t, i_mom);
                if (time_rev) {
                    double bc_mult = (gn < 8 ? bc_baryon_t : -bc_baryon_t);
                    for (int t = 0 ; t < tlen ; t++)
                        cplx_buf[t] *=  bc_mult;
                }
                /* write to AFF */
                struct AffNode_s *node = aff_writer_mkdir(aff_w->ptr, aff_gn, 
                        get_momkey(i_mom));
                if (NULL == node) {
                    errstr = aff_writer_errstr(aff_w->ptr);
                    goto clearerr_1_0;
                }
                if (aff_node_put_complex(aff_w->ptr, node, cplx_buf, tlen)) {
                    errstr = aff_writer_errstr(aff_w->ptr);
                    goto clearerr_1_0;
                }
            }
        }
clearerr_1_0:
        free_not_null(momkey_list);
        free_not_null(cplx_buf);
    }

clearerr_0_0:
    free_not_null(bblat_arr);
    free_not_null(bbmom_arr);
    free_not_null(ph_matr);
    free_not_null(x3_idx);
    QDP_D3_reset_P(F);
    QDP_D3_reset_P(B);
    return errstr;
#undef NC
#undef NS
#undef NGAMMA
#undef bblat
#undef bbmom
//#undef get_mom
}
//#endif

int
q_save_bb(lua_State *L)
{
    mAffWriter *aff_w = qlua_checkAffWriter(L, 1);
    const char *key_path = luaL_checkstring(L, 2);
    mLatDirProp3 *F = qlua_checkLatDirProp3(L, 3, NULL, 3);
    mLattice *S = qlua_ObjLattice(L, 3);
    int Sidx = lua_gettop(L);
    mLatDirProp3 *B = qlua_checkLatDirProp3(L, 4, S, 3);
    int *csrc = qlua_checkintarray(L, 5, S->rank, NULL);
    int tsnk = luaL_checkint(L, 6);
    int time_rev = luaL_checkint(L, 8);
    int t_axis = luaL_checkint(L, 9);
    double bc_baryon = luaL_checknumber(L, 10);
    int i, j, k;
    const char *status = NULL;

    if (Sidx < 11)
        return luaL_error(L, "bad arguments");

    if (csrc == NULL)
        return luaL_error(L, "bad value for coord_src");

    luaL_checktype(L, 7, LUA_TTABLE);
    int n_qext = lua_objlen(L, 7);
    int qext[n_qext * S->rank];
    for (k = i = 0; i < n_qext; i++) {
        lua_pushnumber(L, i + 1);
        lua_gettable(L, 7);
        qlua_checktable(L, -1, "momentum at #7[%d]", i + 1);
        for (j = 0; j < S->rank; j++, k++) {
            lua_pushnumber(L, j + 1);
            lua_gettable(L, -2);
            qext[k] = qlua_checkint(L, -1, "momentum component at #7[%d][%d]",
                                    i + 1, j + 1);
            lua_pop(L, 1);
        }
        lua_pop(L, 1);
    }

    qlua_Aff_enter(L);
    CALL_QDP(L);

    status = save_bb(L, S, aff_w, key_path, F->ptr, B->ptr, csrc, tsnk, n_qext, qext,
                     time_rev, t_axis, bc_baryon);
    qlua_Aff_leave();

    qlua_free(L, csrc);

    if (status)
        luaL_error(L, status);

    return 0;
}

/* qcd.save_bb_fast(
           aff[1], aff_kpath[2], 
           frw_prop[3], bkw_prop[4], 
           csrc[5], tlen[6], ...[7]
           time_rev[8], t_axis[9], bc_baryon[10],
           )
 */
int
q_save_bb_fast(lua_State *L)
{
    const char *errstr = NULL;
    mAffWriter *aff_w = qlua_checkAffWriter(L, 1);
    const char *aff_kpath = luaL_checkstring(L, 2);
    mLatDirProp3 *F = qlua_checkLatDirProp3(L, 3, NULL, 3);
    mLattice *S = qlua_ObjLattice(L, 3);
    int Sidx = lua_gettop(L);
    if (Sidx < 11)
        return luaL_error(L, "bad arguments");
    mLatDirProp3 *B = qlua_checkLatDirProp3(L, 4, S, 3);

    int tlen = luaL_checkint(L, 6);
    int time_rev = lua_toboolean(L, 8);
    int t_axis = luaL_checkint(L, 9);
    double bc_baryon = luaL_checknumber(L, 10);

    int *csrc = NULL,
        *mom3d_list = NULL;
    csrc       = qlua_check_array1d_int(L, 5, S->rank, NULL);
    if (csrc == NULL) {
        errstr = "bad value for coord_src";
        goto clearerr_1;
    }
    int n_mom = -1;
    mom3d_list   = qlua_check_array2d_int(L, 7, -1, S->rank - 1, &n_mom, NULL);
    if (n_mom < 0) {
        errstr = "bad momentum list";
        goto clearerr_1;
    }

    if (0 < n_mom) {
        qlua_Aff_enter(L);
        CALL_QDP(L);
        errstr = save_bb_fast(L, S, aff_w, aff_kpath, F->ptr, B->ptr, 
                    csrc, tlen, n_mom, mom3d_list, time_rev, t_axis, bc_baryon);
        qlua_Aff_leave();
    }

clearerr_1:
    qlua_free_not_null(L, csrc);
    qlua_free_not_null(L, mom3d_list);

    if (errstr)
        luaL_error(L, errstr);
    
    return 0;
}
