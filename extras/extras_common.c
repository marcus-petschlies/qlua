#include "modules.h"                                                /* DEPS */
#include "qlua.h"                                                   /* DEPS */
#include "lattice.h"                                                /* DEPS */
#include "extras_common.h"

#include <assert.h>

int
is_masternode()
{
    return (0 == QDP_this_node ||       /* masternode */
            QDP_this_node < 0);         /* qdp not initialized */
}

/*****************************************************************************
 coordinate functions
 *****************************************************************************/

/* calculate subgrid dimensions and "lowest corner" coordinate for `lat'
   TODO use qlua_sublattice to DRY
 */
int 
calc_subgrid(mLattice *S, int dims[], int cmin[])
{
    QDP_Lattice *lat = S->lat;
    int ndim = QDP_ndim_L(lat);
    assert(ndim <= QLUA_MAX_LATTICE_RANK);
    int n_site = QDP_sites_on_node_L(lat);
    assert(0 < n_site);
    int lo[QLUA_MAX_LATTICE_RANK],
        hi[QLUA_MAX_LATTICE_RANK];
    qlua_sublattice(lo, hi, S->node, S);

    /* XXX code below is for cross-check */
#if 1
#warning "remove deprecated calc_subgrid"
    QDP_get_coords_L(lat, cmin, QDP_this_node, 0);
    int cmax[QLUA_MAX_LATTICE_RANK];
    for (int k = 0 ; k < ndim ; k++)
        cmax[k] = cmin[k];
    
    int coord[QLUA_MAX_LATTICE_RANK];
    for (int i_site = 1; i_site < n_site ; i_site++) {
        QDP_get_coords_L(lat, coord, QDP_this_node, i_site);
        for (int k = 0 ; k < ndim ; k++) {
            if (coord[k] < cmin[k])
                cmin[k] = coord[k];
            if (cmax[k] < coord[k])
                cmax[k] = coord[k];
        }
    }
    
    int vol = 1;
    for (int k = 0; k < ndim; k++) {
        dims[k] = 1 + cmax[k] - cmin[k];
        assert(0 < dims[k]);
        vol *= dims[k];
        assert(cmin[k] == lo[k]);
        assert(dims[k] == hi[k] - lo[k]);
    }
    assert(vol == n_site);
#else
    /* this part copies qlua_sublattice to the output */
    for (int k = 0; k < ndim; k++) {
        cmin[k] = lo[k];
        dims[k] = hi[k] - lo[k];
    }
#endif

    return 0;
}

int *
locindex_space(QDP_Lattice *lat, int node, int t_axis, 
        const int locsize[], const int loc_c0[])
/* calculate local index -> local space index assuming rectangular subgrid
    lat         QDP lattice object
    ndim        == lat. dimension D (sizes of coord, c0, latsize)
    t_axis      skipped dimension; if 0 < t_axis or ndim<=t_axis, full index is calculated
    locsize     local lattice size
    loc_c0      coor. D-vector: min.corner of the local subgrid 
    locsize, loc_c0 are computed by calc_subgrid
    RETURN : pointer to the index or NULL if ENOMEM
 */
{
    int ndim = QDP_ndim_L(lat);
    assert(ndim <= QLUA_MAX_LATTICE_RANK);
    int dim_stride[QLUA_MAX_LATTICE_RANK],
        coord[QLUA_MAX_LATTICE_RANK];
    int d, fact;
    /* calculate strides for dimensions */
    for (d = 0, fact = 1 ; d < ndim ; d++) {
        if (d == t_axis)
            dim_stride[d]   = 0;
        else {
            dim_stride[d]   = fact;
            fact            *= locsize[d];
        }
    }
    int n_site  = QDP_numsites_L(lat, QDP_this_node);
    int *idx    = malloc(n_site * sizeof(int));
    if (NULL == idx)
        return NULL;
    for (int i_site = 0 ; i_site < n_site ; i_site++) {
        QDP_get_coords_L(lat, coord, node, i_site);
        int i_x3 = 0;
        for (int d = 0 ; d < ndim ; d++)
            i_x3 += dim_stride[d] * (coord[d] - loc_c0[d]);
        idx[i_site] = i_x3;
    }
    return idx;
}

/* momentum phase for (D-1) phase transform, one point
    ndim        == lat. dimension D (sizes of coord, c0, latsize)
    coord       D-coordinate D-vector; {t_axis} component is present but ignored
    c0          initial coord for the phase; {t_axis} component is present but ignored
    latsize     total lattice size
    t_axis      skipped dimension
    mom         momentum (D-1)-vector; {t_axis} component is skipped
 */
double complex
calc_exp_iphase_space(int ndim, 
                const int coord[], 
                const int c0[], 
                const int latsize[], 
                int t_axis,
                const int mom[])
{
    double phase = 0.;
    for (int i = 0; i < ndim; i++) {
        if (i < t_axis || t_axis < 0)
            phase += mom[i] * (double)(coord[i] - c0[i]) / latsize[i];
        else if (t_axis < i)
            phase += mom[i - 1] * (double)(coord[i] - c0[i]) / latsize[i];
    }
    phase *= 2 * M_PI;
    return cos(phase) + I * sin(phase) ;
}
/* initialize a matrix of momentum phases for (D-1) phase transform 
        (ColMajor)[i_x3, i_mom] = matr[i_x3 + locvol*i_mom]
    ndim        == lat. dimension D (sizes of coord, c0, latsize)
    c0          initial coord for the phase; {t_axis} component is present but ignored
    latsize     total lattice size
    loc_lo      low-corner of the local volume
    locdim      local lattice size
    t_axis      skipped dimension
    nmom        number of (D-1)-momenta
    mom         momentum (D-1)-vector; {t_axis} component is skipped 
                [i_mom, mu] = mom[i_mom*(ndim-1) + mu], mu\in[0;ndim-1)
    locvol_     if !=NULL, return the local volume
 */
double complex *
matr_exp_iphase_space(int ndim, 
                const int c0[], 
                const int latsize[], 
                const int loc_lo[],
                const int locdim[],
                int t_axis,
                int nmom, const int mom[],
                long long *locvol_)
{
    long long locvol = 1;
    long long xstride[QLUA_MAX_LATTICE_RANK + 1];
    for (int i = 0 ; i < ndim ; i++) {
        xstride[i] = locvol;
        if (i != t_axis)
            locvol *= locdim[i];
    }
    xstride[ndim] = locvol;
    if (NULL != locvol_) 
        *locvol_ = locvol;

    double complex *ph_matr = NULL;
    if (NULL == (ph_matr = malloc(locvol * nmom * sizeof(ph_matr[0]))))
        return NULL;

#pragma omp parallel for
    for (long long i_x = 0 ; i_x < locvol ; i_x++) {
        int coord[QLUA_MAX_LATTICE_RANK];
        for (int i = 0; i < ndim; i++)
            coord[i] = loc_lo[i] + (i_x % xstride[1+i]) / xstride[i];
        for (int i_mom = 0 ; i_mom < nmom ; i_mom++)
            ph_matr[i_x + locvol * i_mom] = calc_exp_iphase_space(ndim, 
                    coord, c0, latsize, t_axis, mom + i_mom * (ndim - 1));
    }
    return ph_matr;
}



struct get_exp_ipx_C_arg {
    int dim;
    const int *latsize;
    const int *ft_mom;
    const int *ft_x0;
};

static void
get_planewave_C_D(QLA_D_Complex *dest, int coord[], void *arg_)
{
    struct get_exp_ipx_C_arg *arg = (struct get_exp_ipx_C_arg *)arg_;
    double phase = 0.;
    for (int d = 0; d < arg->dim ; d++) {
        phase += 2 * M_PI * (coord[d] - arg->ft_x0[d]) * 
                (double)arg->ft_mom[d] / arg->latsize[d];
    }
    QLA_c_eq_r_plus_ir(*dest, cos(phase), sin(phase));
}

void
get_planewave_C_F(QLA_F_Complex *dest, int coord[], void *arg_)
{
    struct get_exp_ipx_C_arg *arg = (struct get_exp_ipx_C_arg *)arg_;
    double phase = 0.;
    for (int d = 0; d < arg->dim ; d++) {
        phase += 2 * M_PI * (coord[d] - arg->ft_x0[d]) * 
                (double)arg->ft_mom[d] / arg->latsize[d];
    }
    QLA_c_eq_r_plus_ir(*dest, cos(phase), sin(phase));
}

void
calc_F_C_eq_planewave(QDP_F_Complex *exp_ipx, 
        const int mom[], const int x0[])
{
    QDP_Lattice *lat = QDP_F_get_lattice_C(exp_ipx);
    int dim = QDP_ndim_L(lat);
    int *latsize = malloc(sizeof(int) * dim);
    QDP_latsize_L(lat, latsize);
    struct get_exp_ipx_C_arg arg = { dim, latsize, mom, x0 };
    QDP_F_C_eq_funca(exp_ipx, get_planewave_C_F, &arg, QDP_all_L(lat));
    free(latsize);
}
void
calc_D_C_eq_planewave(QDP_D_Complex *exp_ipx, 
        const int mom[], const int x0[])
{
    QDP_Lattice *lat = QDP_D_get_lattice_C(exp_ipx);
    int dim = QDP_ndim_L(lat);
    int *latsize = malloc(sizeof(int) * dim);
    QDP_latsize_L(lat, latsize);
    struct get_exp_ipx_C_arg arg = { dim, latsize, mom, x0 };
    QDP_D_C_eq_funca(exp_ipx, get_planewave_C_D, &arg, QDP_all_L(lat));
    free(latsize);
}
