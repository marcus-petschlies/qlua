
const char *
gen_laplace_ID(lua_State *L, mLattice *S,
               QDP_Type *res, double a, double b, 
               QDP_D3_ColorMatrix **u, QDP_Type *x, int skip_axis)
{
    QLA_Real r1;
    QDP_Shift *neighbor = QDP_neighbor_L(S->lat);

    if (skip_axis < 0 || S->rank <= skip_axis)
        r1 = a - 2 * b * S->rank;
    else 
        r1 = a - 2 * b * (S->rank - 1);
    QDP_ID_eq_r_times_ID(res, &r1, x, *S->qss);
    int i; 
    QDP_Type *aux = QDP_create_ID(S->lat);
    QDP_ID_eq_zero(aux, *S->qss);
    for (i = 0; i < S->rank; i++) {
        if (i == skip_axis)
            continue;
        QDP_ID_peq_M_times_sID(aux, u[i], x, neighbor[i], QDP_forward, 
                *S->qss);
        QDP_ID_peq_sMa_times_sID(aux, u[i], x, neighbor[i], QDP_backward,
                *S->qss);
    }
    QLA_Real r2 = b;
    QDP_ID_peq_r_times_ID(res, &r2, aux, *S->qss);
    QDP_destroy_ID(aux);
    return NULL;
}


const char *
gen_laplace_noshift_ID(lua_State *L, mLattice *S,
               QDP_Type *res, double a, double b, 
               QDP_D3_ColorMatrix **u, QDP_Type *x, int skip_axis)
{
    QLA_Real r1;

    if (skip_axis < 0 || S->rank <= skip_axis)
        r1 = a - 2 * b * S->rank;
    else 
        r1 = a - 2 * b * (S->rank - 1);
    QDP_ID_eq_r_times_ID(res, &r1, x, *S->qss);
    int i; 
    QDP_Type *aux = QDP_create_ID(S->lat);
    QDP_ID_eq_zero(aux, *S->qss);
    for (i = 0; i < S->rank; i++) {
        if (i == skip_axis)
            continue;
        QDP_ID_peq_M_times_ID(aux, u[i], x,
                *S->qss);
        QDP_ID_peq_Ma_times_ID(aux, u[i], x, 
                *S->qss);
    }
    QLA_Real r2 = b;
    QDP_ID_peq_r_times_ID(res, &r2, aux, *S->qss);
    QDP_destroy_ID(aux);
    return NULL;
}


const char *
gen_laplace_opt_ID(lua_State *L, mLattice *S,
               QDP_Type *res, double a, double b, int niter,
               QDP_D3_ColorMatrix **u, QDP_Type *x, int skip_axis)
{
    const char *st = NULL;
    QLA_Real r1, r2;
    QDP_Shift *neighbor = QDP_neighbor_L(S->lat);
    QDP_Type *aux = NULL, *neigh = NULL;
    QDP_Type *sh_f[QLUA_MAX_LATTICE_RANK],
             *sh_b[QLUA_MAX_LATTICE_RANK];
    QDP_D3_ColorMatrix *u_b[QLUA_MAX_LATTICE_RANK];
    for (int i = 0 ; i < S->rank ; i++) {
        u_b[i] = NULL;
        sh_f[i] = sh_b[i] = NULL;
    }
    /* allocate auxs and temps for repeated shifts */
    if (    NULL == (aux = QDP_create_ID(S->lat))||
            NULL == (neigh = QDP_create_ID(S->lat))) {
        st = "cannot allocate field";
        goto clearerr_1;
    }
    for (int i = 0 ; i < S->rank ; i++) {
        if (i == skip_axis) continue;
        u_b[i]  = QDP_D3_create_M_L(S->lat);
        sh_f[i] = QDP_create_ID(S->lat);
        sh_b[i] = QDP_create_ID(S->lat);
        if (NULL == u_b[i] || NULL == sh_f[i] || NULL == sh_b[i]) {
            st = "cannot allocate field";
            goto clearerr_1;
        }
        QDP_D3_M_eq_sM(u_b[i], u[i], neighbor[i], QDP_backward, *S->qss);
    }
    /* ndim-dependent arithmetics: (a + b*Laplacian)*f = (a-2*dim*b)*f + b*neigh(f) */
    if (skip_axis < 0 || S->rank <= skip_axis)
        r1 = a - 2 * b * S->rank;
    else 
        r1 = a - 2 * b * (S->rank - 1);
    r2 = b;

    /* use res and aux as temps; switch at the start if necessary */
    if (niter % 2) { 
        QDP_Type *tmp = aux ; aux = res ; res = tmp; 
    }
    QDP_ID_eq_ID(res, x, *S->qss);
    for (int k = 0 ; k < niter ; k++) {
        QDP_ID_eq_zero(neigh, *S->qss);
        for (int i = 0; i < S->rank; i++) {
            if (i == skip_axis)
                continue;
            /* optimization of repeated shifts recommended J.Osborn */
            QDP_ID_eq_sID(sh_f[i], res, neighbor[i], QDP_forward, *S->qss);
            QDP_ID_eq_sID(sh_b[i], res, neighbor[i], QDP_backward, *S->qss);

            QDP_ID_peq_M_times_ID(neigh, u[i], sh_f[i], *S->qss);
            QDP_ID_peq_Ma_times_ID(neigh, u_b[i], sh_b[i], *S->qss);

            QDP_discard_ID(sh_f[i]);
            QDP_discard_ID(sh_b[i]);
        }
        QDP_ID_eq_r_times_ID(aux, &r1, res, *S->qss);
        QDP_ID_peq_r_times_ID(aux, &r2, neigh, *S->qss);
        QDP_Type *tmp = aux ; aux = res ; res = tmp;
        QDP_discard_ID(aux);
        QDP_discard_ID(neigh);
    }

clearerr_1:
    if (NULL != aux) QDP_destroy_ID(aux);
    if (NULL != neigh) QDP_destroy_ID(neigh);
    for (int i = 0 ; i < S->rank ; i++) {
        if (i == skip_axis) continue;
        if (NULL != u_b[i]) QDP_D3_destroy_M(u_b[i]);
        if (NULL != sh_f[i]) QDP_destroy_ID(sh_f[i]);
        if (NULL != sh_f[i]) QDP_destroy_ID(sh_b[i]);
    }
    return st;
}

#undef gen_laplace_ID
#undef gen_laplace_noshift_ID
#undef QDP_Type
#undef QDP_create_ID
#undef QDP_destroy_ID
#undef QDP_ID_eq_zero
#undef QDP_ID_eq_r_times_ID
#undef QDP_ID_peq_M_times_sID
#undef QDP_ID_peq_sMa_times_sID
#undef QDP_ID_peq_M_times_ID
#undef QDP_ID_peq_Ma_times_ID
#undef QDP_ID_peq_r_times_ID
#undef gen_laplace_opt_ID
#undef QDP_discard_ID
#undef QDP_ID_eq_ID
#undef QDP_ID_eq_sID
#undef QDP_ID_peq_M_times_ID
#undef QDP_ID_peq_Ma_times_ID
