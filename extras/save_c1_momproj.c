#include <assert.h>
#include <math.h>
#include <complex.h>
#include <string.h>

#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_matrix.h>

#include "qmp.h"
#include "lhpc-aff.h"

#include "modules.h"                                                /* DEPS */
#include "qlua.h"                                                   /* DEPS */
#include "lattice.h"                                                /* DEPS */
#include "extras.h"                                                 /* DEPS */
#include "extras_common.h"                                          /* DEPS */
#include "latcomplex.h"
#include "qparam.h"

#include "mpi.h"

static const char *
save_c1_momproj(lua_State *L, 
        mLattice *S, 
        mAffWriter *aff_w, 
        const char *aff_kpath, 
        int n_fields, 
        const char **strkey_list, 
        QDP_D_Complex **field_list, 
        int n_mom, 
        const int *mom_list, 
        const int *csrc, 
        int tlen,
        int t_axis, 
        double bc_t,
        int ft_sign)
/*  sum over sites with phase exp(i\vec p\vec x)
    XXX note the sign : analog of save_hadspec has to flip the sign of momenta!
        the sign is chosen to agree with save_bb
    strkey_list,
    field_list  [n_fields] arrays of keys,LatComplex to save
    csrc        [ndim] initial coordinates
    mom_list   array[n_mom, (ndim-1)] of momentum components
    t_axis      direction along which Fourier transf is not done
    tlen        number of time slices to save, starting from csrc[t_axis]
 */
{

    int myprocid = 0;
    double ratime, retime;
    MPI_Comm_rank ( MPI_COMM_WORLD, &myprocid );

    if (n_fields <= 0 || n_mom <= 0) /* relax */
        return NULL;
    const char *errstr = NULL;
    int ndim = S->rank;
    long long locvol_space, locvol_space2;
    assert(ndim <= QLUA_MAX_LATTICE_RANK);
    assert(t_axis < ndim);
    assert(1 == ft_sign || -1 == ft_sign);

    QDP_Lattice *lat = S->lat;
    long long n_sites = QDP_sites_on_node_L(S->lat);
    int latsize[QLUA_MAX_LATTICE_RANK];
    QDP_latsize_L(lat, latsize);
    for (int i = 0 ; i < S->rank; i++) {
        if (csrc[i] < 0 || latsize[i] <= csrc[i])
            return "incorrect source coordinates";
    }
    double complex *c2lat_arr = NULL;
    double complex *c2mom_arr = NULL;
    double complex *ph_matr   = NULL;
    QLA_D_Complex **exp_c2 = NULL;
    int *x3_idx = NULL;
    int locdim[QLUA_MAX_LATTICE_RANK];
    int loc_c0[QLUA_MAX_LATTICE_RANK];

    if ( NULL == aff_w ||
         NULL == aff_kpath || 
         NULL == mom_list )
        return "incorrect pointer parameters";

    if (    4 != ndim || 
            4 != QDP_Ns ||
            3 != t_axis)    /* FIXME actually should work for any valid t_axis */
        return "not implemented for this dim, spin, color, or t-axis";
    
    if (n_mom <= 0) 
        return "empty momentum list";
    
    int lt = latsize[t_axis],
        tsrc = csrc[t_axis];
    if (tlen <= 0 || lt < tlen) 
        tlen = lt;       /* default set by passing tlen<=0 or MAX_INT */

    /* timer for calc_subgrid */
    ratime = MPI_Wtime ();

    calc_subgrid(S, locdim, loc_c0);

    retime = MPI_Wtime ();
    fprintf ( stdout, "# [save_c1_momproj] time for calc_subgrid = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

    /* timer for x3_idx */
    ratime = MPI_Wtime ();

    x3_idx = locindex_space(lat, QDP_this_node, t_axis, locdim, loc_c0);

    retime = MPI_Wtime ();
    fprintf ( stdout, "# [save_c1_momproj] time for locindex_space = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

    int loc_lt  = locdim[t_axis],
        loc_t0  = loc_c0[t_axis];

    locvol_space = 1;
    for (int d = 0 ; d < ndim ; d++)
        if (d != t_axis) locvol_space *= locdim[d];

    /* lattice fields 
        c2lat_arr=coord-space corr, local trange loc_lt, [i_x3, {i_c2,t-loc_t0}]
        c2mom_arr=mom-space corr, full trange lt (for gred), [i_mom, {i_c2,t}]
        i_t is changing slowest to make the local trange contiguous
       XXX local trange is GEMM'ed at every node => full lt total; 
            can be reduced for special case of tlen<lt) */
#define c2lat(i_c2, t, i_x3)   (c2lat_arr[(i_x3) + (locvol_space)*((i_c2) + (n_fields)*((t) -(loc_t0)))])
    int c2lat_arr_size  = locvol_space * n_fields * loc_lt * sizeof(c2lat_arr[0]);
    c2lat_arr = malloc(c2lat_arr_size);
#define c2mom(i_c2, t, i_mom) (c2mom_arr[(i_mom) + (n_mom)*((i_c2) + (n_fields)*(t))])
    int c2mom_arr_size = n_mom * n_fields * lt * sizeof(c2mom_arr[0]);
    c2mom_arr   = malloc(c2mom_arr_size);

    /* planewave phase matrix, spatial loc.vol only [i_x3 + locvol_space * i_mom] */

    /* timer for matr_exp_iphase_space */
    ratime = MPI_Wtime ();

    ph_matr = matr_exp_iphase_space(ndim, csrc, latsize, loc_c0, locdim, 
            t_axis, n_mom, mom_list, &locvol_space2);

    retime = MPI_Wtime ();
    fprintf ( stdout, "# [save_c1_momproj] time for matr_exp_iphase_space = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );


    assert(locvol_space2 == locvol_space);

    /* check mem alloc */
    if (    NULL == x3_idx ||
            NULL == c2lat_arr || 
            NULL == c2mom_arr ||
            NULL == ph_matr) {
        errstr = "not enough memory";
        goto clearerr_0_0;
    }

    /* expose fields */
    if (NULL == (exp_c2 = malloc(n_fields * sizeof(exp_c2[0])))) {
        errstr = "not enough memory";
        goto clearerr_0_0;
    }
    for (int i_c2 = 0 ; i_c2 < n_fields ; i_c2++)
        exp_c2[i_c2] = QDP_D_expose_C(field_list[i_c2]);

    memset(c2lat_arr, 0, c2lat_arr_size);   /* zeros may be optimized in gemm */

    /* timer for making FT input field */
    ratime = MPI_Wtime ();

#pragma omp parallel for
    for (int i_site = 0 ; i_site < n_sites ; i_site++) {
        int i_x3 = x3_idx[i_site];
        int coord[QLUA_MAX_LATTICE_RANK];
        QDP_get_coords_L(lat, coord, QDP_this_node, i_site);
        int tcoord = coord[t_axis];
        /* init orig lattice fields, including time BC, only relevant timeslices */
        if (tlen <= (lt + tcoord - tsrc) % lt)
            continue;
        int bc_factor = (tsrc <= tcoord ? 1 : bc_t);
        /* TODO select only relevant timeslices? */
        assert(loc_t0 <= tcoord && tcoord < loc_t0 + loc_lt);
        for (int i_c2 = 0 ; i_c2 < n_fields ; i_c2++) {
            QLA_D_Complex qla_c = exp_c2[i_c2][i_site];
            c2lat(i_c2, tcoord, i_x3) = bc_factor * (
                    QLA_real(qla_c) + I*QLA_imag(qla_c));
        }
    }
    retime = MPI_Wtime ();
    if ( myprocid == 0 ) fprintf ( stdout, "# [save_c1_momproj] time for reorder = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

    /* timer for QDP_D_reset_C */
    ratime = MPI_Wtime ();

    for (int i_c2 = 0 ; i_c2 < n_fields ; i_c2++)
        QDP_D_reset_C(field_list[i_c2]);

    retime = MPI_Wtime ();
    if ( myprocid == 0 ) fprintf ( stdout, "# [save_c1_momproj] time for QDP_D_reset_C = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

    /* project fields */
    memset(c2mom_arr, 0, c2mom_arr_size);
    double zzero[2] = {0., 0.},
           zone[2]  = {1., 0.};

    /* timer for cblas_zgemm */
    ratime = MPI_Wtime ();

    if (1 == ft_sign)
        cblas_zgemm(CblasColMajor, CblasTrans, CblasNoTrans, 
                n_mom, n_fields * loc_lt, locvol_space, 
                zone, ph_matr, locvol_space, c2lat_arr, locvol_space,
                zzero, c2mom_arr + n_mom * n_fields * loc_t0, n_mom);
    else if (-1 == ft_sign)
        cblas_zgemm(CblasColMajor, CblasConjTrans, CblasNoTrans,
                n_mom, n_fields * loc_lt, locvol_space, 
                zone, ph_matr, locvol_space, c2lat_arr, locvol_space,
                zzero, c2mom_arr + n_mom * n_fields * loc_t0, n_mom);
    else assert(NULL == "impossible");
    
    retime = MPI_Wtime ();
    if ( myprocid == 0 ) fprintf ( stdout, "# [save_c1_momproj] time for cblas_zgemm = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

    /* global sum */

    /* timer for QMP_sum_double_array */
    ratime =  MPI_Wtime ();

    if (QMP_sum_double_array((double *)c2mom_arr, c2mom_arr_size / sizeof(double))) {
        errstr = "QMP_sum_double_array error";
        goto clearerr_0_0;
    }
    
    retime = MPI_Wtime ();
    if ( myprocid == 0 ) fprintf ( stdout, "# [save_c1_momproj] time for QMP_sum_double_array = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

    /* save to AFF
       TODO add hdf5 as an option, perhaps splitting this function
       into computing and writing parts */
#define get_mom(i_mom) ((mom_list) + (ndim-1)*(i_mom))
#define get_momkey(i_mom) ((momkey_list) + (i_mom)*(momkey_len))
    if (aff_w->master) {
        double complex *cplx_buf = NULL;
        char *momkey_list = NULL;

        /* check that path can be created */
        struct AffNode_s *aff_top = NULL;

        /* timer for aff_writer_mkpath */
        aff_top = aff_writer_mkpath(aff_w->ptr, aff_w->dir, aff_kpath);

        retime = MPI_Wtime ();
        if ( myprocid == 0 ) fprintf ( stdout, "# [save_c1_momproj] time for aff_writer_mkpath-dir = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

        if (NULL == aff_top) {
            errstr = aff_writer_errstr(aff_w->ptr);
            goto clearerr_1_0;
        }

        cplx_buf = malloc(tlen * sizeof(double complex));
        int momkey_len  = 6*QLUA_MAX_LATTICE_RANK;
        momkey_list = malloc(momkey_len * n_mom);
        if (NULL == cplx_buf || NULL == momkey_list) {
            errstr = "not enough memory" ;
            goto clearerr_1_1;
        }
        /* generate momkey strings */
        for (int i_mom = 0 ; i_mom < n_mom ; i_mom++) {
            const int *mom = get_mom(i_mom);
            /* FIXME support arbitrary momenta: need to change momkey format */
            assert(4 == ndim);          
            snprintf(get_momkey(i_mom), momkey_len,
                    "PX%d_PY%d_PZ%d", mom[0], mom[1], mom[2]);
        }
        /* save data */
        for (int i_c2 = 0; i_c2 < n_fields; i_c2++) {

            /* timer for aff_writer_mkpath  */
            ratime = MPI_Wtime ();

            struct AffNode_s *aff_c2 = aff_writer_mkpath( aff_w->ptr, aff_top, strkey_list[i_c2]);

            retime = MPI_Wtime ();
            if ( myprocid == 0 ) fprintf ( stdout, "# [save_c1_momproj] time for aff_writer_mkpath-field = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

            if (NULL == aff_c2) {
                errstr = aff_writer_errstr(aff_w->ptr);
                goto clearerr_1_1;
            }

            for (int i_mom = 0; i_mom < n_mom; i_mom++) {
                for (int i_t = 0 ; i_t < tlen ; i_t++)
                    cplx_buf[i_t] = c2mom(i_c2, (tsrc + i_t)%lt, i_mom);
                
                /* timer for aff_writer_mkdir */
                ratime = MPI_Wtime ();
                struct AffNode_s *node = aff_writer_mkdir(aff_w->ptr, aff_c2, get_momkey(i_mom));

                retime = MPI_Wtime ();
                if ( myprocid == 0 ) fprintf ( stdout, "# [save_c1_momproj] time for aff_writer_mkdir = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

                if (NULL == node) {
                    errstr = aff_writer_errstr(aff_w->ptr);
                    goto clearerr_1_1;
                }

                /* timer for aff_node_put_complex */
                ratime = MPI_Wtime ();

                if (aff_node_put_complex(aff_w->ptr, node, cplx_buf, tlen)) {
                    errstr = aff_writer_errstr(aff_w->ptr);
                    goto clearerr_1_1;
                }

                retime = MPI_Wtime ();
                if ( myprocid == 0 ) fprintf ( stdout, "# [save_c1_momproj] time for aff_node_put_complex = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );
            }
        }
clearerr_1_1:
        free_not_null(momkey_list);
        free_not_null(cplx_buf);
clearerr_1_0:
        ;
    }

clearerr_0_0:
    free_not_null(c2lat_arr);
    free_not_null(c2mom_arr);
    free_not_null(ph_matr);
    free_not_null(exp_c2);
    free_not_null(x3_idx);

#undef c2lat
#undef c2mom
#undef get_mom
#undef get_momkey

    return errstr;
}


/* save momentum projections of latcomplex to aff_kpath_prefix/relkpath/PX_PY_PZ
   
   save_momproj(
       aff_writer,                  -- 1 TODO make hdf5 version
       aff_kpath_prefix,            -- 2 
       {relkpath->latcomplex},      -- 3
       csrc,                        -- 4
       mom_list,                    -- 5
       t_axis,                      -- 6
       bc_t,                        -- 7
       {                            -- 8 options
         tlen=-1, 
         ft_sign=1 }
       )
 */
int
q_save_momproj(lua_State *L)
{
    const char *status = NULL;
    int argc = lua_gettop(L);
    if (argc < 7) {
        luaL_error(L, "expect 7 arguments");
        return 1;
    }

    int myprocid = 0;
    MPI_Comm_rank ( MPI_COMM_WORLD, &myprocid );

    mAffWriter *aff_w = qlua_checkAffWriter(L, 1);
    const char *aff_kpath = luaL_checkstring(L, 2);
    int t_axis      = luaL_checkint(L, 6);
    double bc_t     = luaL_checknumber(L, 7);

    int tlen    = -1,
        ft_sign =  1;
    int oidx = 8;
    if (qlua_checkopt_paramtable(L, oidx)) {
        tlen    = qlua_tabkey_intopt(L, oidx, "tlen", -1);
        ft_sign = qlua_tabkey_intopt(L, oidx, "ft_sign", 1);
    }                                                           /* [-0,+0,-] */

    int n_fields = -1,
        n_mom = -1;
    const char **strkey_list = NULL;
    QDP_Complex **field_list = NULL;
    mLattice *S     = NULL; 
    int *csrc = NULL,
        *mom_list = NULL;

    double ratime, retime;

    ratime = MPI_Wtime ();

    qlua_check_latcomplex_strmap(L, 3, &S, &n_fields, &strkey_list, &field_list);
    if (n_fields <= 0)
        goto clearerr_1;

    mom_list   = qlua_check_array2d_int(L, 5, -1, S->rank - 1, &n_mom, NULL);
    if (n_mom < 0)
        goto clearerr_1;

    csrc       = qlua_check_array1d_int(L, 4, S->rank, NULL);
    if (csrc == NULL)
        return luaL_error(L, "bad value for coord_src");

    retime = MPI_Wtime ();
    if ( myprocid == 0 ) fprintf ( stdout, "# [q_save_momproj] time for prologue = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

//    printf("csrc={");for(int i=0; i<4; i++) printf("%d", csrc[i]);printf("}\n");
//    for(int j=0; j<n_mom; j++) { printf("mom[%d]={", j);for(int i=0; i<3; i++) printf("%d", mom_list[3*j+i]);printf("}\n"); }
//    printf("t_axis=%d\n", t_axis);
//    printf("bc_t=%f\n", bc_t);
//    printf("aff_kpath=%s\n", aff_kpath);

    if (0 < n_fields && 0 < n_mom) {
        
        ratime = MPI_Wtime ();
      
        qlua_Aff_enter(L);
        CALL_QDP(L);

        retime = MPI_Wtime ();
        if ( myprocid == 0 ) fprintf ( stdout, "# [q_save_momproj] time for qlua_Aff_enter+CALL_QDP = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

        ratime = MPI_Wtime ();

        status = save_c1_momproj(
                L, S, aff_w, aff_kpath, 
                n_fields, strkey_list, field_list, 
                n_mom, mom_list, 
                csrc, tlen, t_axis, bc_t, ft_sign);

        retime = MPI_Wtime ();
        if ( myprocid == 0 ) fprintf ( stdout, "# [q_save_momproj] time for save_c1_momproj = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );

        ratime = MPI_Wtime ();

        qlua_Aff_leave();

        retime = MPI_Wtime ();
        if ( myprocid == 0 ) fprintf ( stdout, "# [q_save_momproj] time for qlua_Aff_leave = %e seconds %s %d\n", retime-ratime, __FILE__, __LINE__ );
    }
 
clearerr_1:   
    qlua_free_not_null(L, csrc);
    qlua_free_not_null(L, mom_list);
    qlua_free_not_null(L, strkey_list);
    qlua_free_not_null(L, field_list);

    if (status)
        luaL_error(L, status);

    return 0;
}

